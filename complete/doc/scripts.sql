
CREATE TABLE `server_configuration` (
  `config_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `config_value` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`config_key`),
  UNIQUE KEY `config_key_UNIQUE` (`config_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


INSERT INTO api_monitor.server_configuration
(config_key, config_value) 
VALUES ('CIELO_CLIENT_API', 'https://qasnews.cielo.com.br/plugin_cielo_api');

INSERT INTO api_monitor.server_configuration
(config_key, config_value) 
VALUES ('CIELO_FIDELIDADE_APP_WS_URL', 'http://hml.cielofidelidade.com.br/site/ws/api');

INSERT INTO api_monitor.server_configuration
(config_key, config_value) 
VALUES ('CIELO_FIDELIDADE_WS_URL', 'http://hml.cielofidelidade.com.br/site/ws/api');

INSERT INTO api_monitor.server_configuration
(config_key, config_value) 
VALUES ('TOKEN_EXPIRATION_IN_SECONDS', '1800');

INSERT INTO api_monitor.server_configuration
(config_key, config_value) 
VALUES ('CIELO_CLIENT_DATA_USER', 'testesitecielo');

INSERT INTO api_monitor.server_configuration
(config_key, config_value) 
VALUES ('CIELO_CLIENT_DATA_PASS', 'cielo2212');

INSERT INTO api_monitor.server_configuration
(config_key, config_value) 
VALUES ('CIELO_CLIENT_DATA_EC', '2000147652');

INSERT INTO api_monitor.server_configuration
(config_key, config_value) 
VALUES ('CIELO_API_KEY', '123456');

INSERT INTO api_monitor.server_configuration
(config_key, config_value) 
VALUES ('WEBPREMIOS_WS_URL', 'http://servicohml.webpremios.com.br');

INSERT INTO api_monitor.server_configuration
(config_key, config_value) 
VALUES ('BURNING_API', 'http://hml.cielofidelidade.com.br/site/ws/api');

INSERT INTO api_monitor.server_configuration
(config_key, config_value) 
VALUES ('LTM_API_KEY', '123456');

CREATE TABLE service_log (
     id BIGINT NOT NULL AUTO_INCREMENT,
     api VARCHAR(10),
     service VARCHAR(40),
     status CHAR(10),
     reason CHAR(20),
     data date,
     PRIMARY KEY (id)
)


