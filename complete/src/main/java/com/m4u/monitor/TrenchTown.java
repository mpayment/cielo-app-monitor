package com.m4u.monitor;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import com.m4u.monitor.commons.persistence.model.ServiceLog;
import com.m4u.monitor.commons.util.Util;




public class TrenchTown {

	@Autowired
	private static Util nMsg = new Util();
	
    @PersistenceContext
    private static EntityManager entityManager;
    
    
	
	public static void main(String[] args) {
		
		SpringApplication.run(Start.class, args);
		nMsg.dispH1("API Monitor iniciando loop de verificação... " + Util.timeNow());
		
		ServiceLog t =  new ServiceLog();
		t.setApi("TESTE");
		entityManager.persist(t);
		
	}

}
