package com.m4u.monitor.model;

public class MonitorApiDTO {

	private String call_user;
	private String call_password;
	private String call_ec;
	private String user_id;
	private String user_nomeLogin;
	private String user_nomePessoa;
	private String user_email;
	private String user_dddTelefoneComercial;
	private String user_telefoneComercial;
	private String ec_nuEstabelecimento;
	private String ec_nuMatriz;
	private String ec_nuCnpj;
	private String ack_token;
	private String ack_errorCode;
	private String ack_call_message;
	private String ltm_api_key;
	private boolean api_online_cielo = true;
	private boolean api_online_ltm = true;
	
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_nomeLogin() {
		return user_nomeLogin;
	}
	public void setUser_nomeLogin(String user_nomeLogin) {
		this.user_nomeLogin = user_nomeLogin;
	}
	public String getUser_nomePessoa() {
		return user_nomePessoa;
	}
	public void setUser_nomePessoa(String user_nomePessoa) {
		this.user_nomePessoa = user_nomePessoa;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getUser_dddTelefoneComercial() {
		return user_dddTelefoneComercial;
	}
	public void setUser_dddTelefoneComercial(String user_dddTelefoneComercial) {
		this.user_dddTelefoneComercial = user_dddTelefoneComercial;
	}
	public String getUser_telefoneComercial() {
		return user_telefoneComercial;
	}
	public void setUser_telefoneComercial(String user_telefoneComercial) {
		this.user_telefoneComercial = user_telefoneComercial;
	}
	public String getEc_nuEstabelecimento() {
		return ec_nuEstabelecimento;
	}
	public void setEc_nuEstabelecimento(String ec_nuEstabelecimento) {
		this.ec_nuEstabelecimento = ec_nuEstabelecimento;
	}
	public String getEc_nuMatriz() {
		return ec_nuMatriz;
	}
	public void setEc_nuMatriz(String ec_nuMatriz) {
		this.ec_nuMatriz = ec_nuMatriz;
	}
	public String getEc_nuCnpj() {
		return ec_nuCnpj;
	}
	public void setEc_nuCnpj(String ec_nuCnpj) {
		this.ec_nuCnpj = ec_nuCnpj;
	}
	public String getCall_user() {
		return call_user;
	}
	public void setCall_user(String call_user) {
		this.call_user = call_user;
	}
	public String getCall_password() {
		return call_password;
	}
	public void setCall_password(String call_password) {
		this.call_password = call_password;
	}
	public String getCall_ec() {
		return call_ec;
	}
	public void setCall_ec(String call_ec) {
		this.call_ec = call_ec;
	}
	public String getAck_token() {
		return ack_token;
	}
	public void setAck_token(String ack_token) {
		this.ack_token = ack_token;
	}
	public String getAck_errorCode() {
		return ack_errorCode;
	}
	public void setAck_errorCode(String ack_errorCode) {
		this.ack_errorCode = ack_errorCode;
	}
	public String getAck_call_message() {
		return ack_call_message;
	}
	public void setAck_call_message(String ack_call_message) {
		this.ack_call_message = ack_call_message;
	}
	public boolean isApi_online_cielo() {
		return api_online_cielo;
	}
	public void setApi_online_cielo(boolean api_online_cielo) {
		this.api_online_cielo = api_online_cielo;
	}
	public boolean isApi_online_ltm() {
		return api_online_ltm;
	}
	public void setApi_online_ltm(boolean api_online_ltm) {
		this.api_online_ltm = api_online_ltm;
	}
	public String getLtm_api_key() {
		return ltm_api_key;
	}
	public void setLtm_api_key(String ltm_api_key) {
		this.ltm_api_key = ltm_api_key;
	}
	
}

