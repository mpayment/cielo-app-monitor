package com.m4u.monitor.model;


public class StateCtrlDTO {

	private boolean api_online_cielo = true;
	private boolean api_online_ltm = true;
	private boolean process_alert = false;
	
	
	public boolean isApi_online_cielo() {
		return api_online_cielo;
	}
	public void setApi_online_cielo(boolean api_online_cielo) {
		this.api_online_cielo = api_online_cielo;
	}
	public boolean isApi_online_ltm() {
		return api_online_ltm;
	}
	public void setApi_online_ltm(boolean api_online_ltm) {
		this.api_online_ltm = api_online_ltm;
	}
	public boolean isProcess_alert() {
		return process_alert;
	}
	public void setProcess_alert(boolean process_alert) {
		this.process_alert = process_alert;
	}

	
}

