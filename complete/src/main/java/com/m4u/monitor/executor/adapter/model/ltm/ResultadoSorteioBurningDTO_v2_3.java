package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResultadoSorteioBurningDTO_v2_3 {
	
	@XmlAttribute(name = "DataResgate")
	private XMLGregorianCalendar dataResgate;
	
	@XmlAttribute(name = "DataSorteio")
	private XMLGregorianCalendar dataSorteio;
	
	@XmlAttribute(name = "Cupom")
	private List<CupomBurningDTO> cupom;

	public XMLGregorianCalendar getDataResgate() {
		return dataResgate;
	}

	public void setDataResgate(XMLGregorianCalendar dataResgate) {
		this.dataResgate = dataResgate;
	}

	public XMLGregorianCalendar getDataSorteio() {
		return dataSorteio;
	}

	public void setDataSorteio(XMLGregorianCalendar dataSorteio) {
		this.dataSorteio = dataSorteio;
	}

	public List<CupomBurningDTO> getCupom() {
		return cupom;
	}

	public void setCupom(List<CupomBurningDTO> cupom) {
		this.cupom = cupom;
	}

	@Override
	public String toString() {
		return "ResultadoSorteioBurningDTO [dataResgate=" + dataResgate
				+ ", dataSorteio=" + dataSorteio + ", cupom=" + cupom + "]";
	}
}