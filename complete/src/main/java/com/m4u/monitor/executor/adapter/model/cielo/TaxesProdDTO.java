package com.m4u.monitor.executor.adapter.model.cielo;

public class TaxesProdDTO {

	private Integer codigoProdutoServico;
	private String nomeProdutoServico;
	private String valorTaxa;
	private Integer quantidadeParcela;
	private Integer prazo;

	public Integer getCodigoProdutoServico() {
		return codigoProdutoServico;
	}

	public void setCodigoProdutoServico(Integer codigoProdutoServico) {
		this.codigoProdutoServico = codigoProdutoServico;
	}

	public String getNomeProdutoServico() {
		return nomeProdutoServico;
	}

	public void setNomeProdutoServico(String nomeProdutoServico) {
		this.nomeProdutoServico = nomeProdutoServico;
	}

	public String getValorTaxa() {
		return valorTaxa;
	}

	public void setValorTaxa(String valorTaxa) {
		this.valorTaxa = valorTaxa;
	}

	public Integer getQuantidadeParcela() {
		return quantidadeParcela;
	}

	public void setQuantidadeParcela(Integer quantidadeParcela) {
		this.quantidadeParcela = quantidadeParcela;
	}

	public Integer getPrazo() {
		return prazo;
	}

	public void setPrazo(Integer prazo) {
		this.prazo = prazo;
	}
}