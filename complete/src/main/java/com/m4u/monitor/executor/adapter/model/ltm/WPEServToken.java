package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class WPEServToken {

	@XmlAttribute(name="IdWsSessao")
	private String idWsSessao;
	
	@XmlElement(name="Campanha")
	private WPCampaign campanha;
	
	@XmlElement(name="Usuario")
	private WPUser usuario;
	
	public WPEServToken() { }
	
	public WPEServToken(WPCampaign campanha) {
		this.campanha = campanha;
	}

	public WPCampaign getCampanha() {
		return campanha;
	}

	public void setCampanha(WPCampaign campanha) {
		this.campanha = campanha;
	}

	public String getIdWsSessao() {
		return idWsSessao;
	}

	public void setIdWsSessao(String idWsSessao) {
		this.idWsSessao = idWsSessao;
	}

	public WPUser getUsuario() {
		return usuario;
	}

	public void setUsuario(WPUser usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		return "WPEServToken [idWsSessao=" + idWsSessao + ", campanha="
				+ campanha + ", usuario=" + usuario + "]";
	}	
}