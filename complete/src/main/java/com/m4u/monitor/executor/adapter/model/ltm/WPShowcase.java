package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;

public class WPShowcase {
	
	@XmlAttribute(name="IdVitrine")
	private Integer idVitrine;
	
	@XmlAttribute(name="Nome")
	private String nome;
	
	@XmlAttribute(name="IdTipoVitrine")
	private Integer idTipoVitrine;
	
	@XmlAttribute(name="Tipo")
	private String tipo;
	
	@XmlAttribute(name="Produtos")
	@XmlList
	private List<WPProduct> produtos;

	public Integer getIdTipoVitrine() {
		return idTipoVitrine;
	}

	public void setIdTipoVitrine(Integer idTipoVitrine) {
		this.idTipoVitrine = idTipoVitrine;
	}

	public Integer getIdVitrine() {
		return idVitrine;
	}

	public void setIdVitrine(Integer idVitrine) {
		this.idVitrine = idVitrine;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<WPProduct> getProdutos() {
	    if (produtos == null) {
	        produtos = new ArrayList<WPProduct>();
	    }
		return produtos;
	}

	public void setProdutos(List<WPProduct> produtos) {
		this.produtos = produtos;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "WPShowcase [idVitrine=" + idVitrine + ", nome=" + nome
				+ ", idTipoVitrine=" + idTipoVitrine + ", tipo=" + tipo
				+ ", produtos=" + produtos + "]";
	}
	
	public String toResumedString() {
		return "WPShowcase [idVitrine=" + idVitrine + ", nome=" + nome
				+ ", idTipoVitrine=" + idTipoVitrine + ", tipo=" + tipo
				+ ", produtos=" + produtos.size() + " items]";
	}
}