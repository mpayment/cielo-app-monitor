package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ListarDepartamentoResponseDTO {

	@XmlElement(name = "Departamentos")
	private List<DepartamentoVitrineDTO> departamentos;

	public List<DepartamentoVitrineDTO> getDepartamentos() {
		return departamentos;
	}

	public void setDepartamentos(List<DepartamentoVitrineDTO> departamentos) {
		this.departamentos = departamentos;
	}

	@Override
	public String toString() {
		return "ListarDepartamentoResponseDTO [departamentos=" + departamentos + "]";
	}
}