package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class WPUserAuthorizationRequest extends WPRequest {
	
	private static final String TIPO_LOGIN = "1";
	private static final boolean USUARIO_TESTE = false;
	
	@XmlAttribute(name="Login")
	private String login;
	
	@XmlAttribute(name="IdCampanhaGestaoPontos")
	private Integer idCampanhaGestaoPontos;
	
	@XmlAttribute(name="IdEmpresaGestaoPontos")
	private Integer idEmpresaGestaoPontos;
	
	@XmlAttribute(name="Senha")
	private String senha;
	
	@XmlAttribute(name="TipoLogin")
	private final String tipoLogin = TIPO_LOGIN;
	
	@XmlAttribute(name="EmailPessoal")
	private String emailPessoal;
	
	@XmlAttribute(name="UsuarioTeste")
	private final boolean usuarioTeste = USUARIO_TESTE;
	
	@XmlAttribute(name="SenhaProvisoria") //OPCIONAL
	private String senhaProvisoria;
	
	@XmlAttribute(name="IdGrupoResgate") //OPCIONAL
	private String idGrupoResgate;
	
	@XmlAttribute(name="CPF") //OBRIGATÓRIO
	private String cpf;
	
	@XmlAttribute(name="CNPJ") //OBRIGATÓRIO
	private String cnpj;
	
	@XmlAttribute(name="DataNascimento") //OPCIONAL
	private String dataNascimento;
	
	@XmlAttribute(name="IdCampanha") //OPCIONAL
	private Long idCampanha;
	
	@XmlAttribute(name="IdEmpresa") //OPCIONAL
	private Long idEmpresa;
	
	@XmlAttribute(name="Nome") //OBRIGATÓRIO
	private String nome;
	
	@XmlAttribute(name="Sobrenome") //OPCIONAL
    private String sobrenome;
	
	@XmlAttribute(name="Apelido") //OPCIONAL
    private String apelido;
	
	@XmlAttribute(name="RG") //OPCIONAL
    private String rg;
	
	@XmlAttribute(name="Sexo") //OPCIONAL
    private String sexo;
	
	@XmlAttribute(name="EstadoCivil") //OPCIONAL
    private String estadoCivil;
	
	@XmlElement(name="DadosPessoais") //OBRIGATÓRIO
    private WPDadosPessoais dadosPessoais;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Integer getIdCampanhaGestaoPontos() {
		return idCampanhaGestaoPontos;
	}

	public void setIdCampanhaGestaoPontos(Integer idCampanhaGestaoPontos) {
		this.idCampanhaGestaoPontos = idCampanhaGestaoPontos;
	}

	public Integer getIdEmpresaGestaoPontos() {
		return idEmpresaGestaoPontos;
	}

	public void setIdEmpresaGestaoPontos(Integer idEmpresaGestaoPontos) {
		this.idEmpresaGestaoPontos = idEmpresaGestaoPontos;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getTipoLogin() {
		return tipoLogin;
	}

	public String getEmailPessoal() {
		return emailPessoal;
	}

	public void setEmailPessoal(String emailPessoal) {
		this.emailPessoal = emailPessoal;
	}

	public boolean isUsuarioTeste() {
		return usuarioTeste;
	}

	public String getSenhaProvisoria() {
		return senhaProvisoria;
	}

	public void setSenhaProvisoria(String senhaProvisoria) {
		this.senhaProvisoria = senhaProvisoria;
	}

	public String getIdGrupoResgate() {
		return idGrupoResgate;
	}

	public void setIdGrupoResgate(String idGrupoResgate) {
		this.idGrupoResgate = idGrupoResgate;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Long getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(Long idCampanha) {
		this.idCampanha = idCampanha;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public WPDadosPessoais getDadosPessoais() {
		return dadosPessoais;
	}

	public void setDadosPessoais(WPDadosPessoais dadosPessoais) {
		this.dadosPessoais = dadosPessoais;
	}

	@Override
	public String toString() {
		return "WPUserAuthorizationRequest [login=" + login
				+ ", idCampanhaGestaoPontos=" + idCampanhaGestaoPontos
				+ ", idEmpresaGestaoPontos=" + idEmpresaGestaoPontos
				+ ", tipoLogin=" + tipoLogin
				+ ", emailPessoal=" + emailPessoal + ", usuarioTeste="
				+ usuarioTeste + ", senhaProvisoria=" + senhaProvisoria
				+ ", idGrupoResgate=" + idGrupoResgate + ", cpf=" + cpf
				+ ", cnpj=" + cnpj + ", dataNascimento=" + dataNascimento
				+ ", idCampanha=" + idCampanha + ", idEmpresa=" + idEmpresa
				+ ", nome=" + nome + ", sobrenome=" + sobrenome + ", apelido="
				+ apelido + ", rg=" + rg + ", sexo=" + sexo + ", estadoCivil="
				+ estadoCivil + ", dadosPessoais=" + dadosPessoais + "]";
	}
}