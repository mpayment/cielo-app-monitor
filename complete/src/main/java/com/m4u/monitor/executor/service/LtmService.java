package com.m4u.monitor.executor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.m4u.monitor.executor.adapter.LtmAppApiWebProxyHolder;
import com.m4u.monitor.executor.adapter.model.ltm.LtmDadosLoginSimplificado;
import com.m4u.monitor.executor.adapter.model.ltm.LtmResponse;
import com.m4u.monitor.executor.adapter.model.ltm.LtmRespostaLoginSimplificado;


@Component
public class LtmService {

	@Autowired
	private LtmAppApiWebProxyHolder webProxyHolder;
	

	public LtmResponse<LtmRespostaLoginSimplificado> loginSimplificado(String apiKey,
    		LtmDadosLoginSimplificado dadosLogin) {
    	
    	final LtmResponse<LtmRespostaLoginSimplificado> ltmResponse = 
    		 webProxyHolder.getWebProxy().loginSimplificado(apiKey, dadosLogin);
    	
    	return ltmResponse;
    }
	
	
}

