package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResultadoSorteioBurningDTO_v2_7 {

	@XmlElement(name = "DataResgate")
	private XMLGregorianCalendar dataResgate;
	
	@XmlElement(name = "DataSorteio")
	private XMLGregorianCalendar dataSorteio;
	
	@XmlElement(name = "Premio")
	private String premio;
	
	@XmlElement(name = "Contemplado")
	private boolean contemplado;
	
	@XmlElement(name = "Cupom")
	private CupomBurningDTO cupom;

	public XMLGregorianCalendar getDataResgate() {
		return dataResgate;
	}

	public void setDataResgate(XMLGregorianCalendar dataResgate) {
		this.dataResgate = dataResgate;
	}

	public XMLGregorianCalendar getDataSorteio() {
		return dataSorteio;
	}

	public void setDataSorteio(XMLGregorianCalendar dataSorteio) {
		this.dataSorteio = dataSorteio;
	}

	public CupomBurningDTO getCupom() {
		return cupom;
	}

	public void setCupom(CupomBurningDTO cupom) {
		this.cupom = cupom;
	}

	public String getPremio() {
		return premio;
	}

	public void setPremio(String premio) {
		this.premio = premio;
	}

	public boolean isContemplado() {
		return contemplado;
	}

	public void setContemplado(boolean contemplado) {
		this.contemplado = contemplado;
	}

	@Override
	public String toString() {
		return "ResultadoSorteioBurningDTO [dataResgate=" + dataResgate
				+ ", dataSorteio=" + dataSorteio + ", cupom=" + cupom
				+ ", premio=" + premio + ", contemplado=" + contemplado + "]";
	}
}