package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPPartnerAuthorizationResponse {

	@XmlAttribute(name="TokenSessao")
	private String tokenSessao;
	
	@XmlAttribute(name="UrlAcesso")
	private String urlAcesso;

	public String getTokenSessao() {
		return tokenSessao;
	}

	public void setTokenSessao(String tokenSessao) {
		this.tokenSessao = tokenSessao;
	}

	public String getUrlAcesso() {
		return urlAcesso;
	}

	public void setUrlAcesso(String urlAcesso) {
		this.urlAcesso = urlAcesso;
	}

	@Override
	public String toString() {
		return "WPAuthorizePartnerResponse [tokenSessao=" + tokenSessao
				+ ", urlAcesso=" + urlAcesso + "]";
	}
}