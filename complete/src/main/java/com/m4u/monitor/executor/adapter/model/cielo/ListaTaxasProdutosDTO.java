package com.m4u.monitor.executor.adapter.model.cielo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;



@XmlAccessorType(XmlAccessType.FIELD)
public class ListaTaxasProdutosDTO {

	private List<TaxesProdDTO> taxas;

	public List<TaxesProdDTO> getTaxas() {
		return taxas;
	}

	public void setOfertas(List<TaxesProdDTO> taxas) {
		this.taxas = taxas;
	}
}