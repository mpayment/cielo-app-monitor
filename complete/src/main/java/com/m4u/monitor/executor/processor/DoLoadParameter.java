package com.m4u.monitor.executor.processor;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.m4u.monitor.commons.service.CieloCacheManager;
import com.m4u.monitor.commons.service.ServerConfigurationService;
import com.m4u.monitor.commons.util.constants.ServerConfigurationKey;
import com.m4u.monitor.model.MonitorApiDTO;


@Component
public class DoLoadParameter  {

	@Autowired
	private CieloCacheManager cacheManager;
	
	@Autowired
	private ServerConfigurationService srvCfgService;
	
	public MonitorApiDTO processDoLoad(MonitorApiDTO matrix) {
		
		matrix.setCall_user(srvCfgService.getConfiguration(ServerConfigurationKey.CIELO_CLIENT_DATA_USER));
		matrix.setCall_password(srvCfgService.getConfiguration(ServerConfigurationKey.CIELO_CLIENT_DATA_PASS));
		matrix.setCall_ec(srvCfgService.getConfiguration(ServerConfigurationKey.CIELO_CLIENT_DATA_EC));
		matrix.setLtm_api_key(srvCfgService.getConfiguration(ServerConfigurationKey.CIELO_API_KEY));
		
		return matrix;
	}

	
	
		
}
