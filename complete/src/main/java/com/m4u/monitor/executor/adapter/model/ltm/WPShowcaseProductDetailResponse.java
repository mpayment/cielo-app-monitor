package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlElement;

public class WPShowcaseProductDetailResponse {

	@XmlElement(name="Produto")
	private WPProduct produto;

	public WPProduct getProduto() {
		return produto;
	}

	public void setProduto(WPProduct produto) {
		this.produto = produto;
	}

	@Override
	public String toString() {
		return "WPShowcaseProductDetailResponse [produto=" + produto + "]";
	}
}