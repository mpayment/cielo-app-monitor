package com.m4u.monitor.executor.verifier;

import javax.ws.rs.ServiceUnavailableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.m4u.monitor.commons.util.Util;
import com.m4u.monitor.executor.adapter.model.cielo.CieloResponseDTO;
import com.m4u.monitor.executor.adapter.model.cielo.ResponseLoginUsuarioDTO;
import com.m4u.monitor.executor.service.CieloService;
import com.m4u.monitor.model.MonitorApiDTO;


@Component
public class VerifierCielo {

	@Autowired
	private CieloService cieloService;
	
	@Autowired
	private Util nMsg;
	
	
	
	public MonitorApiDTO verifyPlataform(MonitorApiDTO matrix) {
		
		try {
			@SuppressWarnings("unused")
			CieloResponseDTO<ResponseLoginUsuarioDTO> response = 
			   cieloService.doLogin(matrix.getCall_user(), matrix.getCall_password(), matrix.getCall_ec());
			
		} catch (ServiceUnavailableException e){
			nMsg.dispH1("   O F F L I N E   {} ");
			nMsg.dispH1("   Reason : "   + Util.extractFromException(e.getMessage()) + "\n");
			matrix.setApi_online_cielo(false);
			return matrix;
			
		} catch (Exception e){
			nMsg.disp(  "   O F F L I N E   {} ");
			nMsg.dispH1("   Reason : "   + Util.extractFromException(e.getMessage()) + "\n");
			return matrix;
		}
		matrix.setApi_online_cielo(true);
		return matrix;
	}
	

	public MonitorApiDTO verifyLogin(MonitorApiDTO matrix) {
		
		try {
			CieloResponseDTO<ResponseLoginUsuarioDTO> response = 
			   cieloService.doLogin(matrix.getCall_user(), matrix.getCall_password(), matrix.getCall_ec());
			
			nMsg.disp(response.getResponseMessageCode());
			nMsg.disp(response.getObject().getToken());
			nMsg.disp(response.getObject().getUsuario());
			nMsg.disp(response.getObject().getEstabelecimento());
			
		} catch (ServiceUnavailableException e){
			nMsg.dispH1("   O F F L I N E   ");	
		} catch (Exception e){
			nMsg.dispH1("   O F F L I N E   ");	
		}		
		
		return matrix;
	}
	
	
}
