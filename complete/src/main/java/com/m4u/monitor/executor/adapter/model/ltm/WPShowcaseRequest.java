package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.*;

public class WPShowcaseRequest extends WPRequest {

	@XmlElement(name="EServToken")
	private WPEServToken eServToken;
	
	@XmlAttribute(name="IdVitrine")
	private Integer idVitrine;

	public Integer getIdVitrine() {
		return idVitrine;
	}

	public void setIdVitrine(Integer idVitrine) {
		this.idVitrine = idVitrine;
	}
	
	public WPEServToken geteServToken() {
		return eServToken;
	}

	public void setEServToken(WPEServToken eServToken) {
		this.eServToken = eServToken;
	}

	@Override
	public String toString() {
		return "WPShowcaseRequest [eServToken=" + eServToken + ", idVitrine="
				+ idVitrine + "]" + super.toString();
	}	
}