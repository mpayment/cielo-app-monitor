package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class CupomBurningDTO {

	@XmlElement(name = "NumeroSerie")
	private String numeroSerie;

	@XmlElement(name = "NumeroCupom")
	private String numeroCupom;

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getNumeroCupom() {
		return numeroCupom;
	}

	public void setNumeroCupom(String numeroCupom) {
		this.numeroCupom = numeroCupom;
	}

	@Override
	public String toString() {
		return "CupomBurningDTO [numeroSerie=" + numeroSerie + ", numeroCupom="
				+ numeroCupom + "]";
	}
}