package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlElement;

public class Transacao {

	public static final String DATE_TIME_MAIN_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.S";
	public static final String DATE_TIME_FALLBACK_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
	
	@XmlElement(name = "UpdateDate")
	private String updateDate;
	
	@XmlElement(name = "Description")
	private String description;
	
	@XmlElement(name = "Value")
	private String value;
	
	@XmlElement(name = "ExternalCode")
	private Long externalCode;
	
	@XmlElement(name = "TransactionType")
	private Short transactionType;
	
	@XmlElement(name = "InsertDate")
	private String insertDate;

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getExternalCode() {
		return externalCode;
	}

	public void setExternalCode(Long externalCode) {
		this.externalCode = externalCode;
	}

	public Short getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(Short transactionType) {
		this.transactionType = transactionType;
	}

	public String getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(String insertDate) {
		this.insertDate = insertDate;
	}
}