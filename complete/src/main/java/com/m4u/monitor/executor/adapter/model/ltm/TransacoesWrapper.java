package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class TransacoesWrapper {

	@XmlElement(name = "Transactions")
	private List<Transacao> transactions;

	public List<Transacao> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transacao> transactions) {
		this.transactions = transactions;
	}
}