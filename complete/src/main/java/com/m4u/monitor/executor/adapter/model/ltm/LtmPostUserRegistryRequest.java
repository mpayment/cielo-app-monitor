package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class LtmPostUserRegistryRequest {

	@XmlAttribute(name="IdParticipante")
	private long idParticipante;
	
	@XmlAttribute(name="Cpf")
	private String cpf;
	
	@XmlAttribute(name="Sexo")
	private String sexo;
	
	@XmlAttribute(name="DataNascimento")
	private String dataNascimento;
	
	@XmlAttribute(name="DddTelefoneResidencial")
	private String dddTelefoneResidencial;
	
	@XmlAttribute(name="TelefoneResidencial")
	private String telefoneResidencial;
	
	@XmlAttribute(name="IdOperadora")
	private int idOperadora;
	
	@XmlAttribute(name="DddTelefoneCelular")
	private String dddTelefoneCelular;
	
	@XmlAttribute(name="TelefoneCelular")
	private String telefoneCelular;
	
	@XmlAttribute(name="Email")
	private String email;
	
	@XmlAttribute(name="IdCargo")
	private int idCargo;
	
	@XmlAttribute(name="Cnpj")
	private String cnpj;
	
	@XmlAttribute(name="Nome")
	private String nome;
	
	@XmlAttribute(name="Cep")
	private String cep;
	
	@XmlAttribute(name="EnderecoComercial")
	private String enderecoComercial;
	
	@XmlAttribute(name="Numero")
	private String numero;
	
	@XmlAttribute(name="Complemento")
	private String complemento;
	
	@XmlAttribute(name="Bairro")
	private String bairro;
	
	@XmlAttribute(name="Cidade")
	private String cidade;
	
	@XmlAttribute(name="Estado")
	private String estado;
	
	@XmlAttribute(name="DddTelefoneComercial")
	private String dddTelefoneComercial;
	
	@XmlAttribute(name="TelefoneComercial")
	private String telefoneComercial;

	public long getIdParticipante() {
		return idParticipante;
	}

	public void setIdParticipante(long idParticipante) {
		this.idParticipante = idParticipante;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getDddTelefoneResidencial() {
		return dddTelefoneResidencial;
	}

	public void setDddTelefoneResidencial(String dddTelefoneResidencial) {
		this.dddTelefoneResidencial = dddTelefoneResidencial;
	}

	public String getTelefoneResidencial() {
		return telefoneResidencial;
	}

	public void setTelefoneResidencial(String telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}

	public int getIdOperadora() {
		return idOperadora;
	}

	public void setIdOperadora(int idOperadora) {
		this.idOperadora = idOperadora;
	}

	public String getDddTelefoneCelular() {
		return dddTelefoneCelular;
	}

	public void setDddTelefoneCelular(String dddTelefoneCelular) {
		this.dddTelefoneCelular = dddTelefoneCelular;
	}

	public String getTelefoneCelular() {
		return telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(int idCargo) {
		this.idCargo = idCargo;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getEnderecoComercial() {
		return enderecoComercial;
	}

	public void setEnderecoComercial(String enderecoComercial) {
		this.enderecoComercial = enderecoComercial;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getDddTelefoneComercial() {
		return dddTelefoneComercial;
	}

	public void setDddTelefoneComercial(String dddTelefoneComercial) {
		this.dddTelefoneComercial = dddTelefoneComercial;
	}

	public String getTelefoneComercial() {
		return telefoneComercial;
	}

	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

	@Override
	public String toString() {
		return "LtmPostUserRegistryRequest [idParticipante=" + idParticipante
				+ ", cpf=" + cpf + ", sexo=" + sexo + ", dataNascimento="
				+ dataNascimento + ", dddTelefoneResidencial="
				+ dddTelefoneResidencial + ", telefoneResidencial="
				+ telefoneResidencial + ", idOperadora=" + idOperadora
				+ ", dddTelefoneCelular=" + dddTelefoneCelular
				+ ", telefoneCelular=" + telefoneCelular + ", email=" + email
				+ ", idCargo=" + idCargo + ", cnpj=" + cnpj + ", nome=" + nome
				+ ", cep=" + cep + ", enderecoComercial=" + enderecoComercial
				+ ", numero=" + numero + ", complemento=" + complemento
				+ ", bairro=" + bairro + ", cidade=" + cidade + ", estado="
				+ estado + ", dddTelefoneComercial=" + dddTelefoneComercial
				+ ", telefoneComercial=" + telefoneComercial + "]";
	}
}