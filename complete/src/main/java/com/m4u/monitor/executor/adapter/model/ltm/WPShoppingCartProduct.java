package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPShoppingCartProduct {

	@XmlAttribute(name="IdProduto")
	private long idProduto;

	public long getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(long idProduto) {
		this.idProduto = idProduto;
	}

	@Override
	public String toString() {
		return "WPShoppingCartProduct [idProduto=" + idProduto + "]";
	}
}