package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResgateBurningRequestDTO {

	@XmlElement(name = "IdParticipante")
	private long idParticipante;

	@XmlElement(name = "IdBurningCampanha")
	private long idBurningCampanha;

	@XmlElement(name = "IdProduto")
	private long idProduto;

	@XmlElement(name = "Quantidade")
	private int quantidade;

	public long getIdParticipante() {
		return idParticipante;
	}

	public void setIdParticipante(long idParticipante) {
		this.idParticipante = idParticipante;
	}

	public long getIdBurningCampanha() {
		return idBurningCampanha;
	}

	public void setIdBurningCampanha(long idBurningCampanha) {
		this.idBurningCampanha = idBurningCampanha;
	}

	public long getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(long idProduto) {
		this.idProduto = idProduto;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public String toString() {
		return "ResgateBurningRequestDTO [idParticipante=" + idParticipante
				+ ", idBurningCampanha=" + idBurningCampanha + ", idProduto="
				+ idProduto + ", quantidade=" + quantidade + "]";
	}
}