package com.m4u.monitor.executor.adapter.model.cielo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class FilialClienteDTO {

	private long nuEstabelecimento;
	private long nuMatriz;
	private String nmLogradouro;
	private String nmRazaoSocial;

	public long getNuEstabelecimento() {
		return nuEstabelecimento;
	}

	public void setNuEstabelecimento(long nuEstabelecimento) {
		this.nuEstabelecimento = nuEstabelecimento;
	}

	public long getNuMatriz() {
		return nuMatriz;
	}

	public void setNuMatriz(long nuMatriz) {
		this.nuMatriz = nuMatriz;
	}

	public String getNmLogradouro() {
		return nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	public String getNmRazaoSocial() {
		return nmRazaoSocial;
	}

	public void setNmRazaoSocial(String nmRazaoSocial) {
		this.nmRazaoSocial = nmRazaoSocial;
	}
}