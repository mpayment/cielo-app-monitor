package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ListarDepartamentoRequestDTO extends WPRequest {

	@XmlElement(name="EServToken")
	private WPEServToken eServToken;

	public WPEServToken geteServToken() {
		return eServToken;
	}

	public void seteServToken(WPEServToken eServToken) {
		this.eServToken = eServToken;
	}

	@Override
	public String toString() {
		return "ListarDepartamentoRequestDTO [eServToken=" + eServToken
				+ ", TokenAplicacao=" + getTokenAplicacao() + "]";
	}
}
