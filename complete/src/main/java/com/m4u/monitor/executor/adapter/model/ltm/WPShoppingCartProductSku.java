package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPShoppingCartProductSku {

	@XmlAttribute(name="IdProdutoSku")
	private long idProdutoSku;

    @XmlAttribute(name="IdProdutoSkuOriginal")
    private long idProdutoSkuOriginal;

    public long getIdProdutoSku() {
        return idProdutoSku;
    }

    public void setIdProdutoSku(long idProdutoSku) {
        this.idProdutoSku = idProdutoSku;
    }

    public long getIdProdutoSkuOriginal() {
        return idProdutoSkuOriginal;
    }

    public void setIdProdutoSkuOriginal(long idProdutoSkuOriginal) {
        this.idProdutoSkuOriginal = idProdutoSkuOriginal;
    }

    @Override
    public String toString() {
        return "WPShoppingCartProductSku{" +
                "idProdutoSku=" + idProdutoSku +
                ", idProdutoSkuOriginal=" + idProdutoSkuOriginal +
                '}';
    }
}
