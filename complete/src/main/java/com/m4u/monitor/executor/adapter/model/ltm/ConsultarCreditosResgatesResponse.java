package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlElement;

public class ConsultarCreditosResgatesResponse {

	@XmlElement(name = "ValorCreditos")
	public Double valorCreditos;

	@XmlElement(name = "ValorResgates")
	public Double valorResgates;

	public Double getValorCreditos() {
		return valorCreditos;
	}

	public void setValorCreditos(Double valorCreditos) {
		this.valorCreditos = valorCreditos;
	}

	public Double getValorResgates() {
		return valorResgates;
	}

	public void setValorResgates(Double valorResgates) {
		this.valorResgates = valorResgates;
	}
}