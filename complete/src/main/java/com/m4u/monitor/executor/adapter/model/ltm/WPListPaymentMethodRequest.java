package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlElement;

public class WPListPaymentMethodRequest extends WPRequest {

	@XmlElement(name="ListarTudo")
	private boolean listarTudo;
	
    @XmlElement(name="EServToken")
    private WPEServToken eServToken;

    public WPEServToken getEServToken() {
        return eServToken;
    }

    public void setEServToken(WPEServToken eServToken) {
        this.eServToken = eServToken;
    }

    public boolean isListarTudo() {
		return listarTudo;
	}

	public void setListarTudo(boolean listarTudo) {
		this.listarTudo = listarTudo;
	}

	public WPEServToken geteServToken() {
		return eServToken;
	}

	public void seteServToken(WPEServToken eServToken) {
		this.eServToken = eServToken;
	}

	@Override
	public String toString() {
		return "WPListPaymentMethodRequest [listarTudo=" + listarTudo
				+ ", eServToken=" + eServToken + "]";
	}
}