package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class ConsultarExtratoParticipanteResponse {

	@XmlElement(name = "JsonReturn")
	private List<TransacoesWrapper> jsonReturn;

	public List<TransacoesWrapper> getJsonReturn() {
		return jsonReturn;
	}

	public void setJsonReturn(List<TransacoesWrapper> jsonReturn) {
		this.jsonReturn = jsonReturn;
	}
}