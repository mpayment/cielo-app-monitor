package com.m4u.monitor.executor.adapter.model.cielo;

public class GestorResponseDTO {

    private Integer estabelecimentoComercial;
    private Integer codigoGestor;
    private String nomeDiretoria;
    private String nomeGestor;
    private String emailGestor;
    private Integer dddGEstor;
    private String telefoneGestor;
    
    
	public Integer getEstabelecimentoComercial() {
		return estabelecimentoComercial;
	}
	public void setEstabelecimentoComercial(Integer estabelecimentoComercial) {
		this.estabelecimentoComercial = estabelecimentoComercial;
	}
	public Integer getCodigoGestor() {
		return codigoGestor;
	}
	public void setCodigoGestor(Integer codigoGestor) {
		this.codigoGestor = codigoGestor;
	}
	public String getNomeDiretoria() {
		return nomeDiretoria;
	}
	public void setNomeDiretoria(String nomeDiretoria) {
		this.nomeDiretoria = nomeDiretoria;
	}
	public String getNomeGestor() {
		return nomeGestor;
	}
	public void setNomeGestor(String nomeGestor) {
		this.nomeGestor = nomeGestor;
	}
	public String getEmailGestor() {
		return emailGestor;
	}
	public void setEmailGestor(String emailGestor) {
		this.emailGestor = emailGestor;
	}
	public Integer getDddGEstor() {
		return dddGEstor;
	}
	public void setDddGEstor(Integer dddGEstor) {
		this.dddGEstor = dddGEstor;
	}
	public String getTelefoneGestor() {
		return telefoneGestor;
	}
	public void setTelefoneGestor(String telefoneGestor) {
		this.telefoneGestor = telefoneGestor;
	}

	public String getFormatedTelefoneGestor() {
		return "("+this.dddGEstor+") "+telefoneGestor;
	}

}

