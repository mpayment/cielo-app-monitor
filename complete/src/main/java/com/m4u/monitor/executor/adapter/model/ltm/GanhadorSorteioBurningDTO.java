package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
public class GanhadorSorteioBurningDTO {

	@XmlElement(name = "NomeGanhador")
	private String nomeGanhador;
	
	@XmlElement(name = "CupomSorteado")
	private String cupomSorteado;
	
	@XmlElement(name = "CupomContemplado")
	private String cupomContemplado;
	
	@XmlElement(name = "Premio")
	private String premio;
	
	@XmlElement(name = "RazaoSocial")
	private String razaoSocial;

	@XmlElement(name = "TipoSorteio")
	private String tipoSorteio;
	
	@XmlElement(name = "DataInclusao")
	private XMLGregorianCalendar dataInclusao;

	public String getNomeGanhador() {
		return nomeGanhador;
	}

	public void setNomeGanhador(String nomeGanhador) {
		this.nomeGanhador = nomeGanhador;
	}

	public String getCupomSorteado() {
		return cupomSorteado;
	}

	public void setCupomSorteado(String cupomSorteado) {
		this.cupomSorteado = cupomSorteado;
	}

	public String getCupomContemplado() {
		return cupomContemplado;
	}

	public void setCupomContemplado(String cupomContemplado) {
		this.cupomContemplado = cupomContemplado;
	}

	public String getPremio() {
		return premio;
	}

	public void setPremio(String premio) {
		this.premio = premio;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getTipoSorteio() {
		return tipoSorteio;
	}

	public void setTipoSorteio(String tipoSorteio) {
		this.tipoSorteio = tipoSorteio;
	}

	public XMLGregorianCalendar getDataInclusao() {
		return dataInclusao;
	}

	public void setDataInclusao(XMLGregorianCalendar dataInclusao) {
		this.dataInclusao = dataInclusao;
	}

	@Override
	public String toString() {
		return "GanhadorSorteioBurningDTO [nomeGanhador=" + nomeGanhador
				+ ", cupomSorteado=" + cupomSorteado + ", cupomContemplado="
				+ cupomContemplado + ", premio=" + premio + ", razaoSocial="
				+ razaoSocial + ", tipoSorteio=" + tipoSorteio
				+ ", dataInclusao=" + dataInclusao + "]";
	}
}