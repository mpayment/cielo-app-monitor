package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;



/**
 * This class can not be merged to {@link WPProduct} because the {@link WPProduto#urlsImagem} field does not
 * have the same type on both classes.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WPProduto {

	@XmlElement(name = "ImagemVitrine")
	private Boolean imagemVitrine;

	@XmlElement(name = "Ordem")
	private Integer ordem;

	@XmlElement(name = "Ativo")
	private Boolean ativo;

	@XmlElement(name = "ExibePreco")
	private Boolean exibePreco;

	@XmlElement(name = "IdProduto")
	private Long idProduto;

	@XmlElement(name = "Cliente")
	private WPCustomer cliente;

	@XmlElement(name = "Campanha")
	private WPCampaign campanha;

	@XmlElement(name = "Titulo")
	private String titulo;

	@XmlElement(name = "PrecoDeReais")
	private String precoDeReais;

	@XmlElement(name = "PrecoDePontos")
	private String precoDePontos;

	@XmlElement(name = "PrecoDe")
	private String precoDe;

	@XmlElement(name = "PrecoPorReais")
	private String precoPorReais;

	@XmlElement(name = "PrecoPorPontos")
	private String precoPorPontos;

	@XmlElement(name = "PrecoPor")
	private String precoPor;

	@XmlElement(name = "ValorComplementar")
	private String valorComplementar;

	@XmlElement(name = "UrlsImagem")
	private List<String> urlsImagem;

	@XmlElement(name = "ProdutosSku")
	private List<WPProductSku> produtosSku;

	@XmlElement(name = "Descricao")
	private String descricao;

	@XmlElement(name = "IdCategoria")
	private Long idCategoria;

	@XmlElement(name = "NomeCategoria")
	private String nomeCategoria;

	@XmlElement(name = "ProdutoCampanha")
	private String produtoCampanha;

	@XmlElement(name = "IdClienteProduto")
	private String idClienteProduto;

	@XmlElement(name = "ProdutoCliente")
	private String produtoCliente;

	public Boolean getImagemVitrine() {
		return imagemVitrine;
	}

	public void setImagemVitrine(Boolean imagemVitrine) {
		this.imagemVitrine = imagemVitrine;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getExibePreco() {
		return exibePreco;
	}

	public void setExibePreco(Boolean exibePreco) {
		this.exibePreco = exibePreco;
	}

	public Long getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(Long idProduto) {
		this.idProduto = idProduto;
	}

	public WPCustomer getCliente() {
		return cliente;
	}

	public void setCliente(WPCustomer cliente) {
		this.cliente = cliente;
	}

	public WPCampaign getCampanha() {
		return campanha;
	}

	public void setCampanha(WPCampaign campanha) {
		this.campanha = campanha;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getPrecoDeReais() {
		return precoDeReais;
	}

	public void setPrecoDeReais(String precoDeReais) {
		this.precoDeReais = precoDeReais;
	}

	public String getPrecoDePontos() {
		return precoDePontos;
	}

	public void setPrecoDePontos(String precoDePontos) {
		this.precoDePontos = precoDePontos;
	}

	public String getPrecoDe() {
		return precoDe;
	}

	public void setPrecoDe(String precoDe) {
		this.precoDe = precoDe;
	}

	public String getPrecoPorReais() {
		return precoPorReais;
	}

	public void setPrecoPorReais(String precoPorReais) {
		this.precoPorReais = precoPorReais;
	}

	public String getPrecoPorPontos() {
		return precoPorPontos;
	}

	public void setPrecoPorPontos(String precoPorPontos) {
		this.precoPorPontos = precoPorPontos;
	}

	public String getPrecoPor() {
		return precoPor;
	}

	public void setPrecoPor(String precoPor) {
		this.precoPor = precoPor;
	}

	public String getValorComplementar() {
		return valorComplementar;
	}

	public void setValorComplementar(String valorComplementar) {
		this.valorComplementar = valorComplementar;
	}

	public List<String> getUrlsImagem() {
		return urlsImagem;
	}

	public void setUrlsImagem(List<String> urlsImagem) {
		this.urlsImagem = urlsImagem;
	}

	public List<WPProductSku> getProdutosSku() {
		return produtosSku;
	}

	public void setProdutosSku(List<WPProductSku> produtosSku) {
		this.produtosSku = produtosSku;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Long idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getNomeCategoria() {
		return nomeCategoria;
	}

	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}

	public String getProdutoCampanha() {
		return produtoCampanha;
	}

	public void setProdutoCampanha(String produtoCampanha) {
		this.produtoCampanha = produtoCampanha;
	}

	public String getIdClienteProduto() {
		return idClienteProduto;
	}

	public void setIdClienteProduto(String idClienteProduto) {
		this.idClienteProduto = idClienteProduto;
	}

	public String getProdutoCliente() {
		return produtoCliente;
	}

	public void setProdutoCliente(String produtoCliente) {
		this.produtoCliente = produtoCliente;
	}

	@Override
	public String toString() {
		return "WPProduto [imagemVitrine=" + imagemVitrine + ", ordem=" + ordem
				+ ", ativo=" + ativo + ", exibePreco=" + exibePreco
				+ ", idProduto=" + idProduto + ", cliente=" + cliente
				+ ", campanha=" + campanha + ", titulo=" + titulo
				+ ", precoDeReais=" + precoDeReais + ", precoDePontos="
				+ precoDePontos + ", precoDe=" + precoDe + ", precoPorReais="
				+ precoPorReais + ", precoPorPontos=" + precoPorPontos
				+ ", precoPor=" + precoPor + ", valorComplementar="
				+ valorComplementar + ", urlsImagem=" + urlsImagem
				+ ", produtosSku=" + produtosSku + ", descricao=" + descricao
				+ ", idCategoria=" + idCategoria + ", nomeCategoria="
				+ nomeCategoria + ", produtoCampanha=" + produtoCampanha
				+ ", idClienteProduto=" + idClienteProduto
				+ ", produtoCliente=" + produtoCliente + "]";
	}
}