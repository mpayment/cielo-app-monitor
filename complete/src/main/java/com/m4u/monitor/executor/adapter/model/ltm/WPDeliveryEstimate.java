package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.Date;
import javax.xml.bind.annotation.XmlAttribute;

public class WPDeliveryEstimate {

	@XmlAttribute(name="Cep")
	private String cep;
	
	@XmlAttribute(name="ValorReais")
	private double valorReais;
	
	@XmlAttribute(name="ValorPontos")
	private double valorPontos;
	
	@XmlAttribute(name="Valor")
	private double valor;
	
	@XmlAttribute(name="Previsao")
	private Date previsao;
	
	@XmlAttribute(name="QuantidadeDias")
	private int quantidadeDias;

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public double getValorReais() {
		return valorReais;
	}

	public void setValorReais(double valorReais) {
		this.valorReais = valorReais;
	}

	public double getValorPontos() {
		return valorPontos;
	}

	public void setValorPontos(double valorPontos) {
		this.valorPontos = valorPontos;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Date getPrevisao() {
		return previsao;
	}

	public void setPrevisao(Date previsao) {
		this.previsao = previsao;
	}

	public int getQuantidadeDias() {
		return quantidadeDias;
	}

	public void setQuantidadeDias(int quantidadeDias) {
		this.quantidadeDias = quantidadeDias;
	}

	@Override
	public String toString() {
		return "WPDeliveryEstimate [cep=" + cep + ", valorReais=" + valorReais
				+ ", valorPontos=" + valorPontos + ", valor=" + valor
				+ ", previsao=" + previsao + ", quantidadeDias="
				+ quantidadeDias + "]";
	}
}