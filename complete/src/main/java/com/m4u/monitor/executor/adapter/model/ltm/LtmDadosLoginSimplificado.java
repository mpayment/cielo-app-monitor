package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class LtmDadosLoginSimplificado {

	private String numeroEc;
	private String cnpj;

	public String getNumeroEc() {
		return numeroEc;
	}

	public void setNumeroEc(String numeroEc) {
		this.numeroEc = numeroEc;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	@Override
	public String toString() {
		return "LtmDadosLoginSimplificado [numeroEc=" + numeroEc + ", cnpj=" + cnpj + "]";
	}
}