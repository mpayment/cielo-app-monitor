package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPCheckBalanceRequest extends WPRequest {
	
	@XmlAttribute(name="Login")
	private String login;
	
	@XmlAttribute(name="IdCampanhaGestaoPontos")
	private Integer idCampanhaGestaoPontos;
	
	@XmlAttribute(name="IdEmpresaGestaoPontos")
	private Integer idEmpresaGestaoPontos;
	
	public WPCheckBalanceRequest() { }
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Integer getIdCampanhaGestaoPontos() {
		return idCampanhaGestaoPontos;
	}

	public void setIdCampanhaGestaoPontos(Integer idCampanhaGestaoPontos) {
		this.idCampanhaGestaoPontos = idCampanhaGestaoPontos;
	}

	public Integer getIdEmpresaGestaoPontos() {
		return idEmpresaGestaoPontos;
	}

	public void setIdEmpresaGestaoPontos(Integer idEmpresaGestaoPontos) {
		this.idEmpresaGestaoPontos = idEmpresaGestaoPontos;
	}

	@Override
	public String toString() {
		return "WPCheckBalanceRequest [login=" + login
				+ ", idCampanhaGestaoPontos=" + idCampanhaGestaoPontos
				+ ", idEmpresaGestaoPontos=" + idEmpresaGestaoPontos + "]" + super.toString();
	}
}