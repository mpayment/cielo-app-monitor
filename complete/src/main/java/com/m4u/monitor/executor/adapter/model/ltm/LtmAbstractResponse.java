package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;


@XmlAccessorType(XmlAccessType.FIELD)
public abstract class LtmAbstractResponse implements RemoteResponse {
	
	public static final int LTM_OK_CODE = 0;

	@XmlAttribute(name = "CodRetorno")
    private Integer codRetorno;

    @XmlAttribute(name = "MensagemRetorno")
    private String mensagemRetorno;
    
    @Override
    public boolean isResponseOk() {
    	return codRetorno.equals(LTM_OK_CODE);
    }
    
    @Override
    public String getResponseMessage() {
    	return mensagemRetorno;
    }
    
    @Override
	public String getResponseMessageText() {
		return mensagemRetorno;
	}
    
    @Override
	public String getResponseMessageCode() {
    	return codRetorno == null ? null : codRetorno.toString();
    }

	public Integer getCodRetorno() {
		return codRetorno;
	}

	public void setCodRetorno(Integer codRetorno) {
		this.codRetorno = codRetorno;
	}

	public String getMensagemRetorno() {
		return mensagemRetorno;
	}

	public void setMensagemRetorno(String mensagemRetorno) {
		this.mensagemRetorno = mensagemRetorno;
	}

	@Override
	public String toString() {
		return "LtmAbstractResponse [codRetorno=" + codRetorno
				+ ", mensagemRetorno=" + mensagemRetorno + "]";
	}
}