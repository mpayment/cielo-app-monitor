package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class WPEServOrder {

    @XmlAttribute(name="IdPedido")
    private String idPedido;

    @XmlElement(name="Usuario")
    private WPUser usuario;

    public String getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(String idPedido) {
        this.idPedido = idPedido;
    }

    public WPUser getUsuario() {
        return usuario;
    }

    public void setUsuario(WPUser usuario) {
        this.usuario = usuario;
    }

    @Override public String toString() {
        return "WPEServOrder{" +
                "idPedido='" + idPedido + '\'' +
                ", usuario=" + usuario +
                '}';
    }
}
