package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class WPUserAuthorizationResponse {
	
	@XmlAttribute(name="Login")
	private String login;
	
	@XmlAttribute(name="IdCampanha")
	private int idCampanha;
	
	@XmlAttribute(name="TokenSessao")
	private String tokenSessao;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public int getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(int idCampanha) {
		this.idCampanha = idCampanha;
	}

	public String getTokenSessao() {
		return tokenSessao;
	}

	public void setTokenSessao(String tokenSessao) {
		this.tokenSessao = tokenSessao;
	}

	@Override
	public String toString() {
		return "WPUserAuthorizationResponse [login=" + login + ", idCampanha="
				+ idCampanha + ", tokenSessao=" + tokenSessao + "]";
	}	
}
