package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Resultado")
@XmlAccessorType(XmlAccessType.NONE)
public class LtmCustomerStatusResponse {
	
	public static final String STATUS_ACTIVE = "ativo";
	public static final String STATUS_BLOCKED = "bloqueado";
	public static final String STATUS_AVAILABLE = "disponível";
	public static final String STATUS_INACTIVE = "inativo";
	public static final String STATUS_NOTFOUND = "Não localizado";

    @XmlAttribute(name = "IdCampanha")
    private Integer idCampanha;

    @XmlAttribute(name = "Status")
    private String status;

    @XmlAttribute(name = "SenhaTemporaria")
    private boolean senhaTemporaria;

    @XmlAttribute(name = "Login")
    private String login;

    public Integer getIdCampanha() {
        return idCampanha;
    }

    public void setIdCampanha(final Integer idCampanha) {
        this.idCampanha = idCampanha;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public boolean isSenhaTemporaria() {
        return senhaTemporaria;
    }

    public void setSenhaTemporaria(final boolean senhaTemporaria) {
        this.senhaTemporaria = senhaTemporaria;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "LtmCustomerStatusResponse{" +
                "idCampanha=" + idCampanha +
                ", status='" + status + '\'' +
                ", senhaTemporaria=" + senhaTemporaria +
                ", login='" + login + '\'' +
                '}';
    }
}