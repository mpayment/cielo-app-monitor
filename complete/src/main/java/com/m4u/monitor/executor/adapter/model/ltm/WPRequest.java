package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPRequest {
	
	@XmlAttribute(name="TokenAplicacao")
	private String tokenAplicacao;
	
	public String getTokenAplicacao() {
		return tokenAplicacao;
	}

	public void setTokenAplicacao(String tokenAplicacao) {
		this.tokenAplicacao = tokenAplicacao;
	}

	@Override
	public String toString() {
		return "WebPremiosRequest [tokenAplicacao=" + tokenAplicacao + "]";
	}
}