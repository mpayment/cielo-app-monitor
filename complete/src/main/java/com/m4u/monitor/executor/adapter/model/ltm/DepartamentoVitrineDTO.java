package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class DepartamentoVitrineDTO {

	@XmlElement(name = "Ativo")
	private boolean ativo;

	@XmlElement(name = "Departamento")
	private String departamento;

	@XmlElement(name = "IdDepartamento")
	private long idDepartamento;

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public long getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(long idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	@Override
	public String toString() {
		return "ShowcaseDepartmentDTO [ativo=" + ativo + ", departamento="
				+ departamento + ", idDepartamento=" + idDepartamento + "]";
	}
}