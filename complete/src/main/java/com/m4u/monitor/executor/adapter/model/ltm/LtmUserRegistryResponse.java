package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;

public class LtmUserRegistryResponse {

    @XmlAttribute(name = "Bairro")
    private String bairro;

    @XmlAttribute(name = "Cep")
    private String cep;

    @XmlAttribute(name = "Cidade")
    private String cidade;

    @XmlAttribute(name = "Cnpj")
    private String cnpj;

    @XmlAttribute(name = "Complemento")
    private String complemento;

    @XmlAttribute(name = "Cpf")
    private String cpf;

    @XmlAttribute(name = "DataNascimento")
    private XMLGregorianCalendar dataNascimento;

    @XmlAttribute(name = "DddTelefoneCelular")
    private String dddTelefoneCelular;

    @XmlAttribute(name = "DddTelefoneComercial")
    private String dddTelefoneComercial;

    @XmlAttribute(name = "DddTelefoneResidencial")
    private String dddTelefoneResidencial;

    @XmlAttribute(name = "Email")
    private String email;

    @XmlAttribute(name = "EnderecoComercial")
    private String enderecoComercial;

    @XmlAttribute(name = "Estado")
    private String estado;

    @XmlAttribute(name = "IdCargo")
    private int idCargo;

    @XmlAttribute(name = "IdOperadora")
    private int idOperadora;

    @XmlAttribute(name = "IdParticipante")
    private String idParticipante;

    @XmlAttribute(name = "Nome")
    private String nome;

    @XmlAttribute(name = "Numero")
    private String numero;

    @XmlAttribute(name = "Sexo")
    private String sexo;

    @XmlAttribute(name = "TelefoneCelular")
    private String telefoneCelular;

    @XmlAttribute(name = "TelefoneComercial")
    private String telefoneComercial;

    @XmlAttribute(name = "TelefoneResidencial")
    private String telefoneResidencial;

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = StringUtils.trim(cep);
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public XMLGregorianCalendar getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(XMLGregorianCalendar dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getDddTelefoneCelular() {
        return dddTelefoneCelular;
    }

    public void setDddTelefoneCelular(String dddTelefoneCelular) {
        this.dddTelefoneCelular = StringUtils.trim(dddTelefoneCelular);
    }

    public String getDddTelefoneComercial() {
        return dddTelefoneComercial;
    }

    public void setDddTelefoneComercial(String dddTelefoneComercial) {
        this.dddTelefoneComercial = StringUtils.trim(dddTelefoneComercial);
    }

    public String getDddTelefoneResidencial() {
        return dddTelefoneResidencial;
    }

    public void setDddTelefoneResidencial(String dddTelefoneResidencial) {
        this.dddTelefoneResidencial = StringUtils.trim(dddTelefoneResidencial);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEnderecoComercial() {
        return enderecoComercial;
    }

    public void setEnderecoComercial(String enderecoComercial) {
        this.enderecoComercial = enderecoComercial;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(int idCargo) {
        this.idCargo = idCargo;
    }

    public int getIdOperadora() {
        return idOperadora;
    }

    public void setIdOperadora(int idOperadora) {
        this.idOperadora = idOperadora;
    }

    public String getIdParticipante() {
        return idParticipante;
    }

    public void setIdParticipante(String idParticipante) {
        this.idParticipante = idParticipante;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTelefoneCelular() {
        return telefoneCelular;
    }

    public void setTelefoneCelular(String telefoneCelular) {
        this.telefoneCelular = StringUtils.trim(telefoneCelular);
    }

    public String getTelefoneComercial() {
        return telefoneComercial;
    }

    public void setTelefoneComercial(String telefoneComercial) {
        this.telefoneComercial = StringUtils.trim(telefoneComercial);
    }

    public String getTelefoneResidencial() {
        return telefoneResidencial;
    }

    public void setTelefoneResidencial(String telefoneResidencial) {
        this.telefoneResidencial = StringUtils.trim(telefoneResidencial);
    }

    @Override
    public String toString() {
        return "LtmMyRegistryResponse [bairro=" + bairro + ", cep=" + cep
                + ", cidade=" + cidade + ", cnpj=" + cnpj + ", complemento="
                + complemento + ", cpf=" + cpf + ", dataNascimento="
                + dataNascimento + ", dddTelefoneCelular=" + dddTelefoneCelular
                + ", dddTelefoneComercial=" + dddTelefoneComercial
                + ", dddTelefoneResidencial=" + dddTelefoneResidencial
                + ", email=" + email + ", enderecoComercial="
                + enderecoComercial + ", estado=" + estado + ", idCargo="
                + idCargo + ", idOperadora=" + idOperadora
                + ", idParticipante=" + idParticipante + ", nome=" + nome
                + ", numero=" + numero + ", sexo=" + sexo
                + ", telefoneCelular=" + telefoneCelular
                + ", telefoneComercial=" + telefoneComercial
                + ", telefoneResidencial=" + telefoneResidencial + "]";
    }
}
