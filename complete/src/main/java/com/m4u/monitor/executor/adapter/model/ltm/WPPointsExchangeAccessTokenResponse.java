package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;



public class WPPointsExchangeAccessTokenResponse implements RemoteResponse {
	
	@XmlAttribute(name="Status")
	private WPPointsExchangeAccessTokenStatusResponse status;	

	public WPPointsExchangeAccessTokenStatusResponse getStatus() {
		return status;
	}
	
	public void setStatus(WPPointsExchangeAccessTokenStatusResponse status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "WPPointsExchangeResponse [status=" + status + "]";
	}

	@Override
	public boolean isResponseOk() {
		if (status != null && status.isSuccess()) {
			return true;
		}
		return false;
	}

	@Override
	public String getResponseMessage() {
		if (status != null) {
			return status.getText();
		}
		return null;
	}

	@Override
	public String getResponseMessageText() {
		return getResponseMessage();
	}

	@Override
	public String getResponseMessageCode() {
		return getResponseMessage();
	}
}
