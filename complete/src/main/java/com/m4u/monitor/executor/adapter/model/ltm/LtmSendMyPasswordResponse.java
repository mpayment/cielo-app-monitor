package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.*;

@XmlRootElement(name="Response")
@XmlAccessorType(XmlAccessType.NONE)
public class LtmSendMyPasswordResponse {
	@XmlAttribute(name="EmailRetorno")
	private String emailRetorno;
	
	@XmlAttribute(name="IdCampanha")
	private Integer idCampanha;
	
	@XmlAttribute(name="Status")
	private Boolean status;

	
	public String getEmailRetorno() {
		return emailRetorno;
	}
	
	public void setEmailRetorno(String emailRetorno) {
		this.emailRetorno = emailRetorno;
	}
	
	public Integer getIdCampanha() {
		return idCampanha;
	}
	
	public void setIdCampanha(Integer idCampanha) {
		this.idCampanha = idCampanha;
	}
	
	public Boolean getStatus() {
		return status;
	}
	
	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "LtmSendMyPasswordResult [emailRetorno=" + emailRetorno
				+ ", idCampanha=" + idCampanha + ", status=" + status + "]";
	}
}
