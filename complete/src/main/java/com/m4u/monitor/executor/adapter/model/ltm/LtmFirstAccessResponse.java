package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.*;

@XmlRootElement(name="Resultado")
@XmlAccessorType(XmlAccessType.NONE)
public class LtmFirstAccessResponse {
	@XmlAttribute(name="IdParticipante")
	private Integer idParticipante;
	
	@XmlAttribute(name="IdCampanha")
	private Integer idCampanha;
	
	public Integer getIdParticipante() {
		return idParticipante;
	}
	
	public void setIdParticipante(Integer idParticipante) {
		this.idParticipante = idParticipante;
	}
	
	public Integer getIdCampanha() {
		return idCampanha;
	}
	
	public void setIdCampanha(Integer idCampanha) {
		this.idCampanha = idCampanha;
	}

	@Override
	public String toString() {
		return "LtmFirstAccessResult [idParticipante=" + idParticipante
				+ ", idCampanha=" + idCampanha + "]";
	}
}
