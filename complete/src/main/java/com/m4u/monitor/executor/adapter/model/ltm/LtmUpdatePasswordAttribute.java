package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Response")
@XmlAccessorType(XmlAccessType.NONE)
public class LtmUpdatePasswordAttribute {

    @XmlAttribute(name="Status")
    private Boolean status;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(final Boolean status) {
        this.status = status;
    }

    @Override public String toString() {
        return "LtmUpdatePasswordAttribute{" + "status=" + status + '}';
    }
}