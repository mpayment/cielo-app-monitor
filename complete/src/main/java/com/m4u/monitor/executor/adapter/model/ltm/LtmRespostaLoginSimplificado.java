package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class LtmRespostaLoginSimplificado {

    public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

    @XmlAttribute(name = "IdParticipante")
    private Long idParticipante;

    @XmlAttribute(name = "Senha")
    private String senha;

    @XmlAttribute(name = "Nome")
    private String nome;

    @XmlAttribute(name = "DataUltimoAcesso")
    private String dataUltimoAcesso;

    @XmlAttribute(name = "IdPerfil")
    private Integer idPerfil;

    @XmlAttribute(name = "Perfil")
    private String perfil;

    @XmlAttribute(name = "IdCampanha")
    private Integer idCampanha;

    @XmlAttribute(name = "AcessoWP")
    private Boolean acessoWP;

    @XmlAttribute(name = "IdCampanhaECommerceWP")
    private Integer idCampanhaECommerceWP;

    private Integer idCampanhaGestaoPontosWP;

    @XmlAttribute(name = "IdEmpresaGestaoPontosWP")
    private Integer idEmpresaGestaoPontosWP;

    private Integer idParceiroGestaoPontosWP;

    @XmlAttribute(name = "CodigoAcessoWP")
    private String codigoAcessoWP;

    @XmlAttribute(name = "UrlAcesso")
    private String urlAcesso;

    public Long getIdParticipante() {
        return idParticipante;
    }

    public void setIdParticipante(Long idParticipante) {
        this.idParticipante = idParticipante;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDataUltimoAcesso() {
        return dataUltimoAcesso;
    }

    public void setDataUltimoAcesso(String dataUltimoAcesso) {
        this.dataUltimoAcesso = dataUltimoAcesso;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public Integer getIdCampanha() {
        return idCampanha;
    }

    public void setIdCampanha(Integer idCampanha) {
        this.idCampanha = idCampanha;
    }

    public Boolean getAcessoWP() {
        return acessoWP;
    }

    public void setAcessoWP(Boolean acessoWP) {
        this.acessoWP = acessoWP;
    }

    public Integer getIdCampanhaECommerceWP() {
        return idCampanhaECommerceWP;
    }

    public void setIdCampanhaECommerceWP(Integer idCampanhaECommerceWP) {
        this.idCampanhaECommerceWP = idCampanhaECommerceWP;
    }

    public Integer getIdCampanhaGestaoPontosWP() {
        return idCampanhaGestaoPontosWP;
    }

    public void setIdCampanhaGestaoPontosWP(Integer idCampanhaGestaoPontosWP) {
        this.idCampanhaGestaoPontosWP = idCampanhaGestaoPontosWP;
    }

    public Integer getIdEmpresaGestaoPontosWP() {
        return idEmpresaGestaoPontosWP;
    }

    public void setIdEmpresaGestaoPontosWP(Integer idEmpresaGestaoPontosWP) {
        this.idEmpresaGestaoPontosWP = idEmpresaGestaoPontosWP;
    }

    public Integer getIdParceiroGestaoPontosWP() {
        return idParceiroGestaoPontosWP;
    }

    public void setIdParceiroGestaoPontosWP(Integer idParceiroGestaoPontosWP) {
        this.idParceiroGestaoPontosWP = idParceiroGestaoPontosWP;
    }

    public String getCodigoAcessoWP() {
        return codigoAcessoWP;
    }

    public void setCodigoAcessoWP(String codigoAcessoWP) {
        this.codigoAcessoWP = codigoAcessoWP;
    }

    public String getUrlAcesso() {
        return urlAcesso;
    }

    public void setUrlAcesso(String urlAcesso) {
        this.urlAcesso = urlAcesso;
    }

    @Override
    public String toString() {
        return "LtmRespostaLoginSimplificado [idParticipante=" + idParticipante + ", senha=" + senha + ", nome=" + nome
                + ", dataUltimoAcesso=" + dataUltimoAcesso + ", idPerfil=" + idPerfil + ", perfil=" + perfil + ", idCampanha=" + idCampanha
                + ", acessoWP=" + acessoWP + ", idCampanhaECommerceWP=" + idCampanhaECommerceWP + ", idCampanhaGestaoPontosWP="
                + idCampanhaGestaoPontosWP + ", idEmpresaGestaoPontosWP=" + idEmpresaGestaoPontosWP + ", idParceiroGestaoPontosWP="
                + idParceiroGestaoPontosWP + ", codigoAcessoWP=" + codigoAcessoWP + ", urlAcesso=" + urlAcesso + "]";
    }
}