package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;

public class WPEServCestaCompleta {

	@XmlElement(name="Cesta")
	private WPShoppingCart cesta;
	
	@XmlElement(name="Pagamento")
	@XmlList
	private List<WPPayment> pagamento;

	public WPShoppingCart getCesta() {
		return cesta;
	}

	public void setCesta(WPShoppingCart cesta) {
		this.cesta = cesta;
	}

	public List<WPPayment> getPagamento() {
		if (pagamento == null) {
			pagamento = new ArrayList<WPPayment>();
		}
		return pagamento;
	}

    @Override
    public String toString() {
        return "WPEServCestaCompleta{" +
                "cesta=" + cesta +
                ", pagamento=" + pagamento +
                '}';
    }
}
