package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.*;

@XmlRootElement(name="Resultado")
@XmlAccessorType(XmlAccessType.NONE)
public class LtmRegulation {
	@XmlAttribute(name="Regulamento")
	private String regulamento;

	public String getRegulamento() {
		return regulamento;
	}

	public void setRegulamento(String regulamento) {
		this.regulamento = regulamento;
	}

	@Override
	public String toString() {
		return "LtmRegulation [regulamento=" + regulamento + "]";
	}
	
	public String toResumedString() {
	    return "Regulation with " + regulamento.length() + " characters";
	}
}