package com.m4u.monitor.executor.adapter.model.cielo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class UsuarioCieloDTO {

	private Long id;
	private Integer cdStatusUsuario;
	private Integer cdTipoUsuario;
	private Integer cdFuncao;
	private String nomeIdentificacao;
	private String nomeLogin;
	private String nomeGrupo;
	private Long dataNascimento;
	private String nomeApelido;
	
	@XmlAttribute(name = "noemCidade")
	private String nomeCidade;
	
	private String cpf;
	private String rg;
	private String nomePessoa;
	private String email;
	private Integer quantidadeExpiracao;
	private Integer quantidadeInatividade;
	private String numeroTelefoneCelular;
	private String dddTelefoneCelular;
	private String telefoneComercial;
	private String dddTelefoneComercial;
	private Long dataCadastro;
	
	@XmlAttribute(name = "inAtivo")
	private Integer inAtivo;
	
	private Long dataUltimoAcesso;
	
	@XmlAttribute(name = "quantiadeTentativaReset")
	private Integer quantidateTentativaReset;
	
	private Long cdEntidade;
	
	private Long estabelecimentoComercial;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCdStatusUsuario() {
		return cdStatusUsuario;
	}

	public void setCdStatusUsuario(Integer cdStatusUsuario) {
		this.cdStatusUsuario = cdStatusUsuario;
	}

	public Integer getCdTipoUsuario() {
		return cdTipoUsuario;
	}

	public void setCdTipoUsuario(Integer cdTipoUsuario) {
		this.cdTipoUsuario = cdTipoUsuario;
	}

	public Integer getCdFuncao() {
		return cdFuncao;
	}

	public void setCdFuncao(Integer cdFuncao) {
		this.cdFuncao = cdFuncao;
	}

	public String getNomeIdentificacao() {
		return nomeIdentificacao;
	}

	public void setNomeIdentificacao(String nomeIdentificacao) {
		this.nomeIdentificacao = nomeIdentificacao;
	}

	public String getNomeLogin() {
		return nomeLogin;
	}

	public void setNomeLogin(String nomeLogin) {
		this.nomeLogin = nomeLogin;
	}

	public String getNomeGrupo() {
		return nomeGrupo;
	}

	public void setNomeGrupo(String nomeGrupo) {
		this.nomeGrupo = nomeGrupo;
	}

	public Long getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Long dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNomeApelido() {
		return nomeApelido;
	}

	public void setNomeApelido(String nomeApelido) {
		this.nomeApelido = nomeApelido;
	}

	public String getNomeCidade() {
		return nomeCidade;
	}

	public void setNomeCidade(String nomeCidade) {
		this.nomeCidade = nomeCidade;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getNomePessoa() {
		return nomePessoa;
	}

	public void setNomePessoa(String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getQuantidadeExpiracao() {
		return quantidadeExpiracao;
	}

	public void setQuantidadeExpiracao(Integer quantidadeExpiracao) {
		this.quantidadeExpiracao = quantidadeExpiracao;
	}

	public Integer getQuantidadeInatividade() {
		return quantidadeInatividade;
	}

	public void setQuantidadeInatividade(Integer quantidadeInatividade) {
		this.quantidadeInatividade = quantidadeInatividade;
	}

	public String getNumeroTelefoneCelular() {
		return numeroTelefoneCelular;
	}

	public void setNumeroTelefoneCelular(String numeroTelefoneCelular) {
		this.numeroTelefoneCelular = numeroTelefoneCelular;
	}

	public String getDddTelefoneCelular() {
		return dddTelefoneCelular;
	}

	public void setDddTelefoneCelular(String dddTelefoneCelular) {
		this.dddTelefoneCelular = dddTelefoneCelular;
	}

	public String getTelefoneComercial() {
		return telefoneComercial;
	}

	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

	public String getDddTelefoneComercial() {
		return dddTelefoneComercial;
	}

	public void setDddTelefoneComercial(String dddTelefoneComercial) {
		this.dddTelefoneComercial = dddTelefoneComercial;
	}

	public Long getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Long dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getInAtivo() {
		return inAtivo;
	}

	public void setInAtivo(Integer inAtivo) {
		this.inAtivo = inAtivo;
	}

	public Long getDataUltimoAcesso() {
		return dataUltimoAcesso;
	}

	public void setDataUltimoAcesso(Long dataUltimoAcesso) {
		this.dataUltimoAcesso = dataUltimoAcesso;
	}

	public Integer getQuantidateTentativaReset() {
		return quantidateTentativaReset;
	}

	public void setQuantidateTentativaReset(Integer quantidateTentativaReset) {
		this.quantidateTentativaReset = quantidateTentativaReset;
	}

	public Long getCdEntidade() {
		return cdEntidade;
	}

	public void setCdEntidade(Long cdEntidade) {
		this.cdEntidade = cdEntidade;
	}

	public Long getEstabelecimentoComercial() {
		return estabelecimentoComercial;
	}

	public void setEstabelecimentoComercial(Long estabelecimentoComercial) {
		this.estabelecimentoComercial = estabelecimentoComercial;
	}

	@Override
	public String toString() {
		return "CieloUserDTO [id=" + id + ", cdStatusUsuario="
				+ cdStatusUsuario + ", cdTipoUsuario=" + cdTipoUsuario
				+ ", cdFuncao=" + cdFuncao + ", nomeIdentificacao="
				+ nomeIdentificacao + ", nomeLogin=" + nomeLogin
				+ ", nomeGrupo=" + nomeGrupo + ", dataNascimento="
				+ dataNascimento + ", nomeApelido=" + nomeApelido
				+ ", nomeCidade=" + nomeCidade + ", cpf=" + cpf + ", rg=" + rg
				+ ", nomePessoa=" + nomePessoa + ", email=" + email
				+ ", quantidadeExpiracao=" + quantidadeExpiracao
				+ ", quantidadeInatividade=" + quantidadeInatividade
				+ ", numeroTelefoneCelular=" + numeroTelefoneCelular
				+ ", dddTelefoneCelular=" + dddTelefoneCelular
				+ ", telefoneComercial=" + telefoneComercial
				+ ", dddTelefoneComercial=" + dddTelefoneComercial
				+ ", dataCadastro=" + dataCadastro + ", inativo=" + inAtivo
				+ ", dataUltimoAcesso=" + dataUltimoAcesso
				+ ", quantidateTentativaReset=" + quantidateTentativaReset
				+ ", cdEntidade=" + cdEntidade + ", estabelecimentoComercial="
				+ estabelecimentoComercial + "]";
	}
}