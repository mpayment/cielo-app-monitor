package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class LtmResponse<T> extends LtmAbstractResponse {

    @XmlElement(name = "Resultado")
    private T resultado;

	public T getResultado() {
		return resultado;
	}

	public void setResultado(T resultado) {
		this.resultado = resultado;
	}

	@Override
	public String toString() {
		return "LtmResponse [resultado=" + resultado + "]" + super.toString();
	}
}