package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPProductGaleryImage {

	@XmlAttribute(name="ImagemPequena")
	private String imagemPequena;
	
	@XmlAttribute(name="ImagemGrande")
	private String imagemGrande;

	public String getImagemPequena() {
		return imagemPequena;
	}

	public void setImagemPequena(String imagemPequena) {
		this.imagemPequena = imagemPequena;
	}

	public String getImagemGrande() {
		return imagemGrande;
	}

	public void setImagemGrande(String imagemGrande) {
		this.imagemGrande = imagemGrande;
	}

	@Override
	public String toString() {
		return "WPProductGaleryImage [imagemPequena=" + imagemPequena + ", imaigemGrande=" + imagemGrande + "]";
	}
}