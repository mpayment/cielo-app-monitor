package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class LtmEmailValidationResponse {

    @XmlAttribute(name="Status")
    private boolean emailOK;
    
    @XmlAttribute(name="Sugestao")
    private String sugestao;

    public boolean isEmailOk() {
        return emailOK;
    }

    public void setEmailOk(boolean emailOK) {
        this.emailOK = emailOK;
    }

    public String getSugestao() {
        return sugestao;
    }

    public void setSugestao(String sugestao) {
        this.sugestao = sugestao;
    }

    @Override
    public String toString() {
        return "LtmEmailValidationResponse [emailOK=" + emailOK + ", sugestao=" + sugestao + "]";
    }
}