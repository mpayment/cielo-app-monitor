package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPUser {

    @XmlAttribute(name = "Login")
    private String login;

    @XmlAttribute(name = "CPF")
    private String cpf;

    @XmlAttribute(name = "IdUsuario")
    private Long idUsuario;

    @XmlAttribute(name = "Nome")
    private String nome;

    @XmlAttribute(name = "Senha")
    private String senha;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public String toString() {
        return "WPUser{" +
                "login='" + login + '\'' +
                ", cpf='" + cpf + '\'' +
                ", idUsuario=" + idUsuario +
                ", nome='" + nome + '\'' +
                '}';
    }
}
