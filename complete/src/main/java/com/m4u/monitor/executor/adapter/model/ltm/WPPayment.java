package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.*;

public class WPPayment {

	@XmlAttribute(name="IdPagamento")
	private int idPagamento;
	
	@XmlAttribute(name="ValorReais")
	private float valorReais;
	
	@XmlElement(name="TipoPagamento")
	private WPOrderCreationPaymentMethod tipoPagamento;

	public float getValorReais() {
		return valorReais;
	}

	public void setValorReais(float valorReais) {
		this.valorReais = valorReais;
	}

	public int getIdPagamento() {
		return idPagamento;
	}

	public void setIdPagamento(int idPagamento) {
		this.idPagamento = idPagamento;
	}

	public WPOrderCreationPaymentMethod getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(WPOrderCreationPaymentMethod tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	@Override
	public String toString() {
		return "WPPayment [valorReais=" + valorReais + ", idPagamento=" + idPagamento + ", tipoPagamento=" + tipoPagamento + "]";
	}
}