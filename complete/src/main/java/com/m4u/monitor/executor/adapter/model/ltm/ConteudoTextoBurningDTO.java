package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ConteudoTextoBurningDTO {

	@XmlElement(name = "Conteudo")
	private String conteudo;

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	@Override
	public String toString() {
		int length = conteudo == null ? 0 : conteudo.length();
		return "ConteudoTextoDTO [conteudo has " + length + " chars]";
	}
}