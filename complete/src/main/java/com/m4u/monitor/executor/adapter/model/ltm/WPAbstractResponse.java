package com.m4u.monitor.executor.adapter.model.ltm;



import javax.xml.bind.annotation.XmlAttribute;

import org.apache.commons.lang3.StringUtils;

public abstract class WPAbstractResponse implements RemoteResponse {
	
	private static final String USER_SESSION_EXPIRED_ERROR_CODE = "300";

	@XmlAttribute(name = "Sucesso")
	private boolean sucesso;

	@XmlAttribute(name = "MsgErro")
	private String msgErro;

	@XmlAttribute(name = "MensagemException")
	private String mensagemException;

	@XmlAttribute(name = "IdErro")
	private String idErro;

	@Override
	public boolean isResponseOk() {
		return sucesso;
	}

	@Override
	public String getResponseMessage() {
		return msgErro;
	}

	@Override
	public String getResponseMessageText() {
		return getResponseMessage();
	}

	@Override
	public String getResponseMessageCode() {
		return getResponseMessage();
	}
	
	public boolean isUserSessionExpiredError() {
		return USER_SESSION_EXPIRED_ERROR_CODE.equals(StringUtils.trim(idErro));
	}

	public boolean isSucesso() {
		return sucesso;
	}

	public void setSucesso(boolean sucesso) {
		this.sucesso = sucesso;
	}

	public String getMsgErro() {
		return msgErro;
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}

	public String getMensagemException() {
		return mensagemException;
	}

	public void setMensagemException(String mensagemException) {
		this.mensagemException = mensagemException;
	}

	public String getIdErro() {
		return idErro;
	}

	public void setIdErro(String idErro) {
		this.idErro = idErro;
	}

	@Override
	public String toString() {
		return String.format("WPAbstractResponse [sucesso=%s, msgErro=%s, mensagemException=%s, idErro=%s]",
				sucesso, msgErro, mensagemException, idErro);
	}
}