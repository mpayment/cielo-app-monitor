package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class LtmSendMyPasswordRequest {
    @XmlAttribute(name="Login")
    private String cnpj;

    @XmlAttribute(name="CodigoParceiro")
    private Integer codigoParceiro;

    @XmlAttribute(name="LinkApp")
    private String linkApp;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(final String cnpj) {
        this.cnpj = cnpj;
    }

    public Integer getCodigoParceiro() {
        return codigoParceiro;
    }

    public void setCodigoParceiro(final Integer codigoParceiro) {
        this.codigoParceiro = codigoParceiro;
    }

    public String getLinkApp() {
        return linkApp;
    }

    public void setLinkApp(final String linkApp) {
        this.linkApp = linkApp;
    }

    @Override public String toString() {
        return "LtmSendMyPasswordRequest{" +
                "cnpj='" + cnpj + '\'' +
                ", codigoParceiro=" + codigoParceiro +
                ", linkApp='" + linkApp + '\'' +
                '}';
    }
}