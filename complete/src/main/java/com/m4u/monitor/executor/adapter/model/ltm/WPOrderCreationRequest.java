package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class WPOrderCreationRequest extends WPRequest {

    @XmlAttribute(name = "BuscarPedido")
    private boolean buscarPedido;

    @XmlAttribute(name = "DeviceFingerprintID")
    private boolean deviceFingerprintID;

    @XmlAttribute(name = "TemEnderecoEntrega")
    private boolean temEnderecoEntrega;

    @XmlAttribute(name = "TemFrete")
    private boolean temFrete;

    @XmlAttribute(name = "TemAntiFraude")
    private boolean temAntiFraude;

    @XmlElement(name = "EServToken")
    private WPEServToken eServToken;

    @XmlElement(name = "EServCestaCompleta")
    private WPEServCestaCompleta eServCestaCompleta;

    public boolean isBuscarPedido() {
        return buscarPedido;
    }

    public void setBuscarPedido(boolean buscarPedido) {
        this.buscarPedido = buscarPedido;
    }

    public boolean isDeviceFingerprintID() {
        return deviceFingerprintID;
    }

    public void setDeviceFingerprintID(boolean deviceFingerprintID) {
        this.deviceFingerprintID = deviceFingerprintID;
    }

    public boolean isTemEnderecoEntrega() {
        return temEnderecoEntrega;
    }

    public void setTemEnderecoEntrega(boolean temEnderecoEntrega) {
        this.temEnderecoEntrega = temEnderecoEntrega;
    }

    public boolean isTemFrete() {
        return temFrete;
    }

    public void setTemFrete(boolean temFrete) {
        this.temFrete = temFrete;
    }

    public boolean isTemAntiFraude() {
        return temAntiFraude;
    }

    public void setTemAntiFraude(boolean temAntiFraude) {
        this.temAntiFraude = temAntiFraude;
    }

    public WPEServToken getEServToken() {
        return eServToken;
    }

    public void setEServToken(WPEServToken eServToken) {
        this.eServToken = eServToken;
    }

    public WPEServCestaCompleta getEServCestaCompleta() {
        return eServCestaCompleta;
    }

    public void setEServCestaCompleta(WPEServCestaCompleta eServCestaCompleta) {
        this.eServCestaCompleta = eServCestaCompleta;
    }

    @Override public String toString() {
        return "WPOrderCreationRequest{" +
                "buscarPedido=" + buscarPedido +
                ", deviceFingerprintID=" + deviceFingerprintID +
                ", temEnderecoEntrega=" + temEnderecoEntrega +
                ", temFrete=" + temFrete +
                ", temAntiFraude=" + temAntiFraude +
                ", eServToken=" + eServToken +
                ", eServCestaCompleta=" + eServCestaCompleta +
                '}' + super.toString();
    }
}
