package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
public class SorteioBurningDTO {

	@XmlElement(name = "IdBurningSorteio")
	private long idBurningSorteio;
	
	@XmlElement(name = "ImagemPack")
	private String imagemPack;
	
	@XmlElement(name = "DataSorteio")
	private XMLGregorianCalendar dataSorteio;
	
	@XmlElement(name = "DataInicioResgate")
	private XMLGregorianCalendar dataInicioResgate;
	
	@XmlElement(name = "DataFimResgate")
	private XMLGregorianCalendar dataFimResgate;
	
	@XmlElement(name = "BurningGanhadores")
	private List<GanhadorSorteioBurningDTO> burningGanhadores;
	
	@XmlElement(name = "CupomOnline")
	private boolean cupomOnline;
	
	@XmlElement(name = "Serie")
	private String serie;

	public long getIdBurningSorteio() {
		return idBurningSorteio;
	}

	public void setIdBurningSorteio(long idBurningSorteio) {
		this.idBurningSorteio = idBurningSorteio;
	}

	public XMLGregorianCalendar getDataSorteio() {
		return dataSorteio;
	}

	public void setDataSorteio(XMLGregorianCalendar dataSorteio) {
		this.dataSorteio = dataSorteio;
	}

	public String getImagemPack() {
		return imagemPack;
	}

	public void setImagemPack(String imagemPack) {
		this.imagemPack = imagemPack;
	}

	public XMLGregorianCalendar getDataInicioResgate() {
		return dataInicioResgate;
	}

	public void setDataInicioResgate(XMLGregorianCalendar dataInicioResgate) {
		this.dataInicioResgate = dataInicioResgate;
	}

	public XMLGregorianCalendar getDataFimResgate() {
		return dataFimResgate;
	}

	public void setDataFimResgate(XMLGregorianCalendar dataFimResgate) {
		this.dataFimResgate = dataFimResgate;
	}

	public List<GanhadorSorteioBurningDTO> getBurningGanhadores() {
		return burningGanhadores;
	}

	public void setBurningGanhadores(List<GanhadorSorteioBurningDTO> burningGanhadores) {
		this.burningGanhadores = burningGanhadores;
	}

	public boolean isCupomOnline() {
		return cupomOnline;
	}

	public void setCupomOnline(boolean cupomOnline) {
		this.cupomOnline = cupomOnline;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	@Override
	public String toString() {
		return "SorteioBurningDTO [idBurningSorteio=" + idBurningSorteio
				+ ", dataSorteio=" + dataSorteio + ", imagemPack=" + imagemPack
				+ ", dataInicioResgate=" + dataInicioResgate
				+ ", dataFimResgate=" + dataFimResgate + ", burningGanhadores="
				+ burningGanhadores + ", cupomOnline=" + cupomOnline
				+ ", serie=" + serie + "]";
	}
}