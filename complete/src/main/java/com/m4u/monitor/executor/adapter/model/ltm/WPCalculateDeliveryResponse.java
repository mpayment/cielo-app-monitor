package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.List;
import javax.xml.bind.annotation.*;

public class WPCalculateDeliveryResponse {

	@XmlAttribute(name="Resultado")
	@XmlList
	private List<WPDeliveryEstimate> resultado;

	public List<WPDeliveryEstimate> getResultado() {
		return resultado;
	}

	public void setResultado(List<WPDeliveryEstimate> resultado) {
		this.resultado = resultado;
	}

	@Override
	public String toString() {
		return "WPCalculateDeliveryResponse [resultado=" + resultado + "]";
	}
}