package com.m4u.monitor.executor.adapter.model.cielo;

public class RespostaTaxasAntecipacaoAutomaticaDTO {

	private Boolean elegivel;
	private Float taxaPadrao;
	private Float taxaPromocional;

	public Boolean getElegivel() {
		return elegivel;
	}

	public void setElegivel(Boolean elegivel) {
		this.elegivel = elegivel;
	}

	public Float getTaxaPadrao() {
		return taxaPadrao;
	}

	public void setTaxaPadrao(Float taxaPadrao) {
		this.taxaPadrao = taxaPadrao;
	}

	public Float getTaxaPromocional() {
		return taxaPromocional;
	}

	public void setTaxaPromocional(Float taxaPromocional) {
		this.taxaPromocional = taxaPromocional;
	}
}