package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPDadosPessoais {
	
	@XmlAttribute(name="Logradouro") //OBRIGATÓRIO
	private String logradouro;
	
	@XmlAttribute(name="Numero") //OBRIGATÓRIO
	private Long numero;
	
	@XmlAttribute(name="Complemento") //OBRIGATÓRIO
	private String complemento;
	
	@XmlAttribute(name="Bairro") //OBRIGATÓRIO
	private String bairro;
	
	@XmlAttribute(name="Cidade") //OBRIGATÓRIO
	private String cidade;
	
	@XmlAttribute(name="Estado") //OBRIGATÓRIO
	private String estado;
	
	@XmlAttribute(name="Cep") //OBRIGATÓRIO
	private String cep;
	
	@XmlAttribute(name="Email") //OBRIGATÓRIO
	private String email;
	
	@XmlAttribute(name="EmailComercial") //OBRIGATÓRIO
	private String emailComercial;
	
	@XmlAttribute(name="DDDTelResidencial") //OBRIGATÓRIO
	private String dddTelResidencial;
	
	@XmlAttribute(name="TelResidencial") //OBRIGATÓRIO
	private String telResidencial;
	
	@XmlAttribute(name="TelResRamal") //OPCIONAL
	private String telResRamal;
	
	@XmlAttribute(name="DDDTelComercial") //OBRIGATÓRIO
	private String dddTelComercial;
	
	@XmlAttribute(name="TelComercial") //OBRIGATÓRIO
	private String telComercial;
	
	@XmlAttribute(name="TelComRamal") //OPCIONAL
	private String telComRamal;
	
	@XmlAttribute(name="DDDCelular") //OBRIGATÓRIO
	private String dddCelular;
	
	@XmlAttribute(name="Celular") //OBRIGATÓRIO
	private String celular;
	
	@XmlAttribute(name="Referencia") //OPCIONAL
	private String referencia;
	
	@XmlAttribute(name="InscricaoMunicipal") //OPCIONAL
	private String inscricaoMunicipal;
	
	@XmlAttribute(name="InscricaoEstadual") //OPCIONAL
	private String inscricaoEstadual;

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailComercial() {
		return emailComercial;
	}

	public void setEmailComercial(String emailComercial) {
		this.emailComercial = emailComercial;
	}

	public String getDddTelResidencial() {
		return dddTelResidencial;
	}

	public void setDddTelResidencial(String dddTelResidencial) {
		this.dddTelResidencial = dddTelResidencial;
	}

	public String getTelResidencial() {
		return telResidencial;
	}

	public void setTelResidencial(String telResidencial) {
		this.telResidencial = telResidencial;
	}

	public String getTelResRamal() {
		return telResRamal;
	}

	public void setTelResRamal(String telResRamal) {
		this.telResRamal = telResRamal;
	}

	public String getDddTelComercial() {
		return dddTelComercial;
	}

	public void setDddTelComercial(String dddTelComercial) {
		this.dddTelComercial = dddTelComercial;
	}

	public String getTelComercial() {
		return telComercial;
	}

	public void setTelComercial(String telComercial) {
		this.telComercial = telComercial;
	}

	public String getTelComRamal() {
		return telComRamal;
	}

	public void setTelComRamal(String telComRamal) {
		this.telComRamal = telComRamal;
	}

	public String getDddCelular() {
		return dddCelular;
	}

	public void setDddCelular(String dddCelular) {
		this.dddCelular = dddCelular;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	@Override
	public String toString() {
		return "WPDadosPessoais [logradouro=" + logradouro + ", numero="
				+ numero + ", complemento=" + complemento + ", bairro="
				+ bairro + ", cidade=" + cidade + ", estado=" + estado
				+ ", cep=" + cep + ", email=" + email + ", emailComercial="
				+ emailComercial + ", dddTelResidencial=" + dddTelResidencial
				+ ", telResidencial=" + telResidencial + ", telResRamal="
				+ telResRamal + ", dddTelComercial=" + dddTelComercial
				+ ", telComercial=" + telComercial + ", telComRamal="
				+ telComRamal + ", dddCelular=" + dddCelular + ", celular="
				+ celular + ", referencia=" + referencia
				+ ", inscricaoMunicipal=" + inscricaoMunicipal
				+ ", inscricaoEstadual=" + inscricaoEstadual + "]";
	}
	
}
