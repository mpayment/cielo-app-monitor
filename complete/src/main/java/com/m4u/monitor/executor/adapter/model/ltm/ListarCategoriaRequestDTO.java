package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ListarCategoriaRequestDTO extends WPRequest {

	@XmlElement(name="EServToken")
	private WPEServToken eServToken;
	
	@XmlElement(name = "IdSecao")
	private Long idSecao;

	public WPEServToken geteServToken() {
		return eServToken;
	}

	public void seteServToken(WPEServToken eServToken) {
		this.eServToken = eServToken;
	}

	public Long getIdSecao() {
		return idSecao;
	}

	public void setIdSecao(Long idSecao) {
		this.idSecao = idSecao;
	}

	@Override
	public String toString() {
		return "ListarCategoriaRequestDTO [eServToken=" + eServToken
				+ ", idSecao=" + idSecao + "]";
	}
}