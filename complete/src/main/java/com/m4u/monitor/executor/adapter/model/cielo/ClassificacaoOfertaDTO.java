package com.m4u.monitor.executor.adapter.model.cielo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class ClassificacaoOfertaDTO {

	private int codigoClassOferta;
	private String nomeClassOferta;
	private String ativo;
	private String tipo;
	private String urlBanner;
	private String urlLogotipo;

	public int getCodigoClassOferta() {
		return codigoClassOferta;
	}

	public void setCodigoClassOferta(int codigoClassOferta) {
		this.codigoClassOferta = codigoClassOferta;
	}

	public String getNomeClassOferta() {
		return nomeClassOferta;
	}

	public void setNomeClassOferta(String nomeClassOferta) {
		this.nomeClassOferta = nomeClassOferta;
	}

	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getUrlBanner() {
		return urlBanner;
	}

	public void setUrlBanner(String urlBanner) {
		this.urlBanner = urlBanner;
	}

	public String getUrlLogotipo() {
		return urlLogotipo;
	}

	public void setUrlLogotipo(String urlLogotipo) {
		this.urlLogotipo = urlLogotipo;
	}
}