package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.NONE)
public class LtmLoginRequest {

    @XmlAttribute(name = "Login")
    private String login;

    @XmlAttribute(name = "Senha")
    private String senha;

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(final String senha) {
        this.senha = senha;
    }

    @Override public String toString() {
        return "LtmLoginRequest{" +
                "login='" + login + '\'' +
                ", senha='" + senha + '\'' +
                '}';
    }
}