package com.m4u.monitor.executor.adapter.model.cielo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement(name = "retorno")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidarBancoResponseDTO implements RemoteResponse {

	public static final int RESPONSE_CODE_OK = 200;
	public static final int RESPONSE_ESTABLISHMENT_NOT_FOUND = 635;
	public static final int RESPONSE_BAD_DATA = 618;
	public static final int RESPONSE_BLANK_DATA = 617;
	
	@XmlElement(name = "propriedade")
	private Boolean propriedade;

	@XmlElement(name = "codigo")
	private Integer codigo;

	@XmlElement(name = "mensagem")
	private String mensagem;

	public Boolean getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Boolean propriedade) {
		this.propriedade = propriedade;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	@Override
	public String getResponseMessage() {
		return codigo == null ? null : codigo.toString();
	}

	@Override
	public String getResponseMessageText() {
		return mensagem;
	}

	@Override
	public String getResponseMessageCode() {
		return codigo == null ? null : codigo.toString();
	}
	
	@Override
	public boolean isResponseOk() {
		final Integer codigo = getCodigo();

		if (codigo == null) {
			return false;
		}
		
		return codigo.equals(RESPONSE_CODE_OK) ||
				codigo.equals(RESPONSE_BLANK_DATA) ||
				codigo.equals(RESPONSE_BAD_DATA) ||
				codigo.equals(RESPONSE_ESTABLISHMENT_NOT_FOUND);
	}
}