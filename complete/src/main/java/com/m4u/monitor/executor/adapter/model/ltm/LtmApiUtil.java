package com.m4u.monitor.executor.adapter.model.ltm;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.m4u.monitor.commons.service.ServerConfigurationService;
import com.m4u.monitor.commons.util.constants.ServerConfigurationKey;





@Component
public class LtmApiUtil {
	
	public static final String LTM_API_KEY_HEADER_NAME = "X-CIELO-API-Key";
	private static final String POS_STORE_EMPTY_RESPONSE_MESSAGE = "Vazio";

	@Autowired
	private ServerConfigurationService serverConfigurationService;
	
	public String getApiKey() {
    	return serverConfigurationService.getConfiguration(ServerConfigurationKey.CIELO_API_KEY);
    }
	
	public static boolean isOkButEmptyPosResponse(LtmResponse<?> response) {
		if (response.isResponseOk()) {
			return false;
		}

		String responseMessage = StringUtils.trimToNull(response.getMensagemRetorno());
		return POS_STORE_EMPTY_RESPONSE_MESSAGE.equalsIgnoreCase(responseMessage);
	}
}