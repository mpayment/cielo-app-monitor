package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.NONE)
public class LtmUpdatePasswordRequest {

    @XmlAttribute(name = "IdParticipante")
    private String idParticipante;

    @XmlAttribute(name = "Senha")
    private String senha;

    @XmlAttribute(name = "SenhaAtual")
    private String senhaAtual;

    @XmlAttribute(name = "Newsletter")
    private String newsletter;

    public String getIdParticipante() {
        return idParticipante;
    }

    public void setIdParticipante(final String idParticipante) {
        this.idParticipante = idParticipante;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(final String senha) {
        this.senha = senha;
    }

    public String getSenhaAtual() {
        return senhaAtual;
    }

    public void setSenhaAtual(final String senhaAtual) {
        this.senhaAtual = senhaAtual;
    }

    public String getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(final String newsletter) {
        this.newsletter = newsletter;
    }

	@Override
	public String toString() {
		return "LtmUpdatePasswordRequest [idParticipante=" + idParticipante
				+ ", senha=" + senha + ", senhaAtual=" + senhaAtual
				+ ", newsletter=" + newsletter + "]";
	}
}