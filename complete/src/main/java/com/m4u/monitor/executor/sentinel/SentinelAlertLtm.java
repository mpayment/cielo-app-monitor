package com.m4u.monitor.executor.sentinel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.m4u.monitor.commons.service.email.EmailWrapper;
import com.m4u.monitor.commons.util.Util;


@Component
public class SentinelAlertLtm {

	@Autowired
	private EmailWrapper emailW;

	
	public boolean alertLtmOff(){
		emailW.setSubject("[CieloApp] API LTM >> OFFLINE << [HOMOLOG] " + Util.timeNow());
		emailW.setText(ltmOffText());
		emailW.send();
		return true;
	}
	
	public boolean alertLtmOn(){
		emailW.setSubject("[CieloApp] API LTM > ONLINE < [HOMOLOG] " + Util.timeNow());
		emailW.setText(ltmOnText());
		emailW.send();
		return true;
	}
	
	public String ltmOffText(){
		String body;
		body = "============================================================================\n"             
			 + "           CieloApp - API Monitor - Alerta Automático de Disponibilidade de Serviços  \n"
			 + "============================================================================\n"
			 + " 																		    \n"
			 + " 				   API LTM HOMOLOGAÇÃO - Plataforma OFFLINE 				\n"
			 + "                                                                            \n"
			 + "                 Todos os serviços da LTM estão indisponíveis               \n"
			 + " 																		    \n"
			 + "           (*) Uma nova notificação será enviada quando o status mudar para ONLINE    \n"
			 + " 																		    \n"			 
			 + "============================================================================\n"
			 + "                                      M4U Produtos & Serviços (c)2016		        \n"
		     + "============================================================================";
		return body;
	}

	public String ltmOnText(){
		String body;
		body = "============================================================================\n"             
			 + "           CieloApp - API Monitor - Alerta Automático de Disponibilidade de Serviços  \n"
			 + "============================================================================\n"
			 + " 																		    \n"
			 + " 	  			   API LTM HOMOLOGAÇÃO - Plataforma ONLINE  					\n"  
			 + " 																		    \n"
			 + " 																		    \n"			 
			 + "============================================================================\n"
			 + "                                      M4U Produtos & Serviços (c)2016		        \n"
		     + "============================================================================";
		return body;
	}

	
}
