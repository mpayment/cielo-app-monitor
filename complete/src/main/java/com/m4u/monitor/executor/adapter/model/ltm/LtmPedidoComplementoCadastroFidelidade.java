package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.m4u.monitor.commons.util.constants.CieloConstants;



@XmlAccessorType(XmlAccessType.FIELD)
public class LtmPedidoComplementoCadastroFidelidade {

	public static final String DATE_TIME_PATTERN = CieloConstants.ISO_DATE_TIME_PATTERN;

	private String numeroEc;
	private String cnpj;
	private String cpf;
	private String dataNascimento;
	private String dataAceiteRegulamento;

	public String getNumeroEc() {
		return numeroEc;
	}

	public void setNumeroEc(String numeroEc) {
		this.numeroEc = numeroEc;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getDataAceiteRegulamento() {
		return dataAceiteRegulamento;
	}

	public void setDataAceiteRegulamento(String dataAceiteRegulamento) {
		this.dataAceiteRegulamento = dataAceiteRegulamento;
	}

	@Override
	public String toString() {
		return "LtmPedidoComplementoCadastroFidelidade [numeroEc=" + numeroEc + ", cnpj=" + cnpj + ", cpf="
				+ cpf + ", dataNascimento=" + dataNascimento + ", dataAceiteRegulamento="
				+ dataAceiteRegulamento + "]";
	}
}