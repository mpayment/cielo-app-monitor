package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class WPBuscaProdutoResponse {

	@XmlElement(name = "Produtos")
	private List<WPProduto> produtos;

	public List<WPProduto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<WPProduto> produtos) {
		this.produtos = produtos;
	}

	@Override
	public String toString() {
		return "WPBuscaProdutoResponse [produtos=" + produtos + "]";
	}
}