package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlElement;

public class LtmNavigationLogSavingResponse {

    @XmlElement(name = "Status")
    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "LtmNavigationLogSavingResponse [status=" + status + "]";
    }
}