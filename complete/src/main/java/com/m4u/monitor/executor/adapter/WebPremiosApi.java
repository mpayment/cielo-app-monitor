package com.m4u.monitor.executor.adapter;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.m4u.monitor.executor.adapter.model.ltm.ListarCategoriaRequestDTO;
import com.m4u.monitor.executor.adapter.model.ltm.ListarCategoriaResponseDTO;
import com.m4u.monitor.executor.adapter.model.ltm.ListarDepartamentoRequestDTO;
import com.m4u.monitor.executor.adapter.model.ltm.ListarDepartamentoResponseDTO;
import com.m4u.monitor.executor.adapter.model.ltm.WPBalance;
import com.m4u.monitor.executor.adapter.model.ltm.WPBuscaProdutoRequest;
import com.m4u.monitor.executor.adapter.model.ltm.WPBuscaProdutoResponse;
import com.m4u.monitor.executor.adapter.model.ltm.WPCalculateDeliveryRequest;
import com.m4u.monitor.executor.adapter.model.ltm.WPCalculateDeliveryResponse;
import com.m4u.monitor.executor.adapter.model.ltm.WPCheckBalanceRequest;
import com.m4u.monitor.executor.adapter.model.ltm.WPListPaymentMethodRequest;
import com.m4u.monitor.executor.adapter.model.ltm.WPListPaymentMethodResponse;
import com.m4u.monitor.executor.adapter.model.ltm.WPOrderCreationRequest;
import com.m4u.monitor.executor.adapter.model.ltm.WPOrderCreationResponse;
import com.m4u.monitor.executor.adapter.model.ltm.WPPartnerAuthorizationRequest;
import com.m4u.monitor.executor.adapter.model.ltm.WPPartnerAuthorizationResponse;
import com.m4u.monitor.executor.adapter.model.ltm.WPPartnerTokenValidationResponse;
import com.m4u.monitor.executor.adapter.model.ltm.WPPointsExchangeAccessTokenRequest;
import com.m4u.monitor.executor.adapter.model.ltm.WPPointsExchangeAccessTokenResponse;
import com.m4u.monitor.executor.adapter.model.ltm.WPResponse;
import com.m4u.monitor.executor.adapter.model.ltm.WPShowcaseProductDetailRequest;
import com.m4u.monitor.executor.adapter.model.ltm.WPShowcaseProductDetailResponse;
import com.m4u.monitor.executor.adapter.model.ltm.WPShowcaseRequest;
import com.m4u.monitor.executor.adapter.model.ltm.WPShowcaseResponse;
import com.m4u.monitor.executor.adapter.model.ltm.WPUserAuthorizationRequest;
import com.m4u.monitor.executor.adapter.model.ltm.WPUserAuthorizationResponse;



@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface WebPremiosApi {
	
	@POST
	@Path("api/buscarproduto")
	WPResponse<WPBuscaProdutoResponse> buscarProduto(WPBuscaProdutoRequest buscaProdutoRequest);
	
	@POST
	@Path("Api/ListarDepartamento")
	WPResponse<ListarDepartamentoResponseDTO> listarDepartamento(ListarDepartamentoRequestDTO request);
	
	@POST
	@Path("Api/ListarCategoria")
	WPResponse<ListarCategoriaResponseDTO> listarCategoria(ListarCategoriaRequestDTO request);

	@POST
	@Path("Mobicare/ListarVitrine")
	WPResponse<WPShowcaseResponse> showcase(WPShowcaseRequest request);
	
	@POST
	@Path("Mobicare/DetalharProduto")
	WPResponse<WPShowcaseProductDetailResponse> getShowcaseProduct(WPShowcaseProductDetailRequest request);
	
	@POST
	@Path("api/VerificarSaldo")
	WPResponse<WPBalance> checkBalance(WPCheckBalanceRequest request);
	
	@POST
	@Path("api/AutorizarParticipante")
	WPResponse<WPUserAuthorizationResponse> authorizeUser(WPUserAuthorizationRequest request);

	@POST
	@Path("api/AutorizarParceiroExterno")
	WPResponse<WPPartnerAuthorizationResponse> authorizePartner(WPPartnerAuthorizationRequest request);
	
	@POST
	@Path("api/ValidarTokenAcesso")
	WPResponse<WPPartnerTokenValidationResponse> validatePartnerToken(@QueryParam("url") String url, 
			@QueryParam("idSessao") String idSessao, @QueryParam("idCampanha") Integer idCampanha);
	
	@POST
	@Path("api/InserirPedido")
	WPResponse<WPOrderCreationResponse> createOrder(WPOrderCreationRequest request);
	
	@POST
	@Path("api/ListarFormaPagamento")
	WPResponse<WPListPaymentMethodResponse> getPaymentMethodList(WPListPaymentMethodRequest request);
	
	@POST
	@Path("api/CalcularFrete")
	WPResponse<WPCalculateDeliveryResponse> calculateDelivery(WPCalculateDeliveryRequest request);
	
	@POST
	@Path("UmBarato/GetAccessToken")
	WPPointsExchangeAccessTokenResponse getAccessToken(WPPointsExchangeAccessTokenRequest request);
}