package com.m4u.monitor.executor.adapter.model.cielo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class ListaClassificacoesOfertasResponseDTO {

	private List<ClassificacaoOfertaDTO> classificacoesOferta;

	public List<ClassificacaoOfertaDTO> getClassificacoesOferta() {
		return classificacoesOferta;
	}

	public void setClassificacoesOferta(List<ClassificacaoOfertaDTO> classificacoesOferta) {
		this.classificacoesOferta = classificacoesOferta;
	}
}