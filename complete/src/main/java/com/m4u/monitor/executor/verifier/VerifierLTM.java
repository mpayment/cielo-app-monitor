package com.m4u.monitor.executor.verifier;

import javax.ws.rs.ServiceUnavailableException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.m4u.monitor.commons.service.ServerConfigurationService;
import com.m4u.monitor.commons.util.Util;
import com.m4u.monitor.executor.adapter.model.ltm.LtmDadosLoginSimplificado;
import com.m4u.monitor.executor.adapter.model.ltm.LtmResponse;
import com.m4u.monitor.executor.adapter.model.ltm.LtmRespostaLoginSimplificado;
import com.m4u.monitor.executor.service.LtmService;
import com.m4u.monitor.model.MonitorApiDTO;


@Component
public class VerifierLTM {

	@Autowired
	private LtmService ltmService;
	
	@Autowired
	private Util nMsg;
	
	@Autowired
	private ServerConfigurationService srvCfgService;
	
	
	
	public MonitorApiDTO verifyPlataform(MonitorApiDTO matrix) {
		

		LtmDadosLoginSimplificado dadosLogin = new LtmDadosLoginSimplificado();
		dadosLogin.setCnpj(matrix.getEc_nuCnpj());
		dadosLogin.setNumeroEc(matrix.getEc_nuEstabelecimento());
		
		try {
			@SuppressWarnings("unused")
			LtmResponse<LtmRespostaLoginSimplificado> response = 
						ltmService.loginSimplificado(matrix.getLtm_api_key(), dadosLogin);

			
		} catch (ServiceUnavailableException e){
			nMsg.dispH1("   O F F L I N E   {ServiceUnavailableException} ");
			matrix.setApi_online_cielo(false);
			return matrix;
			
		} catch (Exception e){
			nMsg.disp(  "   O F F L I N E   {} ");
			nMsg.dispH1("   Reason : "   + Util.extractFromException(e.getMessage()) + "\n");
			return matrix;
		}
		matrix.setApi_online_cielo(true);
		return matrix;
	}
	

	public MonitorApiDTO verifyLogin(MonitorApiDTO matrix) {
		
		try {
//			LtmResponse<LtmRespostaLoginSimplificado> response = ltmService.loginSimplificado(apiKey, dadosLogin);
//			nMsg.disp(response.getResponseMessageCode());
//			nMsg.disp(response.getObject().getToken());
//			nMsg.disp(response.getObject().getUsuario());
//			nMsg.disp(response.getObject().getEstabelecimento());
			
		} catch (ServiceUnavailableException e){
			nMsg.dispH1("   O F F L I N E   ");	
		} catch (Exception e){
			nMsg.dispH1("   O F F L I N E   ");	
		}		
		
		return matrix;
	}
	
	
}
