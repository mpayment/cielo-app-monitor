package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.*;
import javax.xml.bind.annotation.*;

public class WPProduct {
	
	@XmlAttribute(name="Titulo")
	private String titulo;

	@XmlAttribute(name="ImagemVitrine")
	private Boolean imagemVitrine;

	@XmlAttribute(name="IdProduto")
	private Long idProduto;
	
	@XmlAttribute(name="IdOriginal")
	private Long idOriginal;
	
	@XmlElement(name="Cliente")
	private WPCustomer cliente;
	
	@XmlElement(name="Campanha")
	private WPCampaign campanha;
	
	@XmlAttribute(name="UrlsImagem")
	private String urlsImagem;
	
	@XmlAttribute(name="UrlsImagemGrande")
	private String urlsImagemGrande;
	
	@XmlElement(name="ProdutosSku")
	@XmlList
	private List<WPProductSku> produtosSku;

	@XmlElement(name="ImagensGaleria")
	@XmlList
	private List<WPProductGaleryImage> imagensGaleria;
	
	@XmlAttribute(name="PrecoDeReais")
	private String precoDeReais;
	
	@XmlAttribute(name="PrecoDePontos")
	private String precoDePontos;
	
	@XmlAttribute(name="PrecoDe")
	private String precoDe;
	
	@XmlAttribute(name="PrecoPorReais")
	private String precoPorReais;
	
	@XmlAttribute(name="PrecoPorPontos")
	private String precoPorPontos;
	
	@XmlAttribute(name="PrecoPor")
	private String precoPor;
	
	@XmlAttribute(name="ValorComplementar")
	private String valorComplementar;
	
	@XmlAttribute(name="Descricao")
	private String descricao;
	
	@XmlAttribute(name="IdCategoria")
	private Integer idCategoria;
	
	@XmlAttribute(name="NomeCategoria")
	private String nomeCategoria;
	
	@XmlAttribute(name="Ativo")
	private Boolean ativo;
	
	@XmlAttribute(name="Ordem")
	private Integer ordem;
	
	@XmlAttribute(name = "ParceiroNome")
	private String parceiroNome;
	
	@XmlAttribute(name = "ParceiroCampanha")
	private Long parceiroCampanha;
	
	@XmlAttribute(name = "IdParceiro")
	private Long idParceiro;
	
	public String getParceiroNome() {
		return parceiroNome;
	}

	public void setParceiroNome(String parceiroNome) {
		this.parceiroNome = parceiroNome;
	}

	public Long getParceiroCampanha() {
		return parceiroCampanha;
	}

	public void setParceiroCampanha(Long parceiroCampanha) {
		this.parceiroCampanha = parceiroCampanha;
	}

	public Long getIdParceiro() {
		return idParceiro;
	}

	public void setIdParceiro(Long idParceiro) {
		this.idParceiro = idParceiro;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public WPCampaign getCampanha() {
		return campanha;
	}

	public void setCampanha(WPCampaign campanha) {
		this.campanha = campanha;
	}

	public WPCustomer getCliente() {
		return cliente;
	}

	public void setCliente(WPCustomer cliente) {
		this.cliente = cliente;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}

	public Long getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(Long idProduto) {
		this.idProduto = idProduto;
	}

	public Boolean getImagemVitrine() {
		return imagemVitrine;
	}

	public void setImagemVitrine(Boolean imagemVitrine) {
		this.imagemVitrine = imagemVitrine;
	}

	public String getNomeCategoria() {
		return nomeCategoria;
	}

	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public String getPrecoDe() {
		return precoDe;
	}

	public void setPrecoDe(String precoDe) {
		this.precoDe = precoDe;
	}

	public String getPrecoDePontos() {
		return precoDePontos;
	}

	public void setPrecoDePontos(String precoDePontos) {
		this.precoDePontos = precoDePontos;
	}

	public String getPrecoDeReais() {
		return precoDeReais;
	}

	public void setPrecoDeReais(String precoDeReais) {
		this.precoDeReais = precoDeReais;
	}

	public String getPrecoPor() {
		return precoPor;
	}

	public void setPrecoPor(String precoPor) {
		this.precoPor = precoPor;
	}

	public String getPrecoPorPontos() {
		return precoPorPontos;
	}

	public void setPrecoPorPontos(String precoPorPontos) {
		this.precoPorPontos = precoPorPontos;
	}

	public String getPrecoPorReais() {
		return precoPorReais;
	}

	public void setPrecoPorReais(String precoPorReais) {
		this.precoPorReais = precoPorReais;
	}

	public List<WPProductSku> getProdutosSku() {
		if (produtosSku == null) {
			produtosSku = new ArrayList<WPProductSku>();
		}
		return produtosSku;
	}

	public void setProdutosSku(List<WPProductSku> produtosSku) {
		this.produtosSku = produtosSku;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Long getIdOriginal() {
		return idOriginal;
	}

	public void setIdOriginal(Long idOriginal) {
		this.idOriginal = idOriginal;
	}

	public String getUrlsImagem() {
		return urlsImagem;
	}

	public void setUrlsImagem(String urlsImagem) {
		this.urlsImagem = urlsImagem;
	}

	public String getUrlsImagemGrande() {
		return urlsImagemGrande;
	}

	public void setUrlsImagemGrande(String urlsImagemGrande) {
		this.urlsImagemGrande = urlsImagemGrande;
	}

	public List<WPProductGaleryImage> getImagensGaleria() {
		if (imagensGaleria == null) {
			imagensGaleria = new ArrayList<WPProductGaleryImage>();
		}
		return imagensGaleria;
	}

	public void setImagensGaleria(List<WPProductGaleryImage> imagensGaleria) {
		this.imagensGaleria = imagensGaleria;
	}

	public String getValorComplementar() {
		return valorComplementar;
	}

	public void setValorComplementar(String valorComplementar) {
		this.valorComplementar = valorComplementar;
	}

	@Override
	public String toString() {
		return "WPProduct [titulo=" + titulo + ", imagemVitrine="
				+ imagemVitrine + ", idProduto=" + idProduto + ", idOriginal="
				+ idOriginal + ", cliente=" + cliente + ", campanha="
				+ campanha + ", urlsImagem=" + urlsImagem
				+ ", urlsImagemGrande=" + urlsImagemGrande + ", produtosSku="
				+ produtosSku + ", imagensGaleria=" + imagensGaleria
				+ ", precoDeReais=" + precoDeReais + ", precoDePontos="
				+ precoDePontos + ", precoDe=" + precoDe + ", precoPorReais="
				+ precoPorReais + ", precoPorPontos=" + precoPorPontos
				+ ", precoPor=" + precoPor + ", valorComplementar="
				+ valorComplementar + ", descricao=" + descricao
				+ ", idCategoria=" + idCategoria + ", nomeCategoria="
				+ nomeCategoria + ", ativo=" + ativo + ", ordem=" + ordem
				+ ", parceiroNome=" + parceiroNome + ", parceiroCampanha="
				+ parceiroCampanha + ", idParceiro=" + idParceiro + "]";
	}
}