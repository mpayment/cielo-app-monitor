package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlElement;

public class WPResponse<T> extends WPAbstractResponse {

    @XmlElement(name = "Resultado")
    private T resultado;

    public T getResultado() {
        return resultado;
    }

    public void setResultado(T resultado) {
        this.resultado = resultado;
    }

    @Override
    public String toString() {
        return "WPResponse{" + "resultado=" + resultado + '}' + super.toString();
    }
}
