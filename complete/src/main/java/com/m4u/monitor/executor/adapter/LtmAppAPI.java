package com.m4u.monitor.executor.adapter;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.m4u.monitor.executor.adapter.model.ltm.LtmDadosLoginSimplificado;
import com.m4u.monitor.executor.adapter.model.ltm.LtmEligibilidadeFidelidade;
import com.m4u.monitor.executor.adapter.model.ltm.LtmPedidoCadastroFidelidade;
import com.m4u.monitor.executor.adapter.model.ltm.LtmPedidoComplementoCadastroFidelidade;
import com.m4u.monitor.executor.adapter.model.ltm.LtmResponse;
import com.m4u.monitor.executor.adapter.model.ltm.LtmRespostaCadastro;
import com.m4u.monitor.executor.adapter.model.ltm.LtmRespostaLoginSimplificado;
import com.m4u.monitor.executor.adapter.model.ltm.LtmApiUtil;



@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface LtmAppAPI {
	
	@GET
    @Path("Participante/VerificarElegibilidadeApp")
    LtmResponse<LtmEligibilidadeFidelidade> verificarEligibilidadeFidelidade(
    		@HeaderParam(LtmApiUtil.LTM_API_KEY_HEADER_NAME) String cieloApiKey,
    		@QueryParam("numeroEc") String numeroEc,
    		@QueryParam("cnpj") String cnpj,
    		@QueryParam("email") String email,
    		@QueryParam("dddCelular") String dddCelular,
    		@QueryParam("celular") String celular);
    
    @POST
    @Path("Participante/CadastroFidelidadeApp")
    LtmResponse<LtmRespostaCadastro> cadastroFidelidade(
    		@HeaderParam(LtmApiUtil.LTM_API_KEY_HEADER_NAME) String cieloApiKey,
    		LtmPedidoCadastroFidelidade pedidoCadastro);
    
    @POST
    @Path("Participante/SalvarComplementoCadastroApp")
    LtmResponse<LtmRespostaCadastro> salvarComplementoCadastro(
    		@HeaderParam(LtmApiUtil.LTM_API_KEY_HEADER_NAME) String cieloApiKey,
    		LtmPedidoComplementoCadastroFidelidade pedidoComplemento);
    
    @POST
    @Path("Participante/LoginSimplificadoApp")
    LtmResponse<LtmRespostaLoginSimplificado> loginSimplificado(
    		@HeaderParam(LtmApiUtil.LTM_API_KEY_HEADER_NAME) String cieloApiKey,
    		LtmDadosLoginSimplificado dadosLogin);
}