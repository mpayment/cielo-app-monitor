package com.m4u.monitor.executor.adapter.model.cielo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class ListaOfertasResponseDTO {

	private List<OfertaDTO> ofertas;

	public List<OfertaDTO> getOfertas() {
		return ofertas;
	}

	public void setOfertas(List<OfertaDTO> ofertas) {
		this.ofertas = ofertas;
	}
}