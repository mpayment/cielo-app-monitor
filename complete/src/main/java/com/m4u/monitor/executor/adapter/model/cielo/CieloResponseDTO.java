package com.m4u.monitor.executor.adapter.model.cielo;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.ArrayUtils;



@XmlAccessorType(XmlAccessType.FIELD)
public class CieloResponseDTO<T> implements RemoteResponse {
	
	public static final String OK_MESSAGE = "200";

	private T object;
	private String version;
	private CieloMessageDTO[] message;

	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public CieloMessageDTO[] getMessage() {
		return message;
	}

	public void setMessage(CieloMessageDTO[] message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "CieloResponseDTO [object=" + object + ", version=" + version
				+ ", message=" + Arrays.toString(message) + "]";
	}
	
	@Override
	public boolean isResponseOk() {
		return OK_MESSAGE.equalsIgnoreCase(getResponseMessage());
	}

	@Override
	public String getResponseMessage() {
		return getResponseMessageCode();
	}
	
	public CieloMessageDTO getCieloMessageDTO() {
		return ArrayUtils.isEmpty(message) ? null : message[0];
	}
	
	public String getResponseMessageCode() {
		CieloMessageDTO firstMessage = getCieloMessageDTO();
		return firstMessage == null ? null : getString(firstMessage.getErrorCode());
	}
	
	private String getString(Integer value) {
		return value == null ? null : value.toString().trim();
	}
	
	public String getResponseMessageText() {
		CieloMessageDTO firstMessage = getCieloMessageDTO();
		return firstMessage == null ? null : firstMessage.getMessage();
	}
}