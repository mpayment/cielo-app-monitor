package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlElement;

public class LtmNavigationLogSavingRequest {

    @XmlElement(name = "idAplicacao")
    private int idAplicacao;
    
    @XmlElement(name = "idCampanha")
    private int idCampanha;
    
    @XmlElement(name = "idEmpresa")
    private int idEmpresa;
    
    @XmlElement(name = "idModulo")
    private int idModulo;
    
    @XmlElement(name = "idParticipante")
    private long idParticipante;
    
    @XmlElement(name = "idProjeto")
    private int idProjeto;
    
    @XmlElement(name = "PaginaAnterior")
    private String paginaAnterior;
    
    @XmlElement(name = "PaginaDestino")
    private String paginaDestino;

    public int getIdAplicacao() {
        return idAplicacao;
    }

    public void setIdAplicacao(int idAplicacao) {
        this.idAplicacao = idAplicacao;
    }

    public int getIdCampanha() {
        return idCampanha;
    }

    public void setIdCampanha(int idCampanha) {
        this.idCampanha = idCampanha;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public int getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(int idModulo) {
        this.idModulo = idModulo;
    }

    public long getIdParticipante() {
        return idParticipante;
    }

    public void setIdParticipante(long idParticipante) {
        this.idParticipante = idParticipante;
    }

    public int getIdProjeto() {
        return idProjeto;
    }

    public void setIdProjeto(int idProjeto) {
        this.idProjeto = idProjeto;
    }

    public String getPaginaAnterior() {
        return paginaAnterior;
    }

    public void setPaginaAnterior(String paginaAnterior) {
        this.paginaAnterior = paginaAnterior;
    }

    public String getPaginaDestino() {
        return paginaDestino;
    }

    public void setPaginaDestino(String paginaDestino) {
        this.paginaDestino = paginaDestino;
    }

    @Override
    public String toString() {
        return "LtmNavigationLogSavingRequest [idAplicacao=" + idAplicacao + ", idCampanha=" + idCampanha + ", idEmpresa=" + idEmpresa
                + ", idModulo=" + idModulo + ", idParticipante=" + idParticipante + ", idProjeto=" + idProjeto + ", paginaAnterior="
                + paginaAnterior + ", paginaDestino=" + paginaDestino + "]";
    }
}