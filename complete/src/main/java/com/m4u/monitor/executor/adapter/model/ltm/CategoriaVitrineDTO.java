package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class CategoriaVitrineDTO {

	@XmlElement(name = "IdCategoria")
	private long idCategoria;
	
	@XmlElement(name = "Categoria")
	private String categoria;
	
	@XmlElement(name = "Secao")
	private String secao;

	public long getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(long idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getSecao() {
		return secao;
	}

	public void setSecao(String secao) {
		this.secao = secao;
	}

	@Override
	public String toString() {
		return "CategoriaVitrineDTO [idCategoria=" + idCategoria
				+ ", categoria=" + categoria + ", secao=" + secao + "]";
	}
}