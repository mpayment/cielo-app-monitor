package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPPartnerTokenValidationResponse {

	@XmlAttribute(name="IdCampanha")
	private Integer idCampanha;
	
	@XmlAttribute(name="LoginUsuario")
	private String loginUsuario;
	
	@XmlAttribute(name="TokenUsuario")
	private String tokenUsuario;
	
	@XmlAttribute(name="IdCampanhaGestaoPontos")
	private Integer idCampanhaGestaoPontos;
	
	@XmlAttribute(name="SimboloMoeda")
	private String simboloMoeda;
	
	@XmlAttribute(name="FormatoMoeda")
	private String formatoMoeda;
	
	@XmlAttribute(name="NomeMoeda")
	private String nomeMoeda;
	
	@XmlAttribute(name="NomeUsuario")
	private String nomeUsuario;

	@XmlAttribute(name="IdUsuario")
	private Long idUsuario;
	
	@XmlAttribute(name="Cep")
	private String cep;
	
	@XmlAttribute(name="NomeMoedaComplementar")
	private String nomeMoedaComplementar;
	
	@XmlAttribute(name="SimboloMoedaComplementar")
	private String simboloMoedaComplementar;
	
	@XmlAttribute(name="FatorConversaoComplementar")
	private String fatorConversaoComplementar;
	
	@XmlAttribute(name="FormatoMoedaComplementar")
	private String formatoMoedaComplementar;

	public Integer getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(Integer idCampanha) {
		this.idCampanha = idCampanha;
	}

	public String getLoginUsuario() {
		return loginUsuario;
	}

	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}

	public String getTokenUsuario() {
		return tokenUsuario;
	}

	public void setTokenUsuario(String tokenUsuario) {
		this.tokenUsuario = tokenUsuario;
	}

	public Integer getIdCampanhaGestaoPontos() {
		return idCampanhaGestaoPontos;
	}

	public void setIdCampanhaGestaoPontos(Integer idCampanhaGestaoPontos) {
		this.idCampanhaGestaoPontos = idCampanhaGestaoPontos;
	}

	public String getSimboloMoeda() {
		return simboloMoeda;
	}

	public void setSimboloMoeda(String simboloMoeda) {
		this.simboloMoeda = simboloMoeda;
	}

	public String getFormatoMoeda() {
		return formatoMoeda;
	}

	public void setFormatoMoeda(String formatoMoeda) {
		this.formatoMoeda = formatoMoeda;
	}

	public String getNomeMoeda() {
		return nomeMoeda;
	}

	public void setNomeMoeda(String nomeMoeda) {
		this.nomeMoeda = nomeMoeda;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getNomeMoedaComplementar() {
		return nomeMoedaComplementar;
	}

	public void setNomeMoedaComplementar(String nomeMoedaComplementar) {
		this.nomeMoedaComplementar = nomeMoedaComplementar;
	}

	public String getSimboloMoedaComplementar() {
		return simboloMoedaComplementar;
	}

	public void setSimboloMoedaComplementar(String simboloMoedaComplementar) {
		this.simboloMoedaComplementar = simboloMoedaComplementar;
	}

	public String getFatorConversaoComplementar() {
		return fatorConversaoComplementar;
	}

	public void setFatorConversaoComplementar(String fatorConversaoComplementar) {
		this.fatorConversaoComplementar = fatorConversaoComplementar;
	}

	public String getFormatoMoedaComplementar() {
		return formatoMoedaComplementar;
	}

	public void setFormatoMoedaComplementar(String formatoMoedaComplementar) {
		this.formatoMoedaComplementar = formatoMoedaComplementar;
	}

	@Override	public String toString() {
		return "WPPartnerTokenValidationResponse [idCampanha=" + idCampanha
				+ ", loginUsuario=" + loginUsuario + ", tokenUsuario="
				+ tokenUsuario + ", idCampanhaGestaoPontos="
				+ idCampanhaGestaoPontos + ", simboloMoeda=" + simboloMoeda
				+ ", formatoMoeda=" + formatoMoeda + ", nomeMoeda=" + nomeMoeda
				+ ", nomeUsuario=" + nomeUsuario + ", idUsuario=" + idUsuario
				+ ", cep=" + cep + ", nomeMoedaComplementar="
				+ nomeMoedaComplementar + ", simboloMoedaComplementar="
				+ simboloMoedaComplementar + ", fatorConversaoComplementar="
				+ fatorConversaoComplementar + ", formatoMoedaComplementar="
				+ formatoMoedaComplementar + "]";
	}
}