package com.m4u.monitor.executor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.m4u.monitor.executor.adapter.CieloApiWebProxyHolder;
import com.m4u.monitor.executor.adapter.model.cielo.CieloResponseDTO;
import com.m4u.monitor.executor.adapter.model.cielo.ResponseLoginUsuarioDTO;


@Component
public class CieloService {

	@Autowired
	private CieloApiWebProxyHolder webProxyHolder;
	

	public CieloResponseDTO<ResponseLoginUsuarioDTO> doLogin(String user, String password, String ec) {

		final CieloResponseDTO<ResponseLoginUsuarioDTO> response = 
				webProxyHolder.getWebProxy().login(user, ec, password);

		return response;
		
	}
	
}

