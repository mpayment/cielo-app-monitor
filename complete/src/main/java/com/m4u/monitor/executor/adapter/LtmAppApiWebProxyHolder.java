package com.m4u.monitor.executor.adapter;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.m4u.monitor.commons.service.CieloCacheManager;
import com.m4u.monitor.commons.service.CacheManagerListener;
import com.m4u.monitor.commons.util.constants.ServerConfigurationKey;
import com.m4u.monitor.commons.util.web.WebClientUtil;


@Component
public class LtmAppApiWebProxyHolder implements CacheManagerListener {
	
	@Autowired
	private CieloCacheManager cacheManager;
	
	@Autowired
	private WebClientUtil webClientUtil;
	
    private LtmAppAPI webProxy;

    @PostConstruct
    public void init() {
        reloadCache();
        cacheManager.addListener(this);
    }
    
    @Override
    public void reloadCache() {
    	webProxy = webClientUtil.createJsonClient(LtmAppAPI.class, ServerConfigurationKey.CIELO_FIDELIDADE_APP_WS_URL);
    }

	public LtmAppAPI getWebProxy() {
		return webProxy;
	}
}