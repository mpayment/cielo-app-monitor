package com.m4u.monitor.executor.adapter.model.ltm;

public interface RemoteResponse {

	boolean isResponseOk();
	String getResponseMessage();
	String getResponseMessageText();
	String getResponseMessageCode();
}