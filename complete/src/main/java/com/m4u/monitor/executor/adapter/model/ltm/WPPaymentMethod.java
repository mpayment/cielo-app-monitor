package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPPaymentMethod {

    @XmlAttribute(name="Campanha")
    private WPCampaign campanha;
    
    @XmlAttribute(name="IdCampanhaPagamento")
    private Integer idCampanhaPagamento;
    
    @XmlAttribute(name="IdPagamento")
    private Integer idPagamento;
    
    @XmlAttribute(name="IdPagamentoOriginal")
    private String idPagamentoOriginal;
    
    @XmlAttribute(name="Pagamento")
    private String pagamento;
    
    @XmlAttribute(name="UrlImagem")
    private String urlImagem;
    
    @XmlAttribute(name="UrlImagemPequena")
    private String urlImagemPequena;
    
    @XmlAttribute(name="Oculto")
    private String oculto;
    
    @XmlAttribute(name="IdPagamentoTipo")
    private Integer idPagamentoTipo;
    
    @XmlAttribute(name="PagamentoTipo")
    private String pagamentoTipo;
    
    @XmlAttribute(name="NomeBancoCartao")
    private String nomeBancoCartao;
    

    public WPCampaign getCampanha() {
        return campanha;
    }

    public void setCampanha(WPCampaign campanha) {
        this.campanha = campanha;
    }

    public Integer getIdCampanhaPagamento() {
        return idCampanhaPagamento;
    }

    public void setIdCampanhaPagamento(Integer idCampanhaPagamento) {
        this.idCampanhaPagamento = idCampanhaPagamento;
    }

    public Integer getIdPagamento() {
        return idPagamento;
    }

    public void setIdPagamento(Integer idPagamento) {
        this.idPagamento = idPagamento;
    }

    public String getIdPagamentoOriginal() {
        return idPagamentoOriginal;
    }

    public void setIdPagamentoOriginal(String idPagamentoOriginal) {
        this.idPagamentoOriginal = idPagamentoOriginal;
    }

    public String getPagamento() {
        return pagamento;
    }

    public void setPagamento(String pagamento) {
        this.pagamento = pagamento;
    }

    public String getUrlImagem() {
        return urlImagem;
    }

    public void setUrlImagem(String urlImagem) {
        this.urlImagem = urlImagem;
    }

    public String getUrlImagemPequena() {
        return urlImagemPequena;
    }

    public void setUrlImagemPequena(String urlImagemPequena) {
        this.urlImagemPequena = urlImagemPequena;
    }

    public String getOculto() {
        return oculto;
    }

    public void setOculto(String oculto) {
        this.oculto = oculto;
    }

    public Integer getIdPagamentoTipo() {
        return idPagamentoTipo;
    }

    public void setIdPagamentoTipo(Integer idPagamentoTipo) {
        this.idPagamentoTipo = idPagamentoTipo;
    }

    public String getNomeBancoCartao() {
        return nomeBancoCartao;
    }

    public void setNomeBancoCartao(String nomeBancoCartao) {
        this.nomeBancoCartao = nomeBancoCartao;
    }

    public String getPagamentoTipo() {
        return pagamentoTipo;
    }

    public void setPagamentoTipo(String pagamentoTipo) {
        this.pagamentoTipo = pagamentoTipo;
    }

    @Override
    public String toString() {
        return "WPPaymentMethod [campanha=" + campanha + ", idCampanhaPagamento=" + idCampanhaPagamento + ", idPagamento=" + idPagamento
                + ", idPagamentoOriginal=" + idPagamentoOriginal + ", pagamento=" + pagamento + ", urlImagem=" + urlImagem
                + ", urlImagemPequena=" + urlImagemPequena + ", oculto=" + oculto + ", idPagamentoTipo=" + idPagamentoTipo
                + ", pagamentoTipo=" + pagamentoTipo + ", nomeBancoCartao=" + nomeBancoCartao + "]";
    }
}