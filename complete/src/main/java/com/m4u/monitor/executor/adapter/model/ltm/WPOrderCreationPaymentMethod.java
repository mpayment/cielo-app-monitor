package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPOrderCreationPaymentMethod {
	
	@XmlAttribute(name="IdPagamentoTipo")
	private int idPagamentoTipo;
	
	@XmlAttribute(name="NomeTipoPagamento")
	private String nomeTipoPagamento;

	public int getIdPagamentoTipo() {
		return idPagamentoTipo;
	}

	public void setIdPagamentoTipo(int idPagamentoTipo) {
		this.idPagamentoTipo = idPagamentoTipo;
	}

	public String getNomeTipoPagamento() {
		return nomeTipoPagamento;
	}

	public void setNomeTipoPagamento(String nomeTipoPagamento) {
		this.nomeTipoPagamento = nomeTipoPagamento;
	}

	@Override
	public String toString() {
		return "WPPaymentType [idPagamentoTipo=" + idPagamentoTipo
				+ ", nomeTipoPagamento=" + nomeTipoPagamento + "]";
	}
}