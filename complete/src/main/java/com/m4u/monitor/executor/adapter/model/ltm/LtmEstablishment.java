package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.NONE)
public class LtmEstablishment {
	
	@XmlAttribute(name="Bairro")
	private String bairro;
	
	@XmlAttribute(name="Cep")
	private String cep;
	
	@XmlAttribute(name="Cidade")
	private String cidade;
	
	@XmlAttribute(name="Cnpj")
	private String cnpj;
	
	@XmlAttribute(name="Complemento")
	private String complemento;
	
	@XmlAttribute(name="DddTelefoneComercial")
	private Integer dddTelefoneComercial;
	
	@XmlAttribute(name="EnderecoComercial")
	private String enderecoComercial;
	
	@XmlAttribute(name="Estado")
	private String estado;
	
	@XmlAttribute(name="IdEc")
	private Long idEc;
	
	@XmlAttribute(name="NomeEc")
	private String nomeEc;
	
	@XmlAttribute(name="Numero")
	private String numero;

	@XmlAttribute(name="NumeroEc")
	private String numeroEc;
	
	@XmlAttribute(name="TelefoneComercial")
	private String telefoneComercial;

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Integer getDddTelefoneComercial() {
		return dddTelefoneComercial;
	}

	public void setDddTelefoneComercial(Integer dddTelefoneComercial) {
		this.dddTelefoneComercial = dddTelefoneComercial;
	}

	public String getEnderecoComercial() {
		return enderecoComercial;
	}

	public void setEnderecoComercial(String enderecoComercial) {
		this.enderecoComercial = enderecoComercial;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Long getIdEc() {
		return idEc;
	}

	public void setIdEc(Long idEc) {
		this.idEc = idEc;
	}

	public String getNomeEc() {
		return nomeEc;
	}

	public void setNomeEc(String nomeEc) {
		this.nomeEc = nomeEc;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNumeroEc() {
		return numeroEc;
	}

	public void setNumeroEc(String numeroEc) {
		this.numeroEc = numeroEc;
	}

	public String getTelefoneComercial() {
		return telefoneComercial;
	}

	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

	@Override
	public String toString() {
		return "LtmEC [bairro=" + bairro + ", cep=" + cep + ", cidade="
				+ cidade + ", cnpj=" + cnpj + ", complemento=" + complemento
				+ ", dddTelefoneComercial=" + dddTelefoneComercial
				+ ", enderecoComercial=" + enderecoComercial + ", estado="
				+ estado + ", idEc=" + idEc + ", nomeEc=" + nomeEc
				+ ", numero=" + numero + ", numeroEc=" + numeroEc
				+ ", telefoneComercial=" + telefoneComercial + "]";
	}	
}