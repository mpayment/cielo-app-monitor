package com.m4u.monitor.executor.adapter.model.cielo;

public class ContratarOfertaResponseDTO {

	private boolean success;
	private String message;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = Boolean.TRUE.equals(success);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}