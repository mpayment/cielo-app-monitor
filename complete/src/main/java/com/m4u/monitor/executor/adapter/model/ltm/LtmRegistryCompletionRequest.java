package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class LtmRegistryCompletionRequest {

	@XmlAttribute(name="IdParticipante")
	private String idParticipante;
	
	@XmlAttribute(name="IdEc")
	private long idEc;
	
	@XmlAttribute(name="IdSexo")
	private int idSexo;
	
	@XmlAttribute(name="Cpf")
	private String cpf;
	
	@XmlAttribute(name="DataNascimento")
	private String dataNascimento;
	
	@XmlAttribute(name="Numero")
	private String numero;
	
	@XmlAttribute(name="Complemento")
	private String complemento;
	
	@XmlAttribute(name="Bairro")
	private String bairro;
	
	@XmlAttribute(name="DddTelefoneComercial")
	private String dddTelefoneComercial;
	
	@XmlAttribute(name="TelefoneComercial")
	private String telefoneComercial;
	
	public String getIdParticipante() {
		return idParticipante;
	}

	public void setIdParticipante(String idParticipante) {
		this.idParticipante = idParticipante;
	}

	public long getIdEc() {
		return idEc;
	}

	public void setIdEc(long idEc) {
		this.idEc = idEc;
	}

	public int getIdSexo() {
		return idSexo;
	}

	public void setIdSexo(int idSexo) {
		this.idSexo = idSexo;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getDddTelefoneComercial() {
		return dddTelefoneComercial;
	}

	public void setDddTelefoneComercial(String dddTelefoneComercial) {
		this.dddTelefoneComercial = dddTelefoneComercial;
	}

	public String getTelefoneComercial() {
		return telefoneComercial;
	}

	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

	@Override
	public String toString() {
		return "LtmRegistryCompletionRequest [idParticipante=" + idParticipante
				+ ", idEc=" + idEc + ", idSexo=" + idSexo + ", cpf=" + cpf
				+ ", dataNascimento=" + dataNascimento + ", numero=" + numero
				+ ", complemento=" + complemento + ", bairro=" + bairro
				+ ", dddTelefoneComercial=" + dddTelefoneComercial
				+ ", telefoneComercial=" + telefoneComercial + "]";
	}
	
}