package com.m4u.monitor.executor.adapter.model.cielo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class EstabelecimentoClienteDTO {

	private Long nuEstabelecimento;
	private Long nuMatriz;
	private String nuCnpj;
	private String nomeFantasia;
	private String cdStatus;
	private String cdCategoriaAntecipacao;

	public Long getNuEstabelecimento() {
		return nuEstabelecimento;
	}

	public void setNuEstabelecimento(Long nuEstabelecimento) {
		this.nuEstabelecimento = nuEstabelecimento;
	}

	public Long getNuMatriz() {
		return nuMatriz;
	}

	public void setNuMatriz(Long nuMatriz) {
		this.nuMatriz = nuMatriz;
	}

	public String getNuCnpj() {
		return nuCnpj;
	}

	public void setNuCnpj(String nuCnpj) {
		this.nuCnpj = nuCnpj;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCdStatus() {
		return cdStatus;
	}

	public void setCdStatus(String cdStatus) {
		this.cdStatus = cdStatus;
	}

	public String getCdCategoriaAntecipacao() {
		return cdCategoriaAntecipacao;
	}

	public void setCdCategoriaAntecipacao(String cdCategoriaAntecipacao) {
		this.cdCategoriaAntecipacao = cdCategoriaAntecipacao;
	}

	@Override
	public String toString() {
		return "EstabelecimentoClienteDTO [nuEstabelecimento="
				+ nuEstabelecimento + ", nuMatriz=" + nuMatriz + ", nuCnpj="
				+ nuCnpj + ", nomeFantasia=" + nomeFantasia + ", cdStatus="
				+ cdStatus + ", cdCategoriaAntecipacao="
				+ cdCategoriaAntecipacao + "]";
	}
}