package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPBalance {
	
	@XmlAttribute(name="Login")
	private String login;
	
	@XmlAttribute(name="Saldo")
	private String saldo;
	
	@XmlAttribute(name="SaldoPontos")
	private String saldoPontos;
	
	@XmlAttribute(name="SaldoReais")
	private String saldoReais;
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getSaldo() {
		return saldo;
	}
	
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	
	public String getSaldoPontos() {
		return saldoPontos;
	}
	
	public void setSaldoPontos(String saldoPontos) {
		this.saldoPontos = saldoPontos;
	}
	
	public String getSaldoReais() {
		return saldoReais;
	}
	
	public void setSaldoReais(String saldoReais) {
		this.saldoReais = saldoReais;
	}

	@Override
	public String toString() {
		return "WebPremiosSaldo [login=" + login + ", saldo=" + saldo
				+ ", saldoPontos=" + saldoPontos + ", saldoReais=" + saldoReais + "]";
	}
}