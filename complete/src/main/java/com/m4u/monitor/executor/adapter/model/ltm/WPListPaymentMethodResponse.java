package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlList;

public class WPListPaymentMethodResponse {

    @XmlAttribute(name="FormasPagamento")
    @XmlList
    private List<WPPaymentMethod> formasPagamento;

    public List<WPPaymentMethod> getFormasPagamento() {
        if (formasPagamento == null) {
            formasPagamento = new ArrayList<WPPaymentMethod>();
        }
        return formasPagamento;
    }

    public void setFormasPagamento(List<WPPaymentMethod> paymentMethodList) {
        this.formasPagamento = paymentMethodList;
    }

    @Override
    public String toString() {
        return "WPListPaymentMethodResponse [formasPagamento=" + formasPagamento + "]";
    }
}