package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPCampaign {

	@XmlAttribute(name="IdCampanha")
	private Integer idCampanha;
	
	public WPCampaign() { }
	
	public WPCampaign(Integer idCampanha) {
		this.idCampanha = idCampanha;
	}

	public Integer getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(Integer idCampanha) {
		this.idCampanha = idCampanha;
	}

	@Override
	public String toString() {
		return "WPCampaign [idCampanha=" + idCampanha + "]";
	}	
}