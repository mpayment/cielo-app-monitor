package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResgateBurningResponseDTO {

	@XmlElement(name = "Status")
	private boolean status;
	
	@XmlElement(name = "FlContemplado")
	private boolean flContemplado;

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public boolean isFlContemplado() {
		return flContemplado;
	}

	public void setFlContemplado(boolean flContemplado) {
		this.flContemplado = flContemplado;
	}

	@Override
	public String toString() {
		return "ResgateBurningResponseDTO [status=" + status
				+ ", flContemplado=" + flContemplado + "]";
	}
}