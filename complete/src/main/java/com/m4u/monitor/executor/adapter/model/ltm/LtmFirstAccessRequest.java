package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class LtmFirstAccessRequest {
	@XmlAttribute(name="Email")
	private String email;
	
	@XmlAttribute(name="DddTelefoneCelular")
	private Integer dddTelefoneCelular;
	
	@XmlAttribute(name="TelefoneCelular")
	private Integer telefoneCelular;
	
	@XmlAttribute(name="Newsletter")
	private boolean newsLetter;
	
	@XmlAttribute(name="Nome")
	private String nome;
	
	@XmlAttribute(name="CnpjPrimeiroAcesso")
	private String cnpjPrimeiroAcesso;
	
	@XmlAttribute(name="ECPrimeiroAcesso")
	private String ecPrimeiroAcesso;
	
	@XmlAttribute(name="CodigoParceiro")
	private Integer codigoParceiro;
	
	@XmlAttribute(name="LinkApp")
	private String linkApp;
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Integer getDddTelefoneCelular() {
		return dddTelefoneCelular;
	}
	
	public void setDddTelefoneCelular(Integer dddTelefoneCelular) {
		this.dddTelefoneCelular = dddTelefoneCelular;
	}
	
	public Integer getTelefoneCelular() {
		return telefoneCelular;
	}
	
	public void setTelefoneCelular(Integer telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}
	
	public boolean isNewsLetter() {
		return newsLetter;
	}
	
	public void setNewsLetter(boolean newsLetter) {
		this.newsLetter = newsLetter;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCnpjPrimeiroAcesso() {
		return cnpjPrimeiroAcesso;
	}
	
	public void setCnpjPrimeiroAcesso(String cnpjPrimeiroAcesso) {
		this.cnpjPrimeiroAcesso = cnpjPrimeiroAcesso;
	}
	
	public String getEcPrimeiroAcesso() {
		return ecPrimeiroAcesso;
	}
	
	public void setEcPrimeiroAcesso(String ecPrimeiroAcesso) {
		this.ecPrimeiroAcesso = ecPrimeiroAcesso;
	}
	
	public Integer getCodigoParceiro() {
		return codigoParceiro;
	}
	
	public void setCodigoParceiro(Integer codigoParceiro) {
		this.codigoParceiro = codigoParceiro;
	}
	
	public String getLinkApp() {
		return linkApp;
	}
	
	public void setLinkApp(String linkApp) {
		this.linkApp = linkApp;
	}
	
	public String toString() {
		return "LtmFirstAccessRequest [email=" + email
				+ ", dddTelefoneCelular=" + dddTelefoneCelular
				+ ", telefoneCelular=" + telefoneCelular + ", newsLetter="
				+ newsLetter + ", nome=" + nome + ", cnpjPrimeiroAcesso="
				+ cnpjPrimeiroAcesso + ", ecPrimeiroAcesso=" + ecPrimeiroAcesso
				+ ", codigoParceiro=" + codigoParceiro + ", linkApp=" + linkApp
				+ "]";
	}
}