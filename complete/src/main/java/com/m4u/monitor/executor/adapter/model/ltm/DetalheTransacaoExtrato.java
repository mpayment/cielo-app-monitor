package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlElement;

public class DetalheTransacaoExtrato {
	
	@XmlElement(name = "Fare")
	private Double fare;

	@XmlElement(name = "OrderValue")
	private Double orderValue;

	public Double getFare() {
		return fare;
	}

	public void setFare(Double fare) {
		this.fare = fare;
	}

	public Double getOrderValue() {
		return orderValue;
	}

	public void setOrderValue(Double orderValue) {
		this.orderValue = orderValue;
	}
}