package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.*;

public class WPShowcaseProductDetailRequest extends WPRequest {

    @XmlElement(name="EServToken")
    private WPEServToken eServToken;
    
    @XmlAttribute(name="IdProduto")
    private long idProduto;

    public WPEServToken getEServToken() {
        return eServToken;
    }

    public void setEServToken(WPEServToken eServToken) {
        this.eServToken = eServToken;
    }

    public long getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(long idProduto) {
        this.idProduto = idProduto;
    }

    @Override
    public String toString() {
        return "WPShowcaseProductDetailRequest [eServToken=" + eServToken + ", idProduto=" + idProduto + "] " + super.toString();
    }
}