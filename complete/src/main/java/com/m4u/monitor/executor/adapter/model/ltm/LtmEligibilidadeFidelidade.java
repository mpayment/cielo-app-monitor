package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class LtmEligibilidadeFidelidade {

    public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

    public static final String STATUS_FIDELIDADE_ATIVO = "Ativo";
    public static final String STATUS_FIDELIDADE_ELEGIVEL = "Disponível";
    public static final String STATUS_FIDELIDADE_NAO_ELEGIVEL = "Inativo";
    public static final String STATUS_FIDELIDADE_BLOQUEADO = "Bloqueado";
    public static final String STATUS_FIDELIDADE_NAO_ENCONTRADO = "Não localizado";

    private String statusFidelidade;
    private Boolean statusEmail;
    private String sugestaoEmail;
    private Boolean statusTelefone;
    private String dataAceiteRegulamento;
    private String cpf;
    private String dataNascimento;

    public String getStatusFidelidade() {
        return statusFidelidade;
    }

    public void setStatusFidelidade(String statusFidelidade) {
        this.statusFidelidade = statusFidelidade;
    }

    public Boolean getStatusEmail() {
        return statusEmail;
    }

    public void setStatusEmail(Boolean statusEmail) {
        this.statusEmail = statusEmail;
    }

    public String getSugestaoEmail() {
        return sugestaoEmail;
    }

    public void setSugestaoEmail(String sugestaoEmail) {
        this.sugestaoEmail = sugestaoEmail;
    }

    public Boolean getStatusTelefone() {
        return statusTelefone;
    }

    public void setStatusTelefone(Boolean statusTelefone) {
        this.statusTelefone = statusTelefone;
    }

    public String getDataAceiteRegulamento() {
        return dataAceiteRegulamento;
    }

    public void setDataAceiteRegulamento(String dataAceiteRegulamento) {
        this.dataAceiteRegulamento = dataAceiteRegulamento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @Override
    public String toString() {
        return "LtmEligibilidadeFidelidade [statusFidelidade=" + statusFidelidade + ", statusEmail=" + statusEmail + ", sugestaoEmail="
                + sugestaoEmail + ", statusTelefone=" + statusTelefone + ", dataAceiteRegulamento=" + dataAceiteRegulamento + ", cpf="
                + cpf + ", dataNascimento=" + dataNascimento + "]";
    }
}