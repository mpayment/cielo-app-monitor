package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.datatype.XMLGregorianCalendar;

public class LtmUserPointsExpirationResponse {

	@XmlAttribute(name="dataProximaExpiracao")
	private XMLGregorianCalendar dataProximaExpiracao;

	public XMLGregorianCalendar getDataProximaExpiracao() {
		return dataProximaExpiracao;
	}

	public void setDataProximaExpiracao(XMLGregorianCalendar dataProximaExpiracao) {
		this.dataProximaExpiracao = dataProximaExpiracao;
	}

	@Override
	public String toString() {
		return "LtmPointsExpirationDateResponse [dataProximaExpiracao=" + dataProximaExpiracao + "]";
	}
}