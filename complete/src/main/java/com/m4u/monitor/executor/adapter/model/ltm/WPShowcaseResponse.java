package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;

public class WPShowcaseResponse {
	
	@XmlElement(name="Vitrines")
	@XmlList
	private List<WPShowcase> vitrines;

	public List<WPShowcase> getVitrines() {
		return vitrines;
	}

	public void setVitrines(List<WPShowcase> vitrines) {
		this.vitrines = vitrines;
	}

	@Override
	public String toString() {
		return "WPShowcaseResponse [vitrines=" + vitrines + "]";
	}	
}