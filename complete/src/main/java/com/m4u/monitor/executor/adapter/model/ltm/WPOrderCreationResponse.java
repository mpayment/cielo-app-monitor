package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlElement;

public class WPOrderCreationResponse {

    @XmlElement(name = "EServPedido")
    private WPEServOrder pedido;

    public WPEServOrder getPedido() {
        return pedido;
    }

    public void setPedido(WPEServOrder pedido) {
        this.pedido = pedido;
    }

    @Override public String toString() {
        return "WPOrderCreationResponse{" +
                "pedido=" + pedido +
                '}';
    }
}
