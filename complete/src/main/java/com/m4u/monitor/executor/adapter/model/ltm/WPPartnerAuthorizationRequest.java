package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPPartnerAuthorizationRequest {

	@XmlAttribute(name="IdCampanha")
	private Integer idCampanha;

	@XmlAttribute(name="IdCampanhaGestaoPontos")
	private Integer idCampanhaGestaoPontos;
	
	@XmlAttribute(name="IdEmpresaGestaoPontos")
	private Integer idEmpresaGestaoPontos;
	
	@XmlAttribute(name="IdParceiroGestaoPontos")
	private Integer idParceiroGestaoPontos;
	
	@XmlAttribute(name="TokenLogin")
	private String tokenLogin;
	
	public Integer getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(Integer idCampanha) {
		this.idCampanha = idCampanha;
	}

	public Integer getIdCampanhaGestaoPontos() {
		return idCampanhaGestaoPontos;
	}

	public void setIdCampanhaGestaoPontos(Integer idCampanhaGestaoPontos) {
		this.idCampanhaGestaoPontos = idCampanhaGestaoPontos;
	}

	public Integer getIdEmpresaGestaoPontos() {
		return idEmpresaGestaoPontos;
	}

	public void setIdEmpresaGestaoPontos(Integer idEmpresaGestaoPontos) {
		this.idEmpresaGestaoPontos = idEmpresaGestaoPontos;
	}

	public Integer getIdParceiroGestaoPontos() {
		return idParceiroGestaoPontos;
	}

	public void setIdParceiroGestaoPontos(Integer idParceiroGestaoPontos) {
		this.idParceiroGestaoPontos = idParceiroGestaoPontos;
	}

	public String getTokenLogin() {
		return tokenLogin;
	}

	public void setTokenLogin(String tokenLogin) {
		this.tokenLogin = tokenLogin;
	}

	@Override
	public String toString() {
		return "WPPartnerAuthorizationRequest [idCampanha=" + idCampanha
				+ ", idCampanhaGestaoPontos=" + idCampanhaGestaoPontos
				+ ", idEmpresaGestaoPontos=" + idEmpresaGestaoPontos
				+ ", idParceiroGestaoPontos=" + idParceiroGestaoPontos
				+ ", tokenLogin=" + tokenLogin + "]";
	}
}