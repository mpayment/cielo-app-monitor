package com.m4u.monitor.executor.adapter;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.m4u.monitor.executor.adapter.model.cielo.AutoCadastroResponseDTO;
import com.m4u.monitor.executor.adapter.model.cielo.AutoCadastroTipoPessoa;
import com.m4u.monitor.executor.adapter.model.cielo.CieloMessageDTO;
import com.m4u.monitor.executor.adapter.model.cielo.CieloResponseDTO;
import com.m4u.monitor.executor.adapter.model.cielo.ContratarOfertaResponseDTO;
import com.m4u.monitor.executor.adapter.model.cielo.GestorResponseDTO;
import com.m4u.monitor.executor.adapter.model.cielo.ListaClassificacoesOfertasResponseDTO;
import com.m4u.monitor.executor.adapter.model.cielo.ListaOfertasResponseDTO;
import com.m4u.monitor.executor.adapter.model.cielo.ListaTaxasProdutosDTO;
import com.m4u.monitor.executor.adapter.model.cielo.MassivaResponseDTO;
import com.m4u.monitor.executor.adapter.model.cielo.ResponseLoginUsuarioDTO;
import com.m4u.monitor.executor.adapter.model.cielo.RespostaAntecipacaoAutomaticaDTO;
import com.m4u.monitor.executor.adapter.model.cielo.RespostaTaxasAntecipacaoAutomaticaDTO;
import com.m4u.monitor.executor.adapter.model.cielo.ValidarBancoResponseDTO;


@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface CieloAPI {

	String AUTO_CADASTRO_DATE_PATTERN = "dd/MM/yyyy";
	String ESTABLISHMENT_ID_PARAM_NAME = "estabelecimentoComercial";
	String TOKEN_PARAM_NAME = "token";
	String DATA_INICIAL_PARAM_NAME = "dataInicial";
	String DATA_FINAL_PARAM_NAME = "dataFinal";

	
	@POST
	@Path("api/autenticacao/login")
	CieloResponseDTO<ResponseLoginUsuarioDTO> login(@FormParam("usuario") String user,
			@FormParam(ESTABLISHMENT_ID_PARAM_NAME) String ec, @FormParam("senha") String password);

	@GET
	@Path("api/extrato/consultarResumoOperacoes")
	Response consultarResumoOperacoes(@QueryParam(TOKEN_PARAM_NAME) String token,
			@QueryParam(ESTABLISHMENT_ID_PARAM_NAME) String estabelecimentoComercial,
			@QueryParam(DATA_INICIAL_PARAM_NAME) String dataInicial,
			@QueryParam(DATA_FINAL_PARAM_NAME) String dataFinal);

	@GET
	@Path("api/ARV/DadosAntecipacao")
	Response dadosAntecipacao(@QueryParam(TOKEN_PARAM_NAME) String token,
			@QueryParam(ESTABLISHMENT_ID_PARAM_NAME) String estabelecimentoComercial);

	@POST
	@Path("api/ARV/EfetivaAntecipacaoTotal")
	Response efetivaAntecipacaoTotal(@QueryParam(TOKEN_PARAM_NAME) String token,
			@QueryParam(ESTABLISHMENT_ID_PARAM_NAME) String estabelecimentoComercial);
	
	@POST
	@Path("api/antecipacaoAutomatica/antecipacoes")
	CieloResponseDTO<RespostaAntecipacaoAutomaticaDTO> realizarAntecipacaoAutomatica(
			@QueryParam("usuario") String usuario,
			@QueryParam("estabelecimentoComercial") String estabelecimentoComercial,
			@QueryParam("token") String token,
			@QueryParam("frequencia") String frequencia,
			@QueryParam("diaSemana") Byte diaSemana,
			@QueryParam("diaMes1") Byte diaMes1,
			@QueryParam("diaMes2") Byte diaMes2,
			@QueryParam("diaMes3") Byte diaMes3);

	@GET
	@Path("api/antecipacaoAutomatica/taxas")
	CieloResponseDTO<RespostaTaxasAntecipacaoAutomaticaDTO> consultaTaxasAntecipacaoAutomatica(
			@QueryParam(ESTABLISHMENT_ID_PARAM_NAME) String ec,
			@QueryParam(TOKEN_PARAM_NAME) String token);
	
	@GET
	@Path("api/produtosOfertas/ofertas")
	CieloResponseDTO<ListaOfertasResponseDTO> listarOfertas(
			@QueryParam(ESTABLISHMENT_ID_PARAM_NAME) String ec,
			@QueryParam(TOKEN_PARAM_NAME) String token);
	
	@GET
	@Path("api/produtosOfertas/classificacoesOferta")
	CieloResponseDTO<ListaClassificacoesOfertasResponseDTO> listarClassificacoesOfertas(
			@QueryParam(ESTABLISHMENT_ID_PARAM_NAME) String ec,
			@QueryParam(TOKEN_PARAM_NAME) String token);

	@GET
	@Path("/api/produtosOfertas/taxas")
	CieloResponseDTO<ListaTaxasProdutosDTO> consultarTaxasProdutos(
			@QueryParam(ESTABLISHMENT_ID_PARAM_NAME) String ec,
			@QueryParam(TOKEN_PARAM_NAME) String token,
			@QueryParam("solucaoCaptura") long codigoClassOferta);

	@POST  
	@Path("api/produtosOfertas/contratacoes")
	CieloResponseDTO<ContratarOfertaResponseDTO> contratarOferta(
			@QueryParam(ESTABLISHMENT_ID_PARAM_NAME) String ec,
			@QueryParam("codigoClassOferta") int codigoClassOferta,
			@QueryParam(TOKEN_PARAM_NAME) String token,
			@QueryParam("origem") byte origem,
			@QueryParam("codigoRetorno") int codigoRetorno,
			@QueryParam("codigoOferta") Integer codigoOferta,
			@QueryParam("codigoCampanha") Integer codigoCampanha,
			@QueryParam("detalhes") String detalhes,
			@QueryParam("usuario") String usuario);

	@POST
	@Path("api/autenticacao/logout")
	Response logout(@QueryParam(TOKEN_PARAM_NAME) String token);

	@POST
	@Path("api/autoCadastro/recuperarSenha")
	CieloResponseDTO<CieloMessageDTO> recuperarSenha(
			@FormParam(ESTABLISHMENT_ID_PARAM_NAME) String estabelecimentoComercial,
			@FormParam("usuario") String usuario, @FormParam("cpfUsuario") String cpfUsuario,
			@FormParam("banco") String banco, @FormParam("agencia") String agencia,
			@FormParam("numeroConta") String numeroConta, @FormParam("senhaBasica") String senhaBasica,
			@FormParam("confSenhaBasica") String confSenhaBasica,
			@FormParam("senhaAvancada") String senhaAvancada,
			@FormParam("confSenhaAvancada") String confSenhaAvancada);

	@POST
	@Path("api/autoCadastro/recuperarEC")
	CieloResponseDTO<List<String>> recuperarEC(@FormParam("cpfCnpj") String cpfCnpj);

	@POST
	@Path("api/autoCadastro/recuperarLogin")
	CieloResponseDTO<String> recuperarLogin(
			@FormParam(ESTABLISHMENT_ID_PARAM_NAME) String estabelecimentoComercial,
			@FormParam("cpfUsuario") String cpfUsuario);

	@GET
	@Path("api/extrato/consultarComprovanteVenda")
	Response consultarComprovanteVenda(@QueryParam(TOKEN_PARAM_NAME) String token,
			@QueryParam(ESTABLISHMENT_ID_PARAM_NAME) String estabelecimentoComercial,
			@QueryParam("dataInicio") String dataInicial, @QueryParam("dataFim") String dataFinal,
			@QueryParam("bandeiras") List<Integer> bandeiras,
			@QueryParam("categoriaProduto") String categoriaProduto);

	@GET
	@Path("api/dadosCadastrais/consultar")
	Response consultarDadosCadastrais(@QueryParam(TOKEN_PARAM_NAME) String token,
			@QueryParam(ESTABLISHMENT_ID_PARAM_NAME) String estabelecimentoComercial);

	@GET
	@Path("api/dadosBancario/listar")
	Response listarBancos();

	@POST
	@Path("/api/dadosBancario/validar")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	ValidarBancoResponseDTO validarDadosBancarios(
			@HeaderParam("nroEstabelecimento") String nroEstabelecimento, @HeaderParam("banco") String banco,
			@HeaderParam("agencia") String agencia, @HeaderParam("conta") String conta,
			@HeaderParam("digito") String digito, @HeaderParam("tipoConta") String tipoConta);

	@POST
	@Path("/api/autoCadastro/cadastrar")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	AutoCadastroResponseDTO realizarAutoCadastro(
			@HeaderParam("nroEstabelecimento") String nroEstabelecimento,
			@HeaderParam("tipoPessoa") AutoCadastroTipoPessoa tipoPessoa,
			@HeaderParam("cpfCnpj") String cpfCnpj, @HeaderParam("nomeUsuario") String nomeUsuario,
			@HeaderParam("senhaBasica") String senhaBasica,
			@HeaderParam("senhaAvancada") String senhaAvancada, @HeaderParam("login") String login,
			@HeaderParam("cpfUsuario") String cpfUsuario, @HeaderParam("rgUsuario") String rgUsuario,
			@HeaderParam("dtNascimento") String dtNascimento, @HeaderParam("email") String email);

	@POST
	@Path("/api/email/agendamentos")
	CieloResponseDTO<Object> enviarEmailAgendamentoCieloPromo(
			@QueryParam(TOKEN_PARAM_NAME) String token,
			@QueryParam(ESTABLISHMENT_ID_PARAM_NAME) String estabelecimentoComercial,
			@QueryParam("codigoClassOferta") int codigoClassOferta,
			@QueryParam("conteudo") String conteudo);
	
	@GET
	@Path("/api/dadosCadastrais/consultarGestor")
	CieloResponseDTO<GestorResponseDTO> consultarGestor(
			@QueryParam(ESTABLISHMENT_ID_PARAM_NAME) String ec,
			@QueryParam(TOKEN_PARAM_NAME) String token);

	@POST
	@Path("/api/autoCadastro/isMassiva")
	MassivaResponseDTO isMassiva(
			@FormParam(ESTABLISHMENT_ID_PARAM_NAME) String ec);
	
}

