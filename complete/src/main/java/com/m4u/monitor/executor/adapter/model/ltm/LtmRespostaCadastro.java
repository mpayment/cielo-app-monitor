package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class LtmRespostaCadastro {

	private Boolean cadastroRealizado;

	public Boolean getCadastroRealizado() {
		return cadastroRealizado;
	}

	public void setCadastroRealizado(Boolean cadastroRealizado) {
		this.cadastroRealizado = cadastroRealizado;
	}

	@Override
	public String toString() {
		return "LtmRespostaCadastro [cadastroRealizado=" + cadastroRealizado + "]";
	}
}