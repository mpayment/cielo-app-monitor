package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlRootElement(name = "Resultado")
@XmlAccessorType(XmlAccessType.FIELD)
public class CampanhaBurningDTO {

	@XmlElement(name = "IdBurningCampanha")
	private long idBurningCampanha;
	
	@XmlElement(name = "Nome")
	private String nome;
	
	@XmlElement(name = "DataInicio")
	private XMLGregorianCalendar dataInicio;
	
	@XmlElement(name = "DataFim")
	private XMLGregorianCalendar dataFim;
	
	@XmlElement(name = "ImagemLogo")
	private String imagemLogo;
	
	/**
	 * @See {@link CampanhaBurningTemplate}
	 */
	@XmlAttribute(name = "Template")
	private String template;
	
	@XmlElement(name = "CompraAberta")
	private boolean compraAberta;
	
	@XmlAttribute(name = "Descricao")
	private String descricao;
	
	@XmlAttribute(name = "DescricaoCompleta")
	private String descricaoCompleta;
	
	@XmlElement(name = "FlPremiacaoInstantanea")
	private boolean flPremiacaoInstantanea;
	
	public boolean isFlPremiacaoInstantanea() {
		return flPremiacaoInstantanea;
	}

	public void setFlPremiacaoInstantanea(boolean flPremiacaoInstantanea) {
		this.flPremiacaoInstantanea = flPremiacaoInstantanea;
	}

	public long getIdBurningCampanha() {
		return idBurningCampanha;
	}

	public void setIdBurningCampanha(long idBurningCampanha) {
		this.idBurningCampanha = idBurningCampanha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public XMLGregorianCalendar getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(XMLGregorianCalendar dataInicio) {
		this.dataInicio = dataInicio;
	}

	public XMLGregorianCalendar getDataFim() {
		return dataFim;
	}

	public void setDataFim(XMLGregorianCalendar dataFim) {
		this.dataFim = dataFim;
	}

	public String getImagemLogo() {
		return imagemLogo;
	}

	public void setImagemLogo(String imagemLogo) {
		this.imagemLogo = imagemLogo;
	}

	public boolean isCompraAberta() {
		return compraAberta;
	}

	public void setCompraAberta(boolean compraAberta) {
		this.compraAberta = compraAberta;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricaoCompleta() {
		return descricaoCompleta;
	}

	public void setDescricaoCompleta(String descricaoCompleta) {
		this.descricaoCompleta = descricaoCompleta;
	}

	@Override
	public String toString() {
		return "CampanhaBurningDTO [idBurningCampanha=" + idBurningCampanha
				+ ", nome=" + nome + ", dataInicio=" + dataInicio
				+ ", dataFim=" + dataFim + ", imagemLogo=" + imagemLogo
				+ ", compraAberta=" + compraAberta
				+ ", flPremiacaoInstantanea=" + flPremiacaoInstantanea
				+ ", template=" + template + ", descricao=" + descricao
				+ ", descricaoCompleta=" + descricaoCompleta + "]";
	}
}