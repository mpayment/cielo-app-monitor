package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class WPShoppingCartItem {
	
	@XmlAttribute(name="Quantidade")
	private int quantidade = 1;
	
	@XmlAttribute(name="ValorProdutoPontos")
	private float valorProdutoPontos;
	
	@XmlElement(name="Produto")
	private WPShoppingCartProduct produto; 

	@XmlElement(name="ProdutoSku")
	private WPShoppingCartProductSku produtoSku;

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		if (quantidade >= 1) {
			this.quantidade = quantidade;
		}
	}

	public float getValorProdutoPontos() {
		return valorProdutoPontos;
	}

	public void setValorProdutoPontos(float valorProdutoPontos) {
		this.valorProdutoPontos = valorProdutoPontos;
	}

	public WPShoppingCartProduct getProduto() {
		return produto;
	}

	public void setProduto(WPShoppingCartProduct produto) {
		this.produto = produto;
	}

	public WPShoppingCartProductSku getProdutoSku() {
		return produtoSku;
	}

	public void setProdutoSku(WPShoppingCartProductSku produtoSku) {
		this.produtoSku = produtoSku;
	}

	@Override
	public String toString() {
		return "WPShoppingCartItem [quantidade=" + quantidade
				+ ", valorProdutoPontos=" + valorProdutoPontos + ", produto="
				+ produto + ", produtoSku=" + produtoSku + "]";
	}	
}