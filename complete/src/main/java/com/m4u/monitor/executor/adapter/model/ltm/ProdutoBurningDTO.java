package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class ProdutoBurningDTO {

	@XmlAttribute(name = "IdBurningProduto")
	private long idBurningProduto;

	@XmlAttribute(name = "Nome")
	private String nome;

	@XmlAttribute(name = "ValorPontos")
	private String valorPontos;

	public long getIdBurningProduto() {
		return idBurningProduto;
	}

	public void setIdBurningProduto(long idBurningProduto) {
		this.idBurningProduto = idBurningProduto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getValorPontos() {
		return valorPontos;
	}

	public void setValorPontos(String valorPontos) {
		this.valorPontos = valorPontos;
	}

	@Override
	public String toString() {
		return "ProdutoBurningDTO [idBurningProduto=" + idBurningProduto
				+ ", nome=" + nome + ", valorPontos=" + valorPontos + "]";
	}
}