package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Resultado")
@XmlAccessorType(XmlAccessType.NONE)
public class LtmLoginAttributes {

    @XmlAttribute(name = "Id")
    private String id;

    @XmlAttribute(name = "Nome")
    private String nome;

    @XmlAttribute(name = "Perfil")
    private String perfil;

    @XmlAttribute(name = "IdPerfil")
    private String idPerfil;

    @XmlAttribute(name = "AcessoWP")
    private String acessoWP;

    @XmlAttribute(name = "IdCampanha")
    private Integer idCampanha;

    @XmlAttribute(name = "CodigoAcessoWP")
    private String codigoAcessoWP;

    @XmlAttribute(name = "SenhaTemporaria")
    private String senhaTemporaria;

    @XmlAttribute(name = "DataUltimoAcesso")
    private String dataUltimoAcesso;

    @XmlAttribute(name = "IdCampanhaECommerceWP")
    private Integer idCampanhaECommerceWP;

    @XmlAttribute(name = "IdEmpresaGestaoPontosWP")
    private Integer idEmpresaGestaoPontosWP;

    @XmlAttribute(name = "idCampanhaGestaoPontosWP")
    private Integer idCampanhaGestaoPontosWP;

    @XmlAttribute(name = "idParceiroGestaoPontosWP")
    private Integer idParceiroGestaoPontosWP;

    public String getAcessoWP() {
        return acessoWP;
    }

    public void setAcessoWP(final String acessoWP) {
        this.acessoWP = acessoWP;
    }

    public String getCodigoAcessoWP() {
        return codigoAcessoWP;
    }

    public void setCodigoAcessoWP(final String codigoAcessoWP) {
        this.codigoAcessoWP = codigoAcessoWP;
    }

    public String getDataUltimoAcesso() {
        return dataUltimoAcesso;
    }

    public void setDataUltimoAcesso(final String dataUltimoAcesso) {
        this.dataUltimoAcesso = dataUltimoAcesso;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public Integer getIdCampanha() {
        return idCampanha;
    }

    public void setIdCampanha(final Integer idCampanha) {
        this.idCampanha = idCampanha;
    }

    public Integer getIdCampanhaECommerceWP() {
        return idCampanhaECommerceWP;
    }

    public void setIdCampanhaECommerceWP(final Integer idCampanhaECommerceWP) {
        this.idCampanhaECommerceWP = idCampanhaECommerceWP;
    }

    public Integer getIdCampanhaGestaoPontosWP() {
        return idCampanhaGestaoPontosWP;
    }

    public void setIdCampanhaGestaoPontosWP(final Integer idCampanhaGestaoPontosWP) {
        this.idCampanhaGestaoPontosWP = idCampanhaGestaoPontosWP;
    }

    public Integer getIdEmpresaGestaoPontosWP() {
        return idEmpresaGestaoPontosWP;
    }

    public void setIdEmpresaGestaoPontosWP(final Integer idEmpresaGestaoPontosWP) {
        this.idEmpresaGestaoPontosWP = idEmpresaGestaoPontosWP;
    }

    public Integer getIdParceiroGestaoPontosWP() {
        return idParceiroGestaoPontosWP;
    }

    public void setIdParceiroGestaoPontosWP(final Integer idParceiroGestaoPontosWP) {
        this.idParceiroGestaoPontosWP = idParceiroGestaoPontosWP;
    }

    public String getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(final String idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(final String perfil) {
        this.perfil = perfil;
    }

    public String getSenhaTemporaria() {
        return senhaTemporaria;
    }

    public void setSenhaTemporaria(final String senhaTemporaria) {
        this.senhaTemporaria = senhaTemporaria;
    }

    @Override
    public String toString() {
        return "LtmLoginAttributes [id=" + id + ", nome=" + nome + ", perfil=" + perfil + ", idPerfil=" + idPerfil + ", acessoWP="
                + acessoWP + ", idCampanha=" + idCampanha + ", codigoAcessoWP=" + codigoAcessoWP + ", senhaTemporaria=" + senhaTemporaria
                + ", dataUltimoAcesso=" + dataUltimoAcesso + ", idCampanhaECommerceWP=" + idCampanhaECommerceWP
                + ", idEmpresaGestaoPontosWP=" + idEmpresaGestaoPontosWP + ", idCampanhaGestaoPontosWP=" + idCampanhaGestaoPontosWP
                + ", idParceiroGestaoPontosWP=" + idParceiroGestaoPontosWP + "]";
    }
}