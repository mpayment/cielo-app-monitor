package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class WPCalculateDeliveryRequest extends WPRequest {

	@XmlElement(name = "IdWsSessao")
	private String idWsSessao;
	
	@XmlElement(name="Campanha")
	private WPCampaign campanha;
	
	@XmlElement(name="Cep")
	private String cep;
	
	@XmlElement(name="EServProdutoSku")
	private List<WPProductSku> eServProdutoSku;
	
	public String getIdWsSessao() {
		return idWsSessao;
	}

	public void setIdWsSessao(String idWsSessao) {
		this.idWsSessao = idWsSessao;
	}

	public WPCampaign getCampanha() {
		return campanha;
	}

	public void setCampanha(WPCampaign campanha) {
		this.campanha = campanha;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public List<WPProductSku> geteServProdutoSku() {
		if (eServProdutoSku == null) {
			eServProdutoSku = new ArrayList<>();
		}
		return eServProdutoSku;
	}

	public void seteServProdutoSku(List<WPProductSku> eServProdutoSku) {
		this.eServProdutoSku = eServProdutoSku;
	}
}