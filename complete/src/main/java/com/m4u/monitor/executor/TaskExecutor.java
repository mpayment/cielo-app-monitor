package com.m4u.monitor.executor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.m4u.monitor.commons.util.Util;
import com.m4u.monitor.executor.processor.DoLoadParameter;
import com.m4u.monitor.executor.sentinel.SentinelAlertCielo;
import com.m4u.monitor.executor.sentinel.SentinelAlertLtm;
import com.m4u.monitor.executor.service.CieloService;
import com.m4u.monitor.executor.verifier.VerifierCielo;
import com.m4u.monitor.executor.verifier.VerifierLTM;
import com.m4u.monitor.model.MonitorApiDTO;
import com.m4u.monitor.model.StateCtrlDTO;


@Component
public class TaskExecutor {

	@Autowired
	private CieloService cieloService;

	@Autowired
	private DoLoadParameter doLoadParameter;

	@Autowired
	private Util nMsg;
	
	@Autowired
	private VerifierCielo vCielo;

	@Autowired
	private VerifierLTM vLtm;
	
	@Autowired
	private SentinelAlertCielo sentinel;

	@Autowired
	private SentinelAlertLtm sentinel2;

	

	private StateCtrlDTO state = new StateCtrlDTO();
	private MonitorApiDTO matrix = new MonitorApiDTO();

	
	@Scheduled(fixedDelay = 60000)
	public void executeCallService() {

		matrix = doLoadParameter.processDoLoad(matrix);
		
		matrix = vCielo.verifyPlataform(matrix); 
		if(!matrix.isApi_online_cielo()){
			if(matrix.isApi_online_cielo() != state.isApi_online_cielo()){
				sentinel.alertCieloOff();
				state.setApi_online_cielo(false);
				state.setProcess_alert(true);
			} else {
				nMsg.disp("Cielo API {OFFLINE} - Cancelando e-mail, já enviado. " + Util.timeNow());
			}
		} else {
			nMsg.disp("Cielo API {ONLINE} - " + Util.timeNow());
			if(matrix.isApi_online_cielo() != state.isApi_online_cielo()){
				sentinel.alertCieloOn();
				state.setApi_online_cielo(true);
			}
		}
		

		matrix = vLtm.verifyPlataform(matrix);
		if(!matrix.isApi_online_ltm()){
			if(matrix.isApi_online_ltm() != state.isApi_online_ltm()){
				sentinel2.alertLtmOff();
				state.setApi_online_ltm(false);
				state.setProcess_alert(true);
			} else {
				nMsg.disp("LTM API {OFFLINE} - Cancelando e-mail, já enviado. " + Util.timeNow());
			}
		} else {
			nMsg.disp("LTM   API {ONLINE} - "  + Util.timeNow());
			if(matrix.isApi_online_ltm() != state.isApi_online_ltm()){
				sentinel2.alertLtmOn();
				state.setApi_online_ltm(true);
			}
		}
		
	}

}

