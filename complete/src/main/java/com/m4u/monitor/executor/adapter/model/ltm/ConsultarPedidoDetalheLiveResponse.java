package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlElement;

public class ConsultarPedidoDetalheLiveResponse {

	@XmlElement(name = "JsonReturn")
	private DetalheTransacaoExtrato jsonReturn;

	public DetalheTransacaoExtrato getJsonReturn() {
		return jsonReturn;
	}

	public void setJsonReturn(DetalheTransacaoExtrato jsonReturn) {
		this.jsonReturn = jsonReturn;
	}
}