package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPProductSku {
	
	@XmlAttribute(name="Descricao")
	private String descricao;
	
	@XmlAttribute(name="IdProdutoSku")
	private Long idProdutoSku;
	
	@XmlAttribute(name="IdProdutoSkuOriginal")
	private Long idProdutoSkuOriginal;
	
	@XmlAttribute(name="Quantidade")
	private Integer quantidade;
	
	@XmlAttribute(name="UrlImagem")
	private String urlImagem;
	
	@XmlAttribute(name="UrlImagemGrande")
	private String urlImagemGrande;
	
	@XmlAttribute(name="UrlImagemMedia")
	private String urlImagemMedia;
	
	@XmlAttribute(name="Valor")
	private Float valor;
	
	@XmlAttribute(name="ValorPontos")
	private Float valorPontos;
	
	@XmlAttribute(name="ValorReais")
	private Float valorReais;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getIdProdutoSku() {
		return idProdutoSku;
	}

	public void setIdProdutoSku(Long idProdutoSku) {
		this.idProdutoSku = idProdutoSku;
	}

	public Long getIdProdutoSkuOriginal() {
		return idProdutoSkuOriginal;
	}

	public void setIdProdutoSkuOriginal(Long idProdutoSkuOriginal) {
		this.idProdutoSkuOriginal = idProdutoSkuOriginal;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}

	public String getUrlImagemGrande() {
		return urlImagemGrande;
	}

	public void setUrlImagemGrande(String urlImagemGrande) {
		this.urlImagemGrande = urlImagemGrande;
	}

	public String getUrlImagemMedia() {
		return urlImagemMedia;
	}

	public void setUrlImagemMedia(String urlImagemMedia) {
		this.urlImagemMedia = urlImagemMedia;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	public Float getValorPontos() {
		return valorPontos;
	}

	public void setValorPontos(Float valorPontos) {
		this.valorPontos = valorPontos;
	}

	public Float getValorReais() {
		return valorReais;
	}

	public void setValorReais(Float valorReais) {
		this.valorReais = valorReais;
	}

	@Override
	public String toString() {
		return "WPProductSku ["
				+ (descricao != null ? "descricao=" + descricao + ", " : "")
				+ (idProdutoSku != null ? "idProdutoSku=" + idProdutoSku + ", "
						: "")
				+ (idProdutoSkuOriginal != null ? "idProdutoSkuOriginal="
						+ idProdutoSkuOriginal + ", " : "")
				+ (quantidade != null ? "quantidade=" + quantidade + ", " : "")
				+ (urlImagem != null ? "urlImagem=" + urlImagem + ", " : "")
				+ (urlImagemGrande != null ? "urlImagemGrande="
						+ urlImagemGrande + ", " : "")
				+ (urlImagemMedia != null ? "urlImagemMedia=" + urlImagemMedia
						+ ", " : "")
				+ (valor != null ? "valor=" + valor + ", " : "")
				+ (valorPontos != null ? "valorPontos=" + valorPontos + ", "
						: "")
				+ (valorReais != null ? "valorReais=" + valorReais : "") + "]";
	}
}