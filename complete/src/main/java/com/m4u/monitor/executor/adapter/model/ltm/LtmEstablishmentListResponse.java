package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.List;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Resultado")
@XmlAccessorType(XmlAccessType.NONE)
public class LtmEstablishmentListResponse extends LtmAbstractResponse {
	
    @XmlElement(name="Resultado")
    @XmlList
	private List<LtmEstablishment> establishmentList;

	public List<LtmEstablishment> getEstablishmentList() {
		return establishmentList;
	}

	public void setEstablishmentList(List<LtmEstablishment> establishmentList) {
		this.establishmentList = establishmentList;
	}

	@Override
	public String toString() {
		return "LtmEstablishmentListResponse [establishmentList="
				+ establishmentList + "]" + super.toString();
	}			
}