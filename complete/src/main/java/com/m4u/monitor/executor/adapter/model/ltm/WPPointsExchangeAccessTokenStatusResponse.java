package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPPointsExchangeAccessTokenStatusResponse {
	
	@XmlAttribute(name="Token")
	private String token;
	
	@XmlAttribute(name="Code")
	private String code;
	
	@XmlAttribute(name="Text")
	private String text;
	
	@XmlAttribute(name="Value")
	private String value;
	
	@XmlAttribute(name="Sucess")
	private boolean success;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@Override
	public String toString() {
		return "WPPointsExchangeResponse [token=" + token + ", code=" + code
				+ ", text=" + text + ", value=" + value + ", success="
				+ success + "]";
	}
}
