package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPEServProductSku {

	@XmlAttribute(name="IdProdutoSkuOriginal")
	private Long idProdutoSkuOriginal;
	
	@XmlAttribute(name="Quantidade")
	private int quantidade;

	public Long getIdProdutoSkuOriginal() {
		return idProdutoSkuOriginal;
	}

	public void setIdProdutoSkuOriginal(Long idProdutoSkuOriginal) {
		this.idProdutoSkuOriginal = idProdutoSkuOriginal;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public String toString() {
		return "WPEServProductSku [idProdutoSkuOriginal="
				+ idProdutoSkuOriginal + ", quantidade=" + quantidade + "]";
	}
}