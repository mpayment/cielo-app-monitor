package com.m4u.monitor.executor.adapter;

import com.m4u.monitor.executor.adapter.model.ltm.ConsultarCreditosResgatesResponse;
import com.m4u.monitor.executor.adapter.model.ltm.ConsultarExtratoParticipanteResponse;
import com.m4u.monitor.executor.adapter.model.ltm.ConsultarPedidoDetalheLiveResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.m4u.monitor.executor.adapter.model.ltm.LtmCustomerStatusResponse;
import com.m4u.monitor.executor.adapter.model.ltm.LtmEmailValidationResponse;
import com.m4u.monitor.executor.adapter.model.ltm.LtmEstablishmentListResponse;
import com.m4u.monitor.executor.adapter.model.ltm.LtmFirstAccessRequest;
import com.m4u.monitor.executor.adapter.model.ltm.LtmFirstAccessResponse;
import com.m4u.monitor.executor.adapter.model.ltm.LtmLoginAttributes;
import com.m4u.monitor.executor.adapter.model.ltm.LtmLoginRequest;
import com.m4u.monitor.executor.adapter.model.ltm.LtmNavigationLogSavingRequest;
import com.m4u.monitor.executor.adapter.model.ltm.LtmNavigationLogSavingResponse;
import com.m4u.monitor.executor.adapter.model.ltm.LtmPostUserRegistryRequest;
import com.m4u.monitor.executor.adapter.model.ltm.LtmRegistryCompletionRequest;
import com.m4u.monitor.executor.adapter.model.ltm.LtmRegulation;
import com.m4u.monitor.executor.adapter.model.ltm.LtmResponse;
import com.m4u.monitor.executor.adapter.model.ltm.LtmSaveRegistryCompletionResponse;
import com.m4u.monitor.executor.adapter.model.ltm.LtmSendMyPasswordRequest;
import com.m4u.monitor.executor.adapter.model.ltm.LtmSendMyPasswordResponse;
import com.m4u.monitor.executor.adapter.model.ltm.LtmUpdatePasswordAttribute;
import com.m4u.monitor.executor.adapter.model.ltm.LtmUpdatePasswordRequest;
import com.m4u.monitor.executor.adapter.model.ltm.LtmUserPointsExpirationResponse;
import com.m4u.monitor.executor.adapter.model.ltm.LtmUserRegistryResponse;

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface LtmApi {

	String CIELO_API_KEY_HEADER_NAME = "X-CIELO-API-Key";
	
	@GET
    @Path("Participante/ObterCampanhaOrigemStatusMobicare")
    LtmResponse<LtmCustomerStatusResponse> nextAction(
    		@QueryParam("Cnpj") String cnpj, 
    		@QueryParam("NumeroEc") String ec, 
    		@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey);

    @POST
    @Path("Participante/Login")
    LtmResponse<LtmLoginAttributes> doLogin(LtmLoginRequest loginRequest, 
    		@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey);
    
    @POST
	@Path("Participante/EsqueciSenha")
	LtmResponse<LtmSendMyPasswordResponse> sendMyPassword(
			@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey, 
			LtmSendMyPasswordRequest request);
    
    @POST
    @Path("Participante/PrimeiroAcesso")
    LtmResponse<LtmFirstAccessResponse> firstAccess(
    		@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey, 
    		LtmFirstAccessRequest parameters);

    @POST
	@Path("Participante/AlterarSenhaTemporaria")
	LtmResponse<LtmUpdatePasswordAttribute> updateMyPassword(
			@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey, 
			LtmUpdatePasswordRequest request);

    @GET
    @Path("Participante/ConsultarECs")
    LtmEstablishmentListResponse getEcList(
    		@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey, 
    		@QueryParam("IdParticipante") String userId);

    @POST
    @Path("Participante/SalvarDadosComplementares")
    LtmResponse<LtmSaveRegistryCompletionResponse> saveRegistryCompletion(
    		@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey, 
    		LtmRegistryCompletionRequest request);
    
	@GET
	@Path("Participante/DataProximaExpiracaoPontos")
	LtmResponse<LtmUserPointsExpirationResponse> getPointsExpirationDate(
			@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey, 
			@QueryParam("idParticipante") String idParticipante);
	
	@GET
	@Path("MeuCadastro/ListaMeuCadastro")
	LtmResponse<LtmUserRegistryResponse> getUserRegistry(
			@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey, 
			@QueryParam("IdParticipante") String idParticipante);
	
	@POST
	@Path("MeuCadastro/MeuCadastroEditaFormulario")
	LtmResponse<LtmSaveRegistryCompletionResponse> postUserRegistry(
			@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey, 
			LtmPostUserRegistryRequest request);

    @GET
    @Path("Regulamento/RegulamentoDeslogado")
    LtmResponse<LtmRegulation> loggedoffRegulation(@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey);

    @GET
    @Path("Participante/ValidarEmail")
    LtmResponse<LtmEmailValidationResponse> validateEmail(
    		@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey, 
    		@QueryParam("Email") String email, 
    		@QueryParam("IdParticipante") String idParticipante, 
    		@QueryParam("IdCampanha") Integer idCampanha);
    
    @GET
    @Path("Participante/ValidarCelular")
    LtmResponse<Boolean> validateCellphoneForNewCustomer(
    		@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey, 
    		@QueryParam("celular") String cellphoneWithAreaCode, 
    		@QueryParam("IdParticipante") String customerId, 
    		@QueryParam("IdCampanha") int campaignId);
    
    @POST
    @Path("Log/GravarLogAcessoCompleto")
    LtmResponse<LtmNavigationLogSavingResponse> saveNavigationLog(
    		@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey, 
    		LtmNavigationLogSavingRequest request);
	
	@GET
	@Path("Participante/ConsultarCreditosResgates")
	LtmResponse<ConsultarCreditosResgatesResponse> consultarCreditosResgates(
			@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey,
			@QueryParam("IdParticipante") long idParticipante);
	
	@GET
	@Path("Participante/ConsultarExtratoParticipante")
	LtmResponse<ConsultarExtratoParticipanteResponse> consultarExtratoParticipante(
			@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey,
			@QueryParam("loginParticipante") String loginParticipante,
			@QueryParam("dataInicio") String dataInicio,
			@QueryParam("dataFim") String dataFim/*,
			@QueryParam("tag") String tag,
			@QueryParam("transactionTypeId") Integer transactionTypeId*/);
	
	@GET
	@Path("Participante/ConsultarPedidoDetalheLive")
	LtmResponse<ConsultarPedidoDetalheLiveResponse> consultarPedidoDetalheLive(
			@HeaderParam(CIELO_API_KEY_HEADER_NAME) String apiKey,
			@QueryParam("participantId") long participantId,
			@QueryParam("orderId") long orderId);
}