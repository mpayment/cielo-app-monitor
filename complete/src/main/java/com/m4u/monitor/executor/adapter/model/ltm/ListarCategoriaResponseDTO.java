package com.m4u.monitor.executor.adapter.model.ltm;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ListarCategoriaResponseDTO {

	@XmlElement(name = "Categorias")
	private List<CategoriaVitrineDTO> categorias;

	public List<CategoriaVitrineDTO> getCategorias() {
		return categorias;
	}

	@Override
	public String toString() {
		return "ListarCategoriaResponseDTO [categorias=" + categorias + "]";
	}
}