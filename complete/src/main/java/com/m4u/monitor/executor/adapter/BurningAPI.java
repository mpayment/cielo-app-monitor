package com.m4u.monitor.executor.adapter;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.m4u.monitor.executor.adapter.model.ltm.CampanhaBurningDTO;
import com.m4u.monitor.executor.adapter.model.ltm.ConteudoTextoBurningDTO;
import com.m4u.monitor.executor.adapter.model.ltm.LtmResponse;
import com.m4u.monitor.executor.adapter.model.ltm.ProdutoBurningDTO;
import com.m4u.monitor.executor.adapter.model.ltm.ResgateBurningRequestDTO;
import com.m4u.monitor.executor.adapter.model.ltm.ResgateBurningResponseDTO;
import com.m4u.monitor.executor.adapter.model.ltm.ResultadoSorteioBurningDTO_v2_3;
import com.m4u.monitor.executor.adapter.model.ltm.ResultadoSorteioBurningDTO_v2_7;
import com.m4u.monitor.executor.adapter.model.ltm.SorteioBurningDTO;



@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
interface BurningAPI {
	
	String API_KEY_HEADER_NAME = "X-CIELO-API-Key";
	String CUSTOMER_ID_PARAM_NAME = "IdParticipante";
	String CAMPAIGN_ID_PARAM_NAME = "IdBurningCampanha";
	String RAFFLE_ID_PARAM_NAME = "IdBurningSorteio";

	@GET
	@Path("Burning/ObterCampanhasVigentes")
	LtmResponse<List<CampanhaBurningDTO>> obterCampanhasVigentes(
			@QueryParam(CUSTOMER_ID_PARAM_NAME) long idParticipante, 
			@HeaderParam(API_KEY_HEADER_NAME) String apiKey);
	
	@GET
	@Path("Burning/ObterRegulamento")
	LtmResponse<ConteudoTextoBurningDTO> obterRegulamento(
			@QueryParam(CUSTOMER_ID_PARAM_NAME) long idParticipante,
			@QueryParam(CAMPAIGN_ID_PARAM_NAME) long idBurningCampanha,
			@HeaderParam(API_KEY_HEADER_NAME) String apiKey);
	
	@GET
	@Path("Burning/ObterFAQ")
	LtmResponse<ConteudoTextoBurningDTO> obterFaq(
			@QueryParam(CUSTOMER_ID_PARAM_NAME) long idParticipante,
			@QueryParam(CAMPAIGN_ID_PARAM_NAME) long idBurningCampanha,
			@HeaderParam(API_KEY_HEADER_NAME) String apiKey);
	
	@GET
	@Path("Burning/ObterSorteios")
	LtmResponse<List<SorteioBurningDTO>> obterSorteios(
			@QueryParam(CUSTOMER_ID_PARAM_NAME) long idParticipante,
			@QueryParam(CAMPAIGN_ID_PARAM_NAME) long idBurningCampanha,
			@HeaderParam(API_KEY_HEADER_NAME) String apiKey);
	
	@GET
	@Path("Burning/ObterCupons")
	LtmResponse<List<ResultadoSorteioBurningDTO_v2_7>> obterCupons_v2_7(
			@QueryParam(CUSTOMER_ID_PARAM_NAME) long idParticipante,
			@QueryParam(CAMPAIGN_ID_PARAM_NAME) long idBurningCampanha,
			@HeaderParam(API_KEY_HEADER_NAME) String apiKey);
	
	@GET
	@Path("Burning/ObterCupons")
	LtmResponse<List<ResultadoSorteioBurningDTO_v2_3>> obterCupons_v2_3(
			@QueryParam(CUSTOMER_ID_PARAM_NAME) long idParticipante,
			@QueryParam(CAMPAIGN_ID_PARAM_NAME) long idBurningCampanha,
			@HeaderParam(API_KEY_HEADER_NAME) String apiKey);
	
	@GET
	@Path("Burning/ObterCuponsInstantaneos")
	LtmResponse<List<ResultadoSorteioBurningDTO_v2_7>> obterCuponsInstantaneos(
			@QueryParam(CUSTOMER_ID_PARAM_NAME) long idParticipante,
			@QueryParam(CAMPAIGN_ID_PARAM_NAME) long idBurningCampanha,
			@HeaderParam(API_KEY_HEADER_NAME) String apiKey);
	
	@GET
	@Path("Burning/ObterProdutos")
	LtmResponse<List<ProdutoBurningDTO>> obterProdutos(
			@QueryParam(CUSTOMER_ID_PARAM_NAME) long idParticipante,
			@QueryParam(CAMPAIGN_ID_PARAM_NAME) long idBurningCampanha,
			@QueryParam(RAFFLE_ID_PARAM_NAME) long idBurningSorteio,
			@HeaderParam(API_KEY_HEADER_NAME) String apiKey);
	
	@POST
	@Path("Burning/Resgatar")
	LtmResponse<ResgateBurningResponseDTO> resgatar(ResgateBurningRequestDTO dadosResgate, 
			@HeaderParam(API_KEY_HEADER_NAME) String apiKey);
}