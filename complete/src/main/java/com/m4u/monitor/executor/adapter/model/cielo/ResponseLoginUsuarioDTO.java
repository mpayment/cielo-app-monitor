package com.m4u.monitor.executor.adapter.model.cielo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseLoginUsuarioDTO {

	private UsuarioCieloDTO usuario;
	private String token;
	private EstabelecimentoClienteDTO estabelecimento;
	private final List<FilialClienteDTO> listaFiliais = new ArrayList<FilialClienteDTO>();
	
	public EstabelecimentoClienteDTO getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(EstabelecimentoClienteDTO estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public List<FilialClienteDTO> getListaFiliais() {
		return listaFiliais;
	}

	public UsuarioCieloDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioCieloDTO usuario) {
		this.usuario = usuario;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "ResponseLoginUsuarioDTO [usuario=" + usuario + ", token="
				+ token + ", estabelecimento=" + estabelecimento
				+ ", listaFiliais=" + listaFiliais + "]";
	}
}