package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import java.util.ArrayList;
import java.util.List;

public class WPShoppingCart {

    @XmlElement(name = "ItensCesta")
    @XmlList
    private List<WPShoppingCartItem> itensCesta;

    @XmlAttribute(name = "ValorFreteReais")
    private String valorFreteReais;

    public List<WPShoppingCartItem> getItensCesta() {
        if (itensCesta == null) {
            itensCesta = new ArrayList<WPShoppingCartItem>();
        }
        return itensCesta;
    }

    public String getValorFreteReais() {
        return valorFreteReais;
    }

    public void setValorFreteReais(String valorFreteReais) {
        this.valorFreteReais = valorFreteReais;
    }

    @Override
    public String toString() {
        return "WPShoppingCart{" +
                "itensCesta=" + itensCesta +
                ", valorFreteReais=" + valorFreteReais +
                '}';
    }
}
