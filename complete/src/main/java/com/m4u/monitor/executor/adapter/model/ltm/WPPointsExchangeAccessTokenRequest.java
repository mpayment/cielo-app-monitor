package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAttribute;

public class WPPointsExchangeAccessTokenRequest extends WPRequest {
	
	@XmlAttribute(name="IdCampanha")
	private int idCampanha;
	
	@XmlAttribute(name="TokenSessao")
	private String tokenSessao;

	public int getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(int idCampanha) {
		this.idCampanha = idCampanha;
	}

	public String getTokenSessao() {
		return tokenSessao;
	}

	public void setTokenSessao(String tokenSessao) {
		this.tokenSessao = tokenSessao;
	}

	@Override
	public String toString() {
		return "WPPointsExchangeRequest [idCampanha=" + idCampanha
				+ ", tokenSessao=" + tokenSessao + "]";
	}	
}
