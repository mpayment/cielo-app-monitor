package com.m4u.monitor.executor.sentinel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.m4u.monitor.commons.service.email.EmailWrapper;
import com.m4u.monitor.commons.util.Util;



@Component
public class SentinelAlertCielo {

	@Autowired
	private EmailWrapper emailW;

	
	public boolean alertCieloOff(){
		emailW.setSubject("[CieloApp] API CIELO >> OFFLINE << [HOMOLOG] " + Util.timeNow());
		emailW.setText(cieloOffText());
		emailW.send();
		return true;
	}
	
	public boolean alertCieloOn(){
		emailW.setSubject("[CieloApp] API CIELO > ONLINE < [HOMOLOG] " + Util.timeNow());
		emailW.setText(cieloOnText());
		emailW.send();
		return true;
	}
	
	public String cieloOffText(){
		String body;
		body = "============================================================================\n"             
			 + "           CieloApp - API Monitor - Alerta Automático de Disponibilidade de Serviços  \n"
			 + "============================================================================\n"
			 + " 																		    \n"
			 + " 				API Cielo HOMOLOGAÇÃO - Plataforma OFFLINE 					\n"
			 + "                                                                            \n"
			 + "              Todos os serviços da Cielo estão indisponíveis                \n"			 
			 + " 																		    \n"
			 + "           (*) Uma nova notificação será enviada quando o status mudar para ONLINE    \n"
			 + " 																		    \n"			 
			 + "============================================================================\n"
			 + "                                      M4U Produtos & Serviços (c)2016		        \n"
		     + "============================================================================";
		return body;
	}

	public String cieloOnText(){
		String body;
		body = "============================================================================\n"             
			 + "           CieloApp - API Monitor - Alerta Automático de Disponibilidade de Serviços  \n"
			 + "============================================================================\n"
			 + " 																		    \n"
			 + " 				API Cielo HOMOLOGAÇÃO - Plataforma ONLINE  					\n"  
			 + " 																		    \n"
			 + " 																		    \n"			 
			 + "============================================================================\n"
			 + "                                      M4U Produtos & Serviços (c)2016		        \n"
		     + "============================================================================";
		return body;
	}

	
}
