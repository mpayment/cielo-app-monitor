package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;



@XmlAccessorType(XmlAccessType.FIELD)
public class WPBuscaProdutoRequest {

    @XmlElement(name = "TokenAplicacao")
    private String tokenAplicacao;
    
    @XmlElement(name = "EServToken")
    private WPEServToken eServToken;

    @XmlElement(name = "ValorMinimo")
    private Integer valorMinimo;

    @XmlElement(name = "ValorMaximo")
    private Integer valorMaximo;

    @XmlElement(name = "Busca")
    private String busca;

    @XmlElement(name = "Ordenacao")
    private String ordenacao;

    @XmlElement(name = "IdCliente")
    private Long idCliente;

    @XmlElement(name = "IdCategoria")
    private Long idCategoria;
    
    @XmlElement(name = "Departamento")
    private Long departamento;

	public Long getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Long departamento) {
		this.departamento = departamento;
	}

	public WPEServToken geteServToken() {
		return eServToken;
	}

	public void seteServToken(WPEServToken eServToken) {
		this.eServToken = eServToken;
	}

	public String getTokenAplicacao() {
        return tokenAplicacao;
    }

    public void setTokenAplicacao(String tokenAplicacao) {
        this.tokenAplicacao = tokenAplicacao;
    }

    public Integer getValorMinimo() {
        return valorMinimo;
    }

    public void setValorMinimo(Integer valorMinimo) {
        this.valorMinimo = valorMinimo;
    }

    public Integer getValorMaximo() {
        return valorMaximo;
    }

    public void setValorMaximo(Integer valorMaximo) {
        this.valorMaximo = valorMaximo;
    }

    public String getBusca() {
        return busca;
    }

    public void setBusca(String busca) {
        this.busca = busca;
    }

    public String getOrdenacao() {
        return ordenacao;
    }

    public void setOrdenacao(String ordenacao) {
        this.ordenacao = ordenacao;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    public Long getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Long idCategoria) {
        this.idCategoria = idCategoria;
    }

    @Override
	public String toString() {
		return "WPBuscaProdutoRequest [tokenAplicacao=" + tokenAplicacao + ", eServToken=" + eServToken
				+ ", valorMinimo=" + valorMinimo + ", valorMaximo=" + valorMaximo + ", busca=" + busca
				+ ", ordenacao=" + ordenacao + ", idCliente=" + idCliente + ", idCategoria=" + idCategoria
				+ ", departamento=" + departamento + "]";
	}
}