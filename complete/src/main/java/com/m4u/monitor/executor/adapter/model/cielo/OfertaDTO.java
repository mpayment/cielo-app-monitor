package com.m4u.monitor.executor.adapter.model.cielo;

public class OfertaDTO {
	
	public final static String OFERTAS_DATE_PATTERN = "dd/MM/yyyy";

	private int codigoClassOferta;
	private String nomeClassOferta;
	private int codigoCampanha;
	private String dataInicioCampanha;
	private String dataFimCampanha;
	private int codigoOferta;
	private String dataInicioOferta;
	private String dataFimOferta;
	private int prioridade;
	private String descricaoPreco;
	private String urlBanner;
	private String urlLogotipo;
	
	public String getUrlLogotipo() {
		return urlLogotipo;
	}

	public void setUrlLogotipo(String urlLogotipo) {
		this.urlLogotipo = urlLogotipo;
	}

	public String getUrlBanner() {
		return urlBanner;
	}

	public void setUrlBanner(String urlBanner) {
		this.urlBanner = urlBanner;
	}

	public int getCodigoClassOferta() {
		return codigoClassOferta;
	}

	public void setCodigoClassOferta(int codigoClassOferta) {
		this.codigoClassOferta = codigoClassOferta;
	}

	public String getNomeClassOferta() {
		return nomeClassOferta;
	}

	public void setNomeClassOferta(String nomeClassOferta) {
		this.nomeClassOferta = nomeClassOferta;
	}

	public int getCodigoCampanha() {
		return codigoCampanha;
	}

	public void setCodigoCampanha(int codigoCampanha) {
		this.codigoCampanha = codigoCampanha;
	}

	public String getDataInicioCampanha() {
		return dataInicioCampanha;
	}

	public void setDataInicioCampanha(String dataInicioCampanha) {
		this.dataInicioCampanha = dataInicioCampanha;
	}

	public String getDataFimCampanha() {
		return dataFimCampanha;
	}

	public void setDataFimCampanha(String dataFimCampanha) {
		this.dataFimCampanha = dataFimCampanha;
	}

	public int getCodigoOferta() {
		return codigoOferta;
	}

	public void setCodigoOferta(int codigoOferta) {
		this.codigoOferta = codigoOferta;
	}

	public String getDataInicioOferta() {
		return dataInicioOferta;
	}

	public void setDataInicioOferta(String dataInicioOferta) {
		this.dataInicioOferta = dataInicioOferta;
	}

	public String getDataFimOferta() {
		return dataFimOferta;
	}

	public void setDataFimOferta(String dataFimOferta) {
		this.dataFimOferta = dataFimOferta;
	}

	public int getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(int prioridade) {
		this.prioridade = prioridade;
	}

	public String getDescricaoPreco() {
		return descricaoPreco;
	}

	public void setDescricaoPreco(String descricaoPreco) {
		this.descricaoPreco = descricaoPreco;
	}
}