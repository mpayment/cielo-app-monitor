package com.m4u.monitor.executor.adapter.model.ltm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.m4u.monitor.commons.util.constants.CieloConstants;



@XmlAccessorType(XmlAccessType.FIELD)
public class LtmPedidoCadastroFidelidade {
	
	public static final String CODIGO_PARCEIRO_FOR_MOBICARE = "2";
	public static final String DATE_TIME_PATTERN = CieloConstants.ISO_DATE_TIME_PATTERN;

	private String numeroEc;
	private String cnpj;
	private String nome;
	private String email;
	private String dddCelular;
	private String celular;
	private Boolean newsletter;
	private String cpf;
	private String dataNascimento;
	private String dataAceiteRegulamento;
	private final String codParceiro = CODIGO_PARCEIRO_FOR_MOBICARE;

	public String getNumeroEc() {
		return numeroEc;
	}

	public void setNumeroEc(String numeroEc) {
		this.numeroEc = numeroEc;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDddCelular() {
		return dddCelular;
	}

	public void setDddCelular(String dddCelular) {
		this.dddCelular = dddCelular;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Boolean getNewsletter() {
		return newsletter;
	}

	public void setNewsletter(Boolean newsletter) {
		this.newsletter = newsletter;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getDataAceiteRegulamento() {
		return dataAceiteRegulamento;
	}

	public void setDataAceiteRegulamento(String dataAceiteRegulamento) {
		this.dataAceiteRegulamento = dataAceiteRegulamento;
	}

	public String getCodParceiro() {
		return codParceiro;
	}

	@Override
	public String toString() {
		return "LtmPedidoCadastroFidelidade [numeroEc=" + numeroEc + ", cnpj=" + cnpj + ", nome=" + nome
				+ ", email=" + email + ", dddCelular=" + dddCelular + ", celular=" + celular
				+ ", newsletter=" + newsletter + ", cpf=" + cpf + ", dataNascimento=" + dataNascimento
				+ ", dataAceiteRegulamento=" + dataAceiteRegulamento + ", codParceiro=" + codParceiro
				+ "]";
	}
}