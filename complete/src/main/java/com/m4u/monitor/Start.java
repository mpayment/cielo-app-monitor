package com.m4u.monitor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.m4u.monitor.commons.util.Util;



@SpringBootApplication
public class Start {

	@Autowired
	private static Util nMsg = new Util();
	
//    @PersistenceContext
//    private static EntityManager entityManager;
    
    
	
	public static void main(String[] args) {
		
		SpringApplication.run(Start.class, args);
		nMsg.dispH1("API Monitor iniciando loop de verificação... " + Util.timeNow());

//		nMsg.disp("testing persist");
//		ServiceLog t =  new ServiceLog();
//		t.setApi("TESTE");
//		entityManager.persist(t);
		
	}
	
}


