package com.m4u.monitor.commons.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.m4u.monitor.commons.util.constants.ServerConfigurationKey;

@Entity
@Table(name="server_configuration")
public class ServerConfiguration {

	@Id
	@Column(name="config_key", unique = true, nullable = false)
	@Enumerated(EnumType.STRING)
	private ServerConfigurationKey key;
	
	@Column(name="config_value")
	private String value;
	
	public ServerConfigurationKey getKey() {
		return key;
	}
	
	public void setKey(ServerConfigurationKey key) {
		this.key = key;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}