package com.m4u.monitor.commons.service.email;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;


@Service
public class MessageManager {

    @Value("#{'${spring.mail.message.to}'.split(',')}") 
    private String[] mailTo;

    @Value("${spring.mail.message.from}")
    private String mailFrom;

    String msgSubject;
    String msgText;
    
    
	public SimpleMailMessage buildMessage() {
	    SimpleMailMessage message = new SimpleMailMessage();
	    message.setTo(mailTo);
	    message.setFrom(mailFrom);
	    return message;
	}

	
	public String getMsgSubject() {
		return msgSubject;
	}

	public void setMsgSubject(String msgSubject) {
		this.msgSubject = msgSubject;
	}

	public String getMsgText() {
		return msgText;
	}

	public void setMsgText(String msgText) {
		this.msgText = msgText;
	}
	
}
