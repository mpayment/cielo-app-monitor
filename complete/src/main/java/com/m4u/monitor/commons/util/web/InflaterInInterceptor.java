package com.m4u.monitor.commons.util.web;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import org.apache.cxf.helpers.CastUtils;
import org.apache.cxf.helpers.HttpHeaderHelper;
import org.apache.cxf.interceptor.AttachmentInInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.common.gzip.GZIPOutInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InflaterInInterceptor extends AbstractPhaseInterceptor<Message> {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public InflaterInInterceptor() {
		super(Phase.RECEIVE);
		addBefore(AttachmentInInterceptor.class.getName());
	}

	public void handleMessage(Message message) throws Fault {
		// check for Content-Encoding header - we are only interested in
		// messages that say they are gzipped.
		Map<String, List<String>> protocolHeaders = CastUtils.cast((Map<?, ?>) message
				.get(Message.PROTOCOL_HEADERS));
		if (protocolHeaders != null) {
			List<String> contentEncoding = HttpHeaderHelper.getHeader(protocolHeaders,
					HttpHeaderHelper.CONTENT_ENCODING);
			if (contentEncoding == null) {
				contentEncoding = protocolHeaders.get(GZIPOutInterceptor.SOAP_JMS_CONTENTENCODING);
			}
			if (contentEncoding != null && (contentEncoding.contains("deflate"))) {
//				try {
					logger.debug("Uncompressing response");
					InputStream is = message.getContent(InputStream.class);
					if (is == null) {
						return;
					}

					// wrap an unzipping stream around the original one
					Inflater inflater = new Inflater(true);
					InflaterInputStream compressedDataInput = new InflaterInputStream(is, inflater);
					message.setContent(InputStream.class, compressedDataInput);
//					compressedDataInput.close();

					// remove content encoding header as we've now dealt with it
					for (String key : protocolHeaders.keySet()) {
						if (key.equalsIgnoreCase("Content-Encoding")) {
							protocolHeaders.remove(key);
							break;
						}
					}
					/*
					 * if (isRequestor(message)) { //record the fact that is
					 * worked so future requests will //automatically be FI
					 * enabled Endpoint ep =
					 * message.getExchange().getEndpoint();
					 * ep.put(GZIPOutInterceptor.USE_GZIP_KEY,
					 * GZIPOutInterceptor.UseGzip.YES); }
					 */
//				} catch (IOException ex) {
//					throw new RuntimeException("Unable to close data uncompresser input stream.", ex);
					// throw new Fault(new
					// org.apache.cxf.common.i18n.Message("COULD_NOT_UNZIP",
					// BUNDLE), ex);
//				}
			}
		}
	}
}