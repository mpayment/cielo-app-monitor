package com.m4u.monitor.commons.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class CieloCacheManager {

	@Autowired
	private ServerConfigurationService serverConfigurationService; // must have special handling
	
	private final List<CacheManagerListener> listeners = new ArrayList<>();
	
	public void addListener(CacheManagerListener listener) {
		if (listener instanceof ServerConfigurationService) {
			return;
		}
		
		listeners.add(listener);
	}
	
	public void removeListener(CacheManagerListener listener) {
		listeners.remove(listener);
	}
	
	public void reloadCache() {
		serverConfigurationService.reloadConfigurations();
		
		for (CacheManagerListener listener : listeners) {
			listener.reloadCache();
		}
	}
}