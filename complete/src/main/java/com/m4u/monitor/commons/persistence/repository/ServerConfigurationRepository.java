package com.m4u.monitor.commons.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.m4u.monitor.commons.persistence.model.ServerConfiguration;
import com.m4u.monitor.commons.util.constants.ServerConfigurationKey;



public interface ServerConfigurationRepository extends JpaRepository<ServerConfiguration, ServerConfigurationKey> { }