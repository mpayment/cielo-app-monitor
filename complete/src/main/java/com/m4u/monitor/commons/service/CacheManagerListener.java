package com.m4u.monitor.commons.service;

public interface CacheManagerListener {

	void reloadCache();
}