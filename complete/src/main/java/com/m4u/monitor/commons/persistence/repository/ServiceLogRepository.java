package com.m4u.monitor.commons.persistence.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.m4u.monitor.commons.persistence.model.ServiceLog;



@Repository
@Transactional
public class ServiceLogRepository {
	
    @PersistenceContext
    private EntityManager manager;

    public ServiceLog getLogById(Integer id) {
	  return manager.find(ServiceLog.class, id);
    }

    
    
    
//    public List<EmployeeEntity> getAllEmployees() {
//        List<EmployeeEntity> employees = manager.createQuery("Select a From EmployeeEntity a", EmployeeEntity.class).getResultList();
//        return employees;
//    }
//     
//    public List<DepartmentEntity> getAllDepartments() {
//        List<DepartmentEntity> depts = manager.createQuery("Select a From DepartmentEntity a", DepartmentEntity.class).getResultList();
//        return depts;
//    }
//     
//    public DepartmentEntity getDepartmentById(Integer id) {
//        return manager.find(DepartmentEntity.class, id);
//    }
     
}