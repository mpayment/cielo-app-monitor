package com.m4u.monitor.commons.util.serialization;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class CustomStringSerializer extends StdSerializer<String> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 911249360115302960L;

	public CustomStringSerializer() {
		super(String.class);
	}

    @Override
    public void serialize(String value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonGenerationException {
		
        if (value != null) {
            value = value.trim();
        }
		
        jgen.writeString(value);
    }
}
