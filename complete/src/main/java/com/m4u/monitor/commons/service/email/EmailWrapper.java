package com.m4u.monitor.commons.service.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.m4u.monitor.commons.util.Util;

@Service
public class EmailWrapper {

	@Autowired
	private EmailManager emailManager;
	
	@Autowired
	private MessageManager msgManager;

	@Autowired
	private Util nMsg;
	
	String subject;
	String text;
	
	
	public void send(){
		
		if(subject.isEmpty() || text.isEmpty()){
			return;
		}
		nMsg.dispH1("  Enviando E-mail...  ");

	    int ctrl = -1;
	    while (ctrl == -1) {
	    	
			JavaMailSender javaMailSender = emailManager.javaMailService();
			SimpleMailMessage message = msgManager.buildMessage();
			message.setSubject(subject);
			message.setText(text);

	        try {
	    	    javaMailSender.send(message);
	    	    nMsg.dispH1("  Email Enviado!!!  ");	    
	    	    ctrl = 1;
	            
	        } catch (MailSendException e) {
	        	nMsg.dispH1(" Erro enviando E-mail, tentando novamente... ");
				nMsg.disp("   O F F L I N E   {} " + Util.timeNow());
				nMsg.disp("----------------------");
				nMsg.disp(" getLocalizedMessage() : " + e.getLocalizedMessage() + "\n");
				nMsg.disp(" getCause().toString() : " + e.getCause().toString() + "\n");
				nMsg.disp(" getMessage()          : " + e.getMessage() + "\n");
				nMsg.disp("----------------------");
	        }  
	    }
	}

	

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
}
