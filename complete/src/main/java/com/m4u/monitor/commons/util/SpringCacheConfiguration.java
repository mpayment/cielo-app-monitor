package com.m4u.monitor.commons.util;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.springframework.cache.CacheManager;
import org.springframework.cache.guava.GuavaCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.google.common.cache.CacheBuilder;
import com.m4u.monitor.commons.util.constants.CacheNames;
import org.springframework.stereotype.Component;


@Component
@Configuration
public class SpringCacheConfiguration {
	
	@Bean(name = "cacheManager")
	public CacheManager getCacheManager() {
		final GuavaCache messageCache = new GuavaCache(CacheNames.MESSAGE_BY_KEY,
				CacheBuilder.newBuilder().expireAfterWrite(10, TimeUnit.MINUTES).build());
		
		final GuavaCache booleanConfigCache = new GuavaCache(CacheNames.SERVER_CONFIGURATION_BOOLEAN,
				CacheBuilder.newBuilder().expireAfterWrite(10, TimeUnit.MINUTES).build());
		final GuavaCache byteConfigCache = new GuavaCache(CacheNames.SERVER_CONFIGURATION_BYTE,
				CacheBuilder.newBuilder().expireAfterWrite(10, TimeUnit.MINUTES).build());
		final GuavaCache intConfigCache = new GuavaCache(CacheNames.SERVER_CONFIGURATION_INT,
				CacheBuilder.newBuilder().expireAfterWrite(10, TimeUnit.MINUTES).build());
		final GuavaCache longConfigCache = new GuavaCache(CacheNames.SERVER_CONFIGURATION_LONG,
				CacheBuilder.newBuilder().expireAfterWrite(10, TimeUnit.MINUTES).build());
		final GuavaCache stringConfigCache = new GuavaCache(CacheNames.SERVER_CONFIGURATION_STRING,
				CacheBuilder.newBuilder().expireAfterWrite(10, TimeUnit.MINUTES).build());
		
		final SimpleCacheManager manager = new SimpleCacheManager();
		manager.setCaches(Arrays.asList(messageCache, booleanConfigCache, byteConfigCache, intConfigCache,
				longConfigCache, stringConfigCache));
		return manager;
	}
}
