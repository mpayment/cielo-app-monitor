package com.m4u.monitor.commons.service;

import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.m4u.monitor.commons.persistence.model.ServerConfiguration;
import com.m4u.monitor.commons.persistence.repository.ServerConfigurationRepository;
import com.m4u.monitor.commons.util.constants.CacheNames;
import com.m4u.monitor.commons.util.constants.ServerConfigurationKey;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;



@Service
public class ServerConfigurationService {
	// Do not implement CacheManagerListener. CacheManager has special handling on this service.

    @Autowired
    private ServerConfigurationRepository configurationRepo;

    @PostConstruct
    public void init() {
        reloadConfigurations();
//        for (ServerConfigurationKey key : ServerConfigurationKey.values()) {
//        	ServerConfiguration conf = configurationRepo.getOne(key);
//        	if (conf == null) {
//        		throw new IllegalStateException("Configuration not found => " + key);
//        	}
//        }
    }

	@Cacheable(CacheNames.SERVER_CONFIGURATION_STRING)
    public String getConfiguration(ServerConfigurationKey key) {
		final ServerConfiguration configuration = configurationRepo.findOne(key);
		return configuration == null ? null : configuration.getValue();
    }

	@Cacheable(cacheNames = CacheNames.SERVER_CONFIGURATION_INT)
    public Integer getConfigurationAsInt(ServerConfigurationKey key) {
        String config = getConfiguration(key);
        return StringUtils.isBlank(config) ? null : Integer.valueOf(config);
    }

	@Cacheable(cacheNames = CacheNames.SERVER_CONFIGURATION_LONG)
    public Long getConfigurationAsLong(ServerConfigurationKey key) {
    	String config = getConfiguration(key);
    	return StringUtils.isBlank(config) ? null : Long.valueOf(config);
    }

	@Cacheable(CacheNames.SERVER_CONFIGURATION_BOOLEAN)
    public Boolean getConfigurationAsBoolean(ServerConfigurationKey key) {
    	String config = getConfiguration(key);
    	return StringUtils.isBlank(config) ? null : Boolean.valueOf(config);
    }
    
	@Cacheable(CacheNames.SERVER_CONFIGURATION_BYTE)
    public Byte getConfigurationAsByte(ServerConfigurationKey key) {
    	String config = getConfiguration(key);
    	return StringUtils.isBlank(config) ? null : Byte.valueOf(config);
	}

	@CacheEvict(allEntries = true, cacheNames = {CacheNames.SERVER_CONFIGURATION_STRING,
			CacheNames.SERVER_CONFIGURATION_INT, CacheNames.SERVER_CONFIGURATION_LONG,
			CacheNames.SERVER_CONFIGURATION_BOOLEAN, CacheNames.SERVER_CONFIGURATION_BYTE})
    public void reloadConfigurations() {
    }
}