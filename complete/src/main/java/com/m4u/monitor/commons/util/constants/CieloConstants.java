package com.m4u.monitor.commons.util.constants;

import java.util.Locale;

import org.springframework.http.MediaType;

public class CieloConstants {

	public static final String ISO_DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";
	public static final String USER_DATE_PATTERN = "dd/MM/yyyy";
	public static final String HTML_UTF8_MEDIA_TYPE = MediaType.TEXT_HTML_VALUE + ";charset=utf-8";
	public static final Locale DEFAULT_LOCALE = new Locale("pt", "BR");
}
