package com.m4u.monitor.commons.util.web;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JsonMapperConfigurator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

import org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean;
import org.apache.cxf.transport.common.gzip.GZIPFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.m4u.monitor.commons.service.ServerConfigurationService;
import com.m4u.monitor.commons.util.constants.ServerConfigurationKey;
import com.m4u.monitor.commons.util.serialization.CustomStringDeserializer;
import com.m4u.monitor.commons.util.serialization.CustomStringSerializer;



@Component
public class WebClientUtil {

	private static final String DEFAULT_REMOTE_DATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
	private static final String DEFAULT_REMOTE_TIMEZONE_PATTERN = "GMT-03:00";

	@Autowired
	private ServerConfigurationService serverConfigurationService;

	public <T> T createJsonClient(Class<T> endpointMapping, ServerConfigurationKey endpointUrlConfigurationKey) {
		String remoteURL = serverConfigurationService.getConfiguration(endpointUrlConfigurationKey);
		return createJsonClient(endpointMapping, remoteURL);
	}

	public static <T> T createJsonClient(Class<T> endpointMapping, String endpointUrl) {
		final Logger logger = LoggerFactory.getLogger(WebClientUtil.class);
		logger.debug("Creating proxy to {} using {}", endpointUrl, endpointMapping);

		final List<Object> providers = buildMessageConverterProviderList();

		final JAXRSClientFactoryBean bean = new JAXRSClientFactoryBean();
		bean.setServiceClass(endpointMapping);
		bean.getFeatures().add(new GZIPFeature());
		bean.setProviders(providers);
		bean.setAddress(endpointUrl);
//        bean.getInInterceptors().add(new GZIPInInterceptor());
		bean.getInInterceptors().add(new InflaterInInterceptor());

		return bean.create(endpointMapping);
	}

	private static List<Object> buildMessageConverterProviderList() {
		final ObjectMapper objectMapper = getConfiguredObjectMapper();
		return Arrays.asList(
				(Object) new JacksonJaxbJsonProvider(objectMapper, JacksonJaxbJsonProvider.DEFAULT_ANNOTATIONS));
	}

	@SuppressWarnings("deprecation")
	private static ObjectMapper getConfiguredObjectMapper() {
		final SimpleModule simpleModule = createSimpleModule();
		final DateFormat dateFormat = createObjectMapperDateFormat();

		final ObjectMapper objectMapper = createDefaultJsonMapper();
		objectMapper.setDateFormat(dateFormat);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.setVisibilityChecker(objectMapper.getSerializationConfig()
				.getDefaultVisibilityChecker()
				.withFieldVisibility(JsonAutoDetect.Visibility.ANY)
				.withGetterVisibility(JsonAutoDetect.Visibility.NONE)
				.withSetterVisibility(JsonAutoDetect.Visibility.NONE)
				.withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
		objectMapper.registerModule(simpleModule);

		return objectMapper;
	}

	private static ObjectMapper createDefaultJsonMapper() {
		return new JsonMapperConfigurator(null, JacksonJaxbJsonProvider.DEFAULT_ANNOTATIONS).getDefaultMapper();
	}

	private static DateFormat createObjectMapperDateFormat() {
		final DateFormat dateFormat = new SimpleDateFormat(DEFAULT_REMOTE_DATETIME_PATTERN);
		dateFormat.setTimeZone(TimeZone.getTimeZone(DEFAULT_REMOTE_TIMEZONE_PATTERN));
		return dateFormat;
	}

	private static SimpleModule createSimpleModule() {
		final SimpleModule simpleModule = new SimpleModule("SimpleModule", new Version(1, 0, 0, null, null, null));
		simpleModule.addSerializer(new CustomStringSerializer());
		simpleModule.addDeserializer(String.class, new CustomStringDeserializer());
		return simpleModule;
	}

	public static String getJSONFormattedRequest(Object request) {
		final ObjectMapper objectMapper = getConfiguredObjectMapper();
		if (objectMapper == null) {
			return "";
		}

		try {
			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(request);
		} catch (Exception ex) {
			throw new RuntimeException("Unable to print the request entity", ex);
		}
	}
}
