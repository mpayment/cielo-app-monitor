package com.m4u.monitor.commons.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class Util {

	
	 public void disp(Object obj) {
         System.out.println(obj);
	 }   
	
	 public void dispH1(String obj) {
		 String lines = new String(new char[obj.length()+2]).replace("\0", "=");
		 System.out.println(lines);
         System.out.println(" " + obj);
         System.out.println(lines);
	 }   
	 
	 public void dispH2(String obj) {
		 String lines = new String(new char[obj.length()+2]).replace("\0", "=");
		 System.out.println(lines);
         System.out.println(" " + obj);
	 }   

	 
	 public static String dateTimeNow() {
		 Date date = new Date();
		 SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); 
		 return dt.format(date); 
	 }

	 public static String dateNow() {
		 Date date = new Date();
		 SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy"); 
		 return dt.format(date); 
	 }

	 public static String timeNow() {
		 Date date = new Date();
		 SimpleDateFormat dt = new SimpleDateFormat("HH:mm:ss"); 
		 return dt.format(date); 
	 }
	 
	 public static String extractFromException(String msg){
		String[] extract = msg.split(" ");
		return extract[1];
	 }
	 
}
