package com.m4u.monitor.commons.util.constants;

import com.m4u.monitor.commons.util.SpringCacheConfiguration;

/**
 * Names of caches configured at {@link SpringCacheConfiguration}
 */
public class CacheNames {
	
	public static final String MESSAGE_BY_KEY = "MESSAGE_BY_KEY";
	public static final String SERVER_CONFIGURATION_STRING = "SERVER_CONFIGURATION_STRING";
	public static final String SERVER_CONFIGURATION_LONG = "SERVER_CONFIGURATION_LONG";
	public static final String SERVER_CONFIGURATION_BYTE = "SERVER_CONFIGURATION_BYTE";
	public static final String SERVER_CONFIGURATION_BOOLEAN = "SERVER_CONFIGURATION_BOOLEAN";
	public static final String SERVER_CONFIGURATION_INT = "SERVER_CONFIGURATION_INT";

}