package com.m4u.monitor.commons.util.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

@SuppressWarnings("serial")
public class CustomStringDeserializer extends StdDeserializer<String> {
	
	public CustomStringDeserializer() {
		super(String.class);
	}

    @Override
    public String deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        final String value = jp.getText();
		return value == null ? null : value.trim();
    }
}